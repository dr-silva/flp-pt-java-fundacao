package fundacao.ordenacoes;

// IMPORTAÇÕES - FRACTAL LOTUS //
import fractallotus.fundacao.cerne.IOrdenacao;
import fractallotus.fundacao.cerne.IProjetor;
import fractallotus.fundacao.ordenacoes.OrdenacaoPorBaldeAscendente;
import fractallotus.fundacao.ordenacoes.OrdenacaoPorBaldeDescendente;

public class TestadorBalde extends TestadorOrdenacao
{
  protected IOrdenacao<Integer> criarOrdenador(boolean crescente)
  {
    IProjetor<Integer, Double> projetor = (valor) -> new Double(valor / 101.0);

    return crescente ? new OrdenacaoPorBaldeAscendente <Integer>(projetor)
                     : new OrdenacaoPorBaldeDescendente<Integer>(projetor);
  }
}
