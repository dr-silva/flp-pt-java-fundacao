package fundacao.ordenacoes;

// IMPORTAÇÕES - FRACTAL LOTUS //
import fractallotus.fundacao.cerne.IOrdenacao;
import fractallotus.fundacao.ordenacoes.OrdenacaoPorInsercaoAscendente;
import fractallotus.fundacao.ordenacoes.OrdenacaoPorInsercaoDescendente;

public class TestadorInsercao extends TestadorOrdenacao
{
  protected IOrdenacao<Integer> criarOrdenador(boolean crescente)
  {
    return crescente ? new OrdenacaoPorInsercaoAscendente <Integer>(comparador)
                     : new OrdenacaoPorInsercaoDescendente<Integer>(comparador);
  }
}
