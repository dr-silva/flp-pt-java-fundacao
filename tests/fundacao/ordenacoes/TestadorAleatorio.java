package fundacao.ordenacoes;

// IMPORTAÇÕES - JUNIT //
import org.junit.Test;
import org.junit.Assert;

// IMPORTAÇÕES - FRACTAL LOTUS //
import fractallotus.fundacao.cerne.IComparador;
import fractallotus.fundacao.cerne.IValidadorOrdenacao;
import fractallotus.fundacao.cerne.IOrdenacao;
import fractallotus.fundacao.cerne.FabricaComparador;
import fractallotus.fundacao.ordenacoes.ValidadorOrdenacaoAscendente;
import fractallotus.fundacao.ordenacoes.OrdenacaoAleatoria;

public class TestadorAleatorio
{
  private final IValidadorOrdenacao<Integer> validador;
  private final IOrdenacao         <Integer> ordenador;

  public TestadorAleatorio()
  {
    IComparador<Integer> comparador = FabricaComparador.<Integer>criar();

    validador = new ValidadorOrdenacaoAscendente<Integer>(comparador);
    ordenador = new OrdenacaoAleatoria          <Integer>();
  }

  private Integer[] criarVetor(int total)
  {
    if (total < 0)
      return null;

    Integer[] result = new Integer[total];

    for (int i = 0; i < total; i++)
      result[i] = new Integer(i + 1);

    return result;
  }

  protected boolean ordenar(int total)
  {
    Integer[] vetor = criarVetor(total);

           ordenador.ordenar(vetor);
    return validador.validar(vetor);
  }

  @Test
  public void testarNulo()
  {
    Assert.assertTrue(ordenar(-1));
  }

  @Test
  public void testarVazio()
  {
    Assert.assertTrue(ordenar(0));
  }

  @Test
  public void testarNaoVazio()
  {
    Assert.assertFalse(ordenar(100));
  }
}
