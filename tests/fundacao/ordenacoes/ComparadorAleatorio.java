package fundacao.ordenacoes;

// IMPORTAÇÕES - JAVA LANG //
import java.util.Comparator;
import java.util.HashMap;
import java.util.Random;

class ComparadorAleatorio implements Comparator<Integer>
{
  private final HashMap<Integer, Double> dicionario;
  private final Random                   aleatorio;

  public ComparadorAleatorio()
  {
    aleatorio  = new Random();
    dicionario = new HashMap<Integer, Double>();
  }

  public int compare(Integer o1, Integer o2)
  {
    Double esq = dicionario.get(o1),
           dir = dicionario.get(o2);

    if (esq == null)
    {
      esq = new Double(aleatorio.nextDouble());
      dicionario.put(o1, esq);
    }

    if (dir == null)
    {
      dir = new Double(aleatorio.nextDouble());
      dicionario.put(o2, dir);
    }

    return esq.compareTo(dir);
  }
}
