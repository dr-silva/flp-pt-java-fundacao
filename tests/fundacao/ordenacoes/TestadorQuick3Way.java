package fundacao.ordenacoes;

// IMPORTAÇÕES - FRACTAL LOTUS //
import fractallotus.fundacao.cerne.IOrdenacao;
import fractallotus.fundacao.ordenacoes.OrdenacaoQuick3WayAscendente;
import fractallotus.fundacao.ordenacoes.OrdenacaoQuick3WayDescendente;

public class TestadorQuick3Way extends TestadorOrdenacao
{
  protected IOrdenacao<Integer> criarOrdenador(boolean crescente)
  {
    return crescente ? new OrdenacaoQuick3WayAscendente <Integer>(comparador)
                     : new OrdenacaoQuick3WayDescendente<Integer>(comparador);
  }
}
