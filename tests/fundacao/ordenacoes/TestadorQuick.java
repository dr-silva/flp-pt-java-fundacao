package fundacao.ordenacoes;

// IMPORTAÇÕES - FRACTAL LOTUS //
import fractallotus.fundacao.cerne.IOrdenacao;
import fractallotus.fundacao.ordenacoes.OrdenacaoQuickAscendente;
import fractallotus.fundacao.ordenacoes.OrdenacaoQuickDescendente;

public class TestadorQuick extends TestadorOrdenacao
{
  protected IOrdenacao<Integer> criarOrdenador(boolean crescente)
  {
    return crescente ? new OrdenacaoQuickAscendente <Integer>(comparador)
                     : new OrdenacaoQuickDescendente<Integer>(comparador);
  }
}
