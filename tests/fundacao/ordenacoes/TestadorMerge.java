package fundacao.ordenacoes;

// IMPORTAÇÕES - FRACTAL LOTUS //
import fractallotus.fundacao.cerne.IOrdenacao;
import fractallotus.fundacao.ordenacoes.OrdenacaoMergeAscendente;
import fractallotus.fundacao.ordenacoes.OrdenacaoMergeDescendente;

public class TestadorMerge extends TestadorOrdenacao
{
  protected IOrdenacao<Integer> criarOrdenador(boolean crescente)
  {
    return crescente ? new OrdenacaoMergeAscendente <Integer>(comparador)
                     : new OrdenacaoMergeDescendente<Integer>(comparador);
  }
}
