package fundacao.ordenacoes;

// IMPORTAÇÕES - FRACTAL LOTUS //
import fractallotus.fundacao.cerne.IOrdenacao;
import fractallotus.fundacao.ordenacoes.OrdenacaoPorSelecaoAscendente;
import fractallotus.fundacao.ordenacoes.OrdenacaoPorSelecaoDescendente;

public class TestadorSelecao extends TestadorOrdenacao
{
  protected IOrdenacao<Integer> criarOrdenador(boolean crescente)
  {
    return crescente ? new OrdenacaoPorSelecaoAscendente <Integer>(comparador)
                     : new OrdenacaoPorSelecaoDescendente<Integer>(comparador);
  }
}
