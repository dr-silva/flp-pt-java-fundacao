package fundacao.ordenacoes;

// IMPORTAÇÕES - FRACTAL LOTUS //
import fractallotus.fundacao.cerne.IOrdenacao;
import fractallotus.fundacao.ordenacoes.OrdenacaoShellAscendente;
import fractallotus.fundacao.ordenacoes.OrdenacaoShellDescendente;

public class TestadorShell extends TestadorOrdenacao
{
  protected IOrdenacao<Integer> criarOrdenador(boolean crescente)
  {
    return crescente ? new OrdenacaoShellAscendente <Integer>(comparador)
                     : new OrdenacaoShellDescendente<Integer>(comparador);
  }
}
