package fundacao.ordenacoes;

// IMPORTAÇÕES - FRACTAL LOTUS //
import fractallotus.fundacao.cerne.IOrdenacao;
import fractallotus.fundacao.ordenacoes.OrdenacaoHeapAscendente;
import fractallotus.fundacao.ordenacoes.OrdenacaoHeapDescendente;

public class TestadorHeap extends TestadorOrdenacao
{
  protected IOrdenacao<Integer> criarOrdenador(boolean crescente)
  {
    return crescente ? new OrdenacaoHeapAscendente <Integer>(comparador)
                     : new OrdenacaoHeapDescendente<Integer>(comparador);
  }
}
