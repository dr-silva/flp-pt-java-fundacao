package fundacao.ordenacoes;

// IMPORTAÇÕES - JAVA LANG //
import java.util.Comparator;
import java.util.Arrays;

// IMPORTAÇÕES - JUNIT //
import org.junit.Test;
import org.junit.Assert;

// IMPORTAÇÕES - FRACTAL LOTUS //
import fractallotus.fundacao.cerne.IComparador;
import fractallotus.fundacao.cerne.IValidadorOrdenacao;
import fractallotus.fundacao.cerne.IOrdenacao;
import fractallotus.fundacao.cerne.FabricaComparador;
import fractallotus.fundacao.ordenacoes.ValidadorOrdenacaoAscendente;
import fractallotus.fundacao.ordenacoes.ValidadorOrdenacaoDescendente;

public abstract class TestadorOrdenacao
{
  protected final Comparator <Integer> aleatorio;
  protected final IComparador<Integer> comparador;

  public TestadorOrdenacao()
  {
    aleatorio  = new ComparadorAleatorio();
    comparador = FabricaComparador.<Integer>criar();
  }

  private IValidadorOrdenacao<Integer> criarValidador(boolean crescente)
  {
    return crescente ? new ValidadorOrdenacaoAscendente <Integer>(comparador)
                     : new ValidadorOrdenacaoDescendente<Integer>(comparador);
  }

  private Integer[] criarVetor(int total)
  {
    if (total < 0)
      return null;

    Integer[] result = new Integer[total];

    for (int i = 0; i < total; i++)
      result[i] = new Integer(i + 1);

    Arrays.sort(result);

    return result;
  }

  protected abstract IOrdenacao<Integer> criarOrdenador(boolean crescente);

  protected boolean ordenar(int total, boolean crescente)
  {
    IOrdenacao         <Integer> ordenador = criarOrdenador(crescente);
    IValidadorOrdenacao<Integer> validador = criarValidador(crescente);
    Integer[]                    vetor     = criarVetor    (total);

           ordenador.ordenar(vetor);
    return validador.validar(vetor);
  }

  @Test
  public void testarCrescenteNulo()
  {
    Assert.assertTrue(ordenar(-1, true));
  }

  @Test
  public void testarCrescenteVazio()
  {
    Assert.assertTrue(ordenar(0, true));
  }

  @Test
  public void testarCrescenteNaoVazio()
  {
    Assert.assertTrue(ordenar(100, true));
  }

  @Test
  public void testarDecrescenteNulo()
  {
    Assert.assertTrue(ordenar(-1, false));
  }

  @Test
  public void testarDecrescenteVazio()
  {
    Assert.assertTrue(ordenar(0, false));
  }

  @Test
  public void testarDecrescenteNaoVazio()
  {
    Assert.assertTrue(ordenar(100, false));
  }
}
