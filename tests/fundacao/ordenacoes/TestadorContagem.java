package fundacao.ordenacoes;

// IMPORTAÇÕES - FRACTAL LOTUS //
import fractallotus.fundacao.cerne.IOrdenacao;
import fractallotus.fundacao.cerne.IProjetor;
import fractallotus.fundacao.ordenacoes.OrdenacaoPorContagemAscendente;
import fractallotus.fundacao.ordenacoes.OrdenacaoPorContagemDescendente;

public class TestadorContagem extends TestadorOrdenacao
{
  protected IOrdenacao<Integer> criarOrdenador(boolean crescente)
  {
    IProjetor<Integer, Integer> projetor = (valor) -> valor;

    return crescente ? new OrdenacaoPorContagemAscendente <Integer>(projetor)
                     : new OrdenacaoPorContagemDescendente<Integer>(projetor);
  }
}
