package fundacao;

import org.junit.runner.JUnitCore;

public class Testador
{
  private static final String[] testes = {
    // ORDENAÇÕES //
    "fundacao.ordenacoes.TestadorInsercao",
    "fundacao.ordenacoes.TestadorSelecao",
    "fundacao.ordenacoes.TestadorShell",
    "fundacao.ordenacoes.TestadorHeap",
    "fundacao.ordenacoes.TestadorMerge",
    "fundacao.ordenacoes.TestadorQuick",
    "fundacao.ordenacoes.TestadorQuick3Way",
    "fundacao.ordenacoes.TestadorContagem",
    "fundacao.ordenacoes.TestadorBalde",
    "fundacao.ordenacoes.TestadorAleatorio",

    // COLEÇÕES //
    "fundacao.colecoes.TestadorPilhaVetorial",
    "fundacao.colecoes.TestadorPilhaEncadeada",

    "fundacao.colecoes.TestadorFilaVetorial",
    "fundacao.colecoes.TestadorFilaEncadeada",
    "fundacao.colecoes.TestadorFilaPrioridadeMinimaVetorial",
    "fundacao.colecoes.TestadorFilaPrioridadeMinimaEncadeada",
    "fundacao.colecoes.TestadorFilaPrioridadeMaximaVetorial",
    "fundacao.colecoes.TestadorFilaPrioridadeMaximaEncadeada",

    "fundacao.colecoes.TestadorBolsaVetorial",
    "fundacao.colecoes.TestadorBolsaEncadeada",
    "fundacao.colecoes.TestadorBolsaOrdenadaVetorial",
    "fundacao.colecoes.TestadorBolsaOrdenadaEncadeada",

    "fundacao.colecoes.TestadorListaVetorial",
    "fundacao.colecoes.TestadorListaEncadeada",

    "fundacao.colecoes.TestadorDicionarioHashSondado",
    "fundacao.colecoes.TestadorDicionarioHashEncadeado",
    "fundacao.colecoes.TestadorDicionarioOrdenadoVetorial",
    "fundacao.colecoes.TestadorDicionarioOrdenadoEncadeado",

    "fundacao.colecoes.TestadorConjuntoHashSondado",
    "fundacao.colecoes.TestadorConjuntoHashEncadeado",
    "fundacao.colecoes.TestadorConjuntoOrdenadoVetorial",
    "fundacao.colecoes.TestadorConjuntoOrdenadoEncadeado",

    // MATEMÁTICA //
    "fundacao.matematica.TestadorGCLMatematica",
    "fundacao.matematica.TestadorGCLAppleCarbon",
    "fundacao.matematica.TestadorGCLBorlandC",
    "fundacao.matematica.TestadorGCLBorlandPascal",
    "fundacao.matematica.TestadorGCLCpp11",
    "fundacao.matematica.TestadorGCLGLibC",
    "fundacao.matematica.TestadorGCLMMIX",
    "fundacao.matematica.TestadorGCLNewLib",
    "fundacao.matematica.TestadorGCLNumericalRecipes",
    "fundacao.matematica.TestadorGCLOpenVms",
    "fundacao.matematica.TestadorGCLPosix",
    "fundacao.matematica.TestadorGCLRtlUniform",

    "fundacao.matematica.TestadorComplexo",

    // ENTRADAS & SAÍDAS //
    "fundacao.entradas_saidas.TestadorImagemBitmap"
  };

  public static void main(String[] args)
  {
    System.out.println("Testando o pacote Fundação da Fractal Lotus.\n");

    JUnitCore.main(testes);
  }
}
