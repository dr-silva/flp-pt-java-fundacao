package fundacao.entradas_saidas;

// IMPORTAÇÕES - JAVA //
import java.io.InputStream;

// IMPORTAÇÕES - JUNIT //
import org.junit.Test;
import org.junit.Assert;

// IMPORTAÇÕES - FRACTAL LOTUS //
import fractallotus.fundacao.entradas_saidas.ImagemBitmap;

public class TestadorImagemBitmap
{
  // ATRIBUTOS //
  private final String ORIGINAL_1,
                       ORIGINAL_2,
                       NEGATIVO,
                       ESPELHO_HORIZONTAL,
                       ESPELHO_VERTICAL,
                       ROTACAO_ESQUERDA,
                       ROTACAO_DIREITA;

  public TestadorImagemBitmap()
  {
    ORIGINAL_1         = obterRecurso("/recursos/original-1.png");
    ORIGINAL_2         = obterRecurso("/recursos/original-2.png");
    NEGATIVO           = obterRecurso("/recursos/neg.png");
    ESPELHO_HORIZONTAL = obterRecurso("/recursos/esp-horz.png");
    ESPELHO_VERTICAL   = obterRecurso("/recursos/esp-vert.png");
    ROTACAO_ESQUERDA   = obterRecurso("/recursos/rot-esq.png");
    ROTACAO_DIREITA    = obterRecurso("/recursos/rot-dir.png");
  }

  private String obterRecurso(String recurso)
  {
    recurso = getClass().getResource(recurso).toString();
    int i   = recurso.indexOf(":");

    return recurso.substring(i + 1);
  }

  private void validarImagens(ImagemBitmap ativo, ImagemBitmap passivo)
  {
    boolean isTrue = (ativo.altura () == passivo.altura ()) &&
                     (ativo.largura() == passivo.largura());

    for (int linha = 0; isTrue && linha < ativo.altura(); linha++)
      for (int coluna = 0; isTrue && coluna < ativo.largura(); coluna++)
        isTrue = ativo.pixel(linha, coluna).equals(passivo.pixel(linha, coluna));

    if (!isTrue)
      Assert.fail("As imagens não são iguais");
  }

  @Test
  public void testarNegativo()
  {
    try
    {
      ImagemBitmap ativo   = new ImagemBitmap(ORIGINAL_1),
                   passivo = new ImagemBitmap(NEGATIVO);

      ativo.negativar();
      validarImagens(ativo, passivo);
    }
    catch (Throwable e)
    {
      Assert.fail(e.getMessage());
    }
  }

  @Test
  public void testarEspelhoHorizontal()
  {
    try
    {
      ImagemBitmap ativo   = new ImagemBitmap(ORIGINAL_2),
                   passivo = new ImagemBitmap(ESPELHO_HORIZONTAL);

      ativo.espelharHorizontalmente();
      validarImagens(ativo, passivo);
    }
    catch (Throwable e)
    {
      Assert.fail(e.getMessage());
    }
  }

  @Test
  public void testarEspelhoVertical()
  {
    try
    {
      ImagemBitmap ativo   = new ImagemBitmap(ORIGINAL_1),
                   passivo = new ImagemBitmap(ESPELHO_VERTICAL);

      ativo.espelharVerticalmente();
      validarImagens(ativo, passivo);
    }
    catch (Throwable e)
    {
      Assert.fail(e.getMessage());
    }
  }

  @Test
  public void testarRotacaoEsquerda()
  {
    try
    {
      ImagemBitmap ativo   = new ImagemBitmap(ORIGINAL_1),
                   passivo = new ImagemBitmap(ROTACAO_ESQUERDA);

      ativo.rotarParaEsquerda();
      validarImagens(ativo, passivo);
    }
    catch (Throwable e)
    {
      Assert.fail(e.getMessage());
    }
  }

  @Test
  public void testarRotacaoDireita()
  {
    try
    {
      ImagemBitmap ativo   = new ImagemBitmap(ORIGINAL_1),
                   passivo = new ImagemBitmap(ROTACAO_DIREITA);

      ativo.rotarParaDireita();
      validarImagens(ativo, passivo);
    }
    catch (Throwable e)
    {
      Assert.fail(e.getMessage());
    }
  }
}
