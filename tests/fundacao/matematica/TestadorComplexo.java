package fundacao.matematica;

// IMPORTAÇÕES - JAVA LANG //
import java.util.Comparator;
import java.util.Arrays;

// IMPORTAÇÕES - JUNIT //
import org.junit.Test;
import org.junit.Assert;

// IMPORTAÇÕES - FRACTAL LOTUS //
import fractallotus.fundacao.cerne.IComparadorIgualdade;
import fractallotus.fundacao.matematica.Complexo;

public class TestadorComplexo
{
  private static final Complexo ESQUERDA = new Complexo( 2,  3),
                                DIREITA  = new Complexo(-4, -4);
  private static final int      INTEIRO  = 2;
  private static final double   REAL     = 3.0;

  private final IComparadorIgualdade<Complexo> comparador;

  public TestadorComplexo()
  {
    comparador = Complexo.comparadorIgualdade();
  }

  private void validar(Complexo esperado, Complexo calculado)
  {
    boolean eIgual   = comparador.eIgual(esperado, calculado);
    String  mensagem = String.format
    (
      "expected <%s>, but calculated <%s>", esperado, calculado
    );

    Assert.assertTrue(mensagem, eIgual);
  }

  @Test
  public void testarPositivo()
  {
    validar
    (
      new Complexo(2, 3),         // ESPERADO
      Complexo.positive(ESQUERDA) // CALCULADO
    );
  }

  @Test
  public void testarNegativo()
  {
    validar
    (
      new Complexo(-2, -3),       // ESPERADO
      Complexo.negative(ESQUERDA) // CALCULADO
    );
  }

  @Test
  public void testarAdicao_1()
  {
    validar
    (
      new Complexo(4, 3),             // ESPERADO
      Complexo.add(ESQUERDA, INTEIRO) // CALCULADO
    );
  }

  @Test
  public void testarAdicao_2()
  {
    validar
    (
      new Complexo(5, 3),          // ESPERADO
      Complexo.add(ESQUERDA, REAL) // CALCULADO
    );
  }

  @Test
  public void testarAdicao_3()
  {
    validar
    (
      new Complexo(-2, -1),           // ESPERADO
      Complexo.add(ESQUERDA, DIREITA) // CALCULADO
    );
  }

  @Test
  public void testarSubtracao_1()
  {
    validar
    (
      new Complexo(0, 3),                  // ESPERADO
      Complexo.subtract(ESQUERDA, INTEIRO) // CALCULADO
    );
  }

  @Test
  public void testarSubtracao_2()
  {
    validar
    (
      new Complexo(0, -3),                 // ESPERADO
      Complexo.subtract(INTEIRO, ESQUERDA) // CALCULADO
    );
  }

  @Test
  public void testarSubtracao_3()
  {
    validar
    (
      new Complexo(-1, 3),              // ESPERADO
      Complexo.subtract(ESQUERDA, REAL) // CALCULADO
    );
  }

  @Test
  public void testarSubtracao_4()
  {
    validar
    (
      new Complexo(1, -3),              // ESPERADO
      Complexo.subtract(REAL, ESQUERDA) // CALCULADO
    );
  }

  @Test
  public void testarSubtracao_5()
  {
    validar
    (
      new Complexo(6, 7),                  // ESPERADO
      Complexo.subtract(ESQUERDA, DIREITA) // CALCULADO
    );
  }

  @Test
  public void testarMultiplicacao_1()
  {
    validar
    (
      new Complexo(4, 6),                  // ESPERADO
      Complexo.multiply(ESQUERDA, INTEIRO) // CALCULADO
    );
  }

  @Test
  public void testarMultiplicacao_2()
  {
    validar
    (
      new Complexo(6, 9),               // ESPERADO
      Complexo.multiply(ESQUERDA, REAL) // CALCULADO
    );
  }

  @Test
  public void testarMultiplicacao_3()
  {
    validar
    (
      new Complexo(4, -20),                // ESPERADO
      Complexo.multiply(ESQUERDA, DIREITA) // CALCULADO
    );
  }

  @Test
  public void testarDivisao_1()
  {
    validar
    (
      new Complexo(-0.375, 0.375),   // ESPERADO
      Complexo.divide(REAL, DIREITA) // CALCULADO
    );
  }

  @Test
  public void testarDivisao_2()
  {
    validar
    (
      new Complexo(0.666666667, 1),   // ESPERADO
      Complexo.divide(ESQUERDA, REAL) // CALCULADO
    );
  }

  @Test
  public void testarDivisao_3()
  {
    validar
    (
      new Complexo(-0.625, -0.125),      // ESPERADO
      Complexo.divide(ESQUERDA, DIREITA) // CALCULADO
    );
  }

  @Test
  public void testarDivisao_4()
  {
    Complexo valor = Complexo.divide(ESQUERDA, 0);

    if (!Complexo.eNaN(valor))
      Assert.fail("Divisão por 0 devia retornar um valor não-numérico");
  }

  @Test
  public void testarDivisao_5()
  {
    Complexo valor = Complexo.divide(ESQUERDA, Complexo.ZERO);

    if (!Complexo.eNaN(valor))
      Assert.fail("Divisão por 0 devia retornar um valor não-numérico");
  }

  @Test
  public void testarExponenciacao_1()
  {
    validar
    (
      new Complexo(-7.315110095, 1.042743656),  // ESPERADO
      Complexo.exp(ESQUERDA)                    // CALCULADO
    );
  }

  @Test
  public void testarExponenciacao_2()
  {
    validar
    (
      new Complexo(-0.281851193, -0.107283263), // ESPERADO
      Complexo.exp(ESQUERDA, DIREITA)           // CALCULADO
    );
  }

  @Test
  public void testarLogaritmo_1()
  {
    validar
    (
      new Complexo(1.282474679, 0.982793723), // ESPERADO
      Complexo.ln(ESQUERDA)                   // CALCULADO
    );
  }

  @Test
  public void testarLogaritmo_2()
  {
    validar
    (
      new Complexo(-0.035736229, -1.809839357), // ESPERADO
      Complexo.log(ESQUERDA, DIREITA)           // CALCULADO
    );
  }

  @Test
  public void testarSeno()
  {
    validar
    (
      new Complexo(9.154499147, -4.16890696), // ESPERADO
      Complexo.sen(ESQUERDA)                  // CALCULADO
    );
  }

  @Test
  public void testarCosseno()
  {
    validar
    (
      new Complexo(-4.189625691, -9.109227894), // ESPERADO
      Complexo.cos(ESQUERDA)                  // CALCULADO
    );
  }

  @Test
  public void testarTangente()
  {
    validar
    (
      new Complexo(-0.003764026, 1.003238627), // ESPERADO
      Complexo.tan(ESQUERDA)                   // CALCULADO
    );
  }

  @Test
  public void testarCossecante()
  {
    validar
    (
      new Complexo(0.09047321, 0.041200986), // ESPERADO
      Complexo.csc(ESQUERDA)                 // CALCULADO
    );
  }

  @Test
  public void testarSecante()
  {
    validar
    (
      new Complexo(-0.041674964, 0.090611137), // ESPERADO
      Complexo.sec(ESQUERDA)                 // CALCULADO
    );
  }

  @Test
  public void testarCotangente()
  {
    validar
    (
      new Complexo(-0.00373971, -0.996757797), // ESPERADO
      Complexo.cot(ESQUERDA)                   // CALCULADO
    );
  }

  @Test
  public void testarSenoHiperbolico()
  {
    validar
    (
      new Complexo(-3.59056459, 0.530921086), // ESPERADO
      Complexo.senh(ESQUERDA)                 // CALCULADO
    );
  }

  @Test
  public void testarCossenoHiperbolico()
  {
    validar
    (
      new Complexo(-3.724545505, 0.51182257), // ESPERADO
      Complexo.cosh(ESQUERDA)                 // CALCULADO
    );
  }

  @Test
  public void testarTangenteHiperbolica()
  {
    validar
    (
      new Complexo(0.965385879, -0.009884375), // ESPERADO
      Complexo.tanh(ESQUERDA)                   // CALCULADO
    );
  }

  @Test
  public void testarCossecanteHiperbolica()
  {
    validar
    (
      new Complexo(-0.272548661, -0.040300579), // ESPERADO
      Complexo.csch(ESQUERDA)                   // CALCULADO
    );
  }

  @Test
  public void testarSecanteHiperbolica()
  {
    validar
    (
      new Complexo(-0.263512975, -0.036211637), // ESPERADO
      Complexo.sech(ESQUERDA)                   // CALCULADO
    );
  }

  @Test
  public void testarCotangenteHiperbolica()
  {
    validar
    (
      new Complexo(1.035746638, 0.010604783), // ESPERADO
      Complexo.coth(ESQUERDA)                 // CALCULADO
    );
  }
}
