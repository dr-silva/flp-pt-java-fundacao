package fundacao.matematica;

// IMPORTAÇÕES - JAVA LANG //

// IMPORTAÇÕES - JUNIT //
import org.junit.Test;
import org.junit.Assert;

// IMPORTAÇÕES - FRACTAL LOTUS //

public abstract class TestadorGCL
{
  private static final double DELTA_INT    = 10.0,
                              DELTA_DOUBLE = 1.0 / 12.0;
  private static final int    TOTAL        = 1000;
  private static final int    LIMITE       = 500;

  protected abstract int    gerarInteger(int limite);
  protected abstract double gerarDouble ();

  @Test
  public void testarInteger()
  {
    double soma = 0;

    for (int i = 0; i < TOTAL; i++)
    {
      int valor = gerarInteger(LIMITE);

      if ((valor < 0) || (valor >= LIMITE))
        Assert.fail("Valor inteiro fora do limite.");

      soma += valor;
    }

    Assert.assertEquals((LIMITE -1) / 2.0, soma / TOTAL, DELTA_INT);
  }

  @Test
  public void testarDouble()
  {
    double soma = 0;

    for (int i = 0; i < TOTAL; i++)
    {
      double valor = gerarDouble();

      if ((valor < 0) || (valor >= 1))
        Assert.fail("Valor double fora do limite.");

      soma += valor;
    }

    Assert.assertEquals(0.5, soma / TOTAL, DELTA_DOUBLE);
  }

  @Test
  public void testarErro()
  {
    try
    {
      gerarInteger(0);

      Assert.fail("Era para ter levantado uma exceção.");
    }
    catch (IllegalArgumentException e)
    {
      // do nothing
    }
    catch (Throwable e)
    {
      Assert.fail("Erro desconhecido: " + e);
    }
  }
}
