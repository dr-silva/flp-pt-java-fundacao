package fundacao.matematica;

// IMPORTAÇÕES - FRACTAL LOTUS //
import fractallotus.fundacao.matematica.Matematica;

public class TestadorGCLMatematica extends TestadorGCL
{
  protected int gerarInteger(int limite)
  {
    return Matematica.uniforme(limite);
  }

  protected double gerarDouble ()
  {
    return Matematica.uniforme();
  }
}
