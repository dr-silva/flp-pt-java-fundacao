package fundacao.matematica;

// IMPORTAÇÕES - FRACTAL LOTUS //
import fractallotus.fundacao.matematica.IGeradorCongruencial;
import fractallotus.fundacao.matematica.TipoGeradorCongruencial;
import fractallotus.fundacao.matematica.FabricaGeradorCongruencial;

public abstract class TestadorGCLInterface extends TestadorGCL
{
  private final IGeradorCongruencial gerador;

  public TestadorGCLInterface(String nome)
  {
    gerador = FabricaGeradorCongruencial.criar
    (
      nome, TipoGeradorCongruencial.Linear, 20210927
    );
  }

  protected int gerarInteger(int limite)
  {
    return gerador.uniforme(limite);
  }

  protected double gerarDouble()
  {
    return gerador.uniforme();
  }
}
