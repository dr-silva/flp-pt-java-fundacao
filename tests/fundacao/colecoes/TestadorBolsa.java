package fundacao.colecoes;

// IMPORTAÇÕES - JUNIT //
import org.junit.Test;
import org.junit.Assert;

// IMPORTAÇÕES - FRACTAL LOTUS //
import fractallotus.fundacao.cerne.IEnumeravel;
import fractallotus.fundacao.cerne.IGuardavel;

public abstract class TestadorBolsa extends TestadorEnumeravel
{
  // ATRIBUTOS //
  protected final IGuardavel<Integer> bolsa;

  // CONSTRUTOR //
  public TestadorBolsa()
  {
    super();

    bolsa = (IGuardavel<Integer>)colecao;
  }

  // ASSISTENTES //
  protected boolean validar()
  {
    return vetor.validarValores(colecao.toArray(Integer.class));
  }

  // TESTES //
  @Test
  public void testarAdicao()
  {
    if (!bolsa.vazio())
      Assert.fail("A coleção devia estar vazia.");

    for (int valor : vetor.valores())
      bolsa.guardar(valor);

    Assert.assertTrue(validar());
  }
}
