package fundacao.colecoes;

// IMPORTAÇÕES - FRACTAL LOTUS //
import fractallotus.fundacao.cerne.IEnumeravel;
import fractallotus.fundacao.colecoes.ConjuntoHashEncadeado;

public class TestadorConjuntoHashEncadeado extends TestadorConjuntoHash
{
  // ASSISTENTES //
  protected IEnumeravel<Integer> criarColecao()
  {
    super.criarColecao();

    return new ConjuntoHashEncadeado<Integer>(vetor.total(), comparador);
  }

  // CONSTRUTOR //
  public TestadorConjuntoHashEncadeado()
  {
    super();
  }
}
