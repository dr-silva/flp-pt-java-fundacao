package fundacao.colecoes;

// IMPORTAÇÕES - JUNIT //
import org.junit.Test;
import org.junit.Assert;

// IMPORTAÇÕES - FRACTAL LOTUS //
import fractallotus.fundacao.cerne.IEnumeravel;
import fractallotus.fundacao.cerne.IEmpilhavel;

public abstract class TestadorPilha extends TestadorEnumeravel
{
  // ATRIBUTOS //
  protected final IEmpilhavel<Integer> pilha;

  // CONSTRUTOR //
  public TestadorPilha()
  {
    super();

    pilha = (IEmpilhavel<Integer>)colecao;
  }

  // TESTES //
  @Test
  public void testarEmpilhamento()
  {
    pilha.limpar();

    if (!pilha.vazio())
      Assert.fail("A coleção devia estar vazia.");

    for (int valor : vetor.valores())
      pilha.empilhar(valor);

    Assert.assertTrue(vetor.validarConteudo(pilha));
  }

  @Test
  public void testarDesempilhamento()
  {
    pilha.limpar();

    if (!pilha.vazio())
      Assert.fail("A coleção devia estar vazia.");

    for (int valor : vetor.valores())
      pilha.empilhar(valor);

    Integer[] itens = new Integer[pilha.total()];
    int       index = 0;

    while (!pilha.vazio())
      itens[index++] = pilha.desempilhar();

    Assert.assertTrue(vetor.validarInverso(itens));
  }
}
