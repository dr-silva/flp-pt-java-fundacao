package fundacao.colecoes;

// IMPORTAÇÕES - FRACTAL LOTUS //
import fractallotus.fundacao.cerne.IDicionarizavel;
import fractallotus.fundacao.colecoes.DicionarioHashEncadeado;

public class TestadorDicionarioHashEncadeado extends TestadorDicionarioHash
{
  // ASSISTENTES //
  protected IDicionarizavel<Integer, Double> criarDicionario()
  {
    return new DicionarioHashEncadeado<Integer, Double>(vetor.total(), comparador);
  }

  // CONSTRUTOR //
  public TestadorDicionarioHashEncadeado()
  {
    super();
  }
}
