package fundacao.colecoes;

import fractallotus.fundacao.cerne.IEnumeravel;
import fractallotus.fundacao.cerne.IComparador;
import fractallotus.fundacao.cerne.FabricaComparador;

public abstract class TestadorFilaPrioridade extends TestadorFila
{
  // ATRIBUTOS //
  protected IComparador<Integer> comparador;

  // CONSTRUTOR //
  public TestadorFilaPrioridade()
  {
    super();
  }

  protected IEnumeravel<Integer> criarColecao()
  {
    comparador = FabricaComparador.<Integer>criar();

    return null;
  }
}
