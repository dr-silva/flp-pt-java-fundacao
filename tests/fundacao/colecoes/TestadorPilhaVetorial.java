package fundacao.colecoes;

// IMPORTAÇÕES - FRACTAL LOTUS //
import fractallotus.fundacao.cerne.IEnumeravel;
import fractallotus.fundacao.colecoes.PilhaVetorial;

public class TestadorPilhaVetorial extends TestadorPilha
{
  public TestadorPilhaVetorial()
  {
    super();
  }

  protected IEnumeravel<Integer> criarColecao()
  {
    return new PilhaVetorial<Integer>(vetor.total());
  }
}
