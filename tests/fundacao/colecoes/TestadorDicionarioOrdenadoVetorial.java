package fundacao.colecoes;

// IMPORTAÇÕES - FRACTAL LOTUS //
import fractallotus.fundacao.cerne.IDicionarizavel;
import fractallotus.fundacao.colecoes.DicionarioOrdenadoVetorial;

public class TestadorDicionarioOrdenadoVetorial extends TestadorDicionarioOrdenado
{
  // ASSISTENTES //
  protected IDicionarizavel<Integer, Double> criarDicionario()
  {
    return new DicionarioOrdenadoVetorial<Integer, Double>(vetor.total(), comparador);
  }

  // CONSTRUTOR //
  public TestadorDicionarioOrdenadoVetorial()
  {
    super();
  }
}
