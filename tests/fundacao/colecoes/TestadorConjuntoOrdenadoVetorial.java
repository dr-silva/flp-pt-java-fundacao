package fundacao.colecoes;

// IMPORTAÇÕES - FRACTAL LOTUS //
import fractallotus.fundacao.cerne.IEnumeravel;
import fractallotus.fundacao.colecoes.ConjuntoOrdenadoVetorial;

public class TestadorConjuntoOrdenadoVetorial extends TestadorConjuntoOrdenado
{
  // ASSISTENTES //
  protected IEnumeravel<Integer> criarColecao()
  {
    super.criarColecao();

    return new ConjuntoOrdenadoVetorial<Integer>(vetor.total(), comparador);
  }

  // CONSTRUTOR //
  public TestadorConjuntoOrdenadoVetorial()
  {
    super();
  }
}
