package fundacao.colecoes;

// IMPORTAÇÕES - JAVA //
import java.util.Arrays;

// IMPORTAÇÕES - JUNIT //
import org.junit.Test;
import org.junit.Assert;

// IMPORTAÇÕES - FRACTAL LOTUS //
import fractallotus.fundacao.cerne.IEnumeravel;
import fractallotus.fundacao.cerne.IEmpilhavel;
import fractallotus.fundacao.cerne.IDistinguivel;
import fractallotus.fundacao.colecoes.PilhaVetorial;

public abstract class TestadorConjunto extends TestadorEnumeravel
{
  // ATRIBUTOS //
  private IDistinguivel<Integer> conjunto;

  // ASSISTENTES //
  protected abstract boolean validarInsercao();

  protected boolean validarConjunto(int[] itens)
  {
    Integer valores[] = colecao.toArray(Integer.class);
    int     total     = colecao.total();
    boolean result    = valores.length == total;

    Arrays.sort(valores);

    for (int i = 0; result && (i < total); i++)
      result = itens[i] == valores[i].intValue();

    return result;
  }

  // CONSTRUTOR //
  public TestadorConjunto()
  {
    super();

    conjunto = (IDistinguivel<Integer>)colecao;
  }

  // TESTES //
  @Test
  public void testarInsercao()
  {
    conjunto.limpar();

    if (!conjunto.vazio())
      Assert.fail("A coleção devia estar vazia.");

    for (int valor : vetor.valores())
      conjunto.adicionar(valor);

    Assert.assertTrue(validarInsercao());
  }

  @Test
  public void testarRemocao()
  {
    conjunto.limpar();

    if (!conjunto.vazio())
      Assert.fail("A coleção devia estar vazia.");

    for (int valor : vetor.valores())
      conjunto.adicionar(valor);

    Integer[] itens = new Integer[conjunto.total()];
    int       index = 0;

    for (Integer valor : vetor.aleatorio())
      if (conjunto.remover(valor))
        itens[index++] = valor;

    if (!conjunto.vazio())
      Assert.fail("A coleção devia estar vazia.");

    Assert.assertTrue(vetor.validarConteudo(itens));
  }

  @Test
  public void testarUniao_1()
  {
    IEmpilhavel<Integer> pilha = new PilhaVetorial<Integer>(10);

    conjunto.limpar();

    for (int i = 0; i < 10; i++)
    {
      conjunto.adicionar(i);
      pilha   .empilhar (i * 2);
    }

    conjunto.unir(pilha);
    Assert  .assertTrue
    (
      validarConjunto(new int[] {0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 12, 14, 16, 18})
    );
  }

  @Test
  public void testarUniao_2()
  {
    IEmpilhavel<Integer> pilha = new PilhaVetorial<Integer>(10);

    conjunto.limpar();

    for (int i = 0; i < 10; i++)
      conjunto.adicionar(i);

    conjunto.unir(pilha);
    Assert  .assertTrue
    (
      validarConjunto(new int[] {0, 1, 2, 3, 4, 5, 6, 7, 8, 9})
    );
  }

  @Test
  public void testarUniao_3()
  {
    IEmpilhavel<Integer> pilha = null;

    conjunto.limpar();

    for (int i = 0; i < 10; i++)
      conjunto.adicionar(i);

    conjunto.unir(pilha);
    Assert  .assertTrue
    (
      validarConjunto(new int[] {0, 1, 2, 3, 4, 5, 6, 7, 8, 9})
    );
  }

  @Test
  public void testarExcecao_1()
  {
    IEmpilhavel<Integer> pilha = new PilhaVetorial<Integer>(10);

    conjunto.limpar();

    for (int i = 0; i < 10; i++)
    {
      conjunto.adicionar(i);
      pilha   .empilhar (i * 2);
    }

    conjunto.excetuar(pilha);
    Assert  .assertTrue
    (
      validarConjunto(new int[] {1, 3, 5, 7, 9})
    );
  }

  @Test
  public void testarExcecao_2()
  {
    IEmpilhavel<Integer> pilha = new PilhaVetorial<Integer>(10);

    conjunto.limpar();

    for (int i = 0; i < 10; i++)
      conjunto.adicionar(i);

    conjunto.excetuar(pilha);
    Assert  .assertTrue
    (
      validarConjunto(new int[] {0, 1, 2, 3, 4, 5, 6, 7, 8, 9})
    );
  }

  @Test
  public void testarExcecao_3()
  {
    IEmpilhavel<Integer> pilha = null;

    conjunto.limpar();

    for (int i = 0; i < 10; i++)
      conjunto.adicionar(i);

    conjunto.excetuar(pilha);
    Assert  .assertTrue
    (
      validarConjunto(new int[] {0, 1, 2, 3, 4, 5, 6, 7, 8, 9})
    );
  }

  @Test
  public void testarInterseccao_1()
  {
    IEmpilhavel<Integer> pilha = new PilhaVetorial<Integer>(10);

    conjunto.limpar();

    for (int i = 0; i < 10; i++)
    {
      conjunto.adicionar(i);
      pilha   .empilhar (i * 2);
    }

    conjunto.interseccionar(pilha);
    Assert  .assertTrue
    (
      validarConjunto(new int[] {0, 2, 4, 6, 8})
    );
  }

  @Test
  public void testarInterseccao_2()
  {
    IEmpilhavel<Integer> pilha = new PilhaVetorial<Integer>(10);

    conjunto.limpar();

    for (int i = 0; i < 10; i++)
      conjunto.adicionar(i);

    conjunto.interseccionar(pilha);
    Assert  .assertTrue
    (
      validarConjunto(new int[] {})
    );
  }

  @Test
  public void testarInterseccao_3()
  {
    IEmpilhavel<Integer> pilha = null;

    conjunto.limpar();

    for (int i = 0; i < 10; i++)
      conjunto.adicionar(i);

    conjunto.interseccionar(pilha);
    Assert  .assertTrue
    (
      validarConjunto(new int[] {})
    );
  }
}
