package fundacao.colecoes;

// IMPORTAÇÕES - JUNIT //
import org.junit.Test;
import org.junit.Assert;

// IMPORTAÇÕES - FRACTAL LOTUS //
import fractallotus.fundacao.cerne.IEnumeravel;
import fractallotus.fundacao.cerne.IDicionarizavel;

public abstract class TestadorDicionario extends TestadorEnumeravel
{
  // ATRIBUTOS //
  private IDicionarizavel<Integer, Double> dicionario;

  // ASSISTENTES //
  protected abstract IDicionarizavel<Integer, Double> criarDicionario();

  protected IEnumeravel<Integer> criarColecao()
  {
    dicionario = criarDicionario();

    return dicionario.chaves();
  }

  protected abstract boolean validarInsercao();

  // CONSTRUTOR //
  public TestadorDicionario()
  {
    super();
  }

  // TESTES //
  @Test
  public void testarInsercao()
  {
    dicionario.limpar();

    if (!dicionario.vazio())
      Assert.fail("A coleção devia estar vazia.");

    for (int valor : vetor.valores())
      dicionario.adicionar(valor, valor / 100.0);

    Assert.assertTrue(validarInsercao());
  }

  @Test
  public void testarRemocao()
  {
    dicionario.limpar();

    if (!dicionario.vazio())
      Assert.fail("A coleção devia estar vazia.");

    for (int valor : vetor.valores())
      dicionario.adicionar(valor, valor / 100.0);

    Integer[] itens = new Integer[dicionario.total()];
    int       index = 0;

    for (Integer valor : vetor.aleatorio())
      if (dicionario.remover(valor))
        itens[index++] = valor;

    if (!dicionario.vazio())
      Assert.fail("A coleção devia estar vazia.");

    Assert.assertTrue(vetor.validarConteudo(itens));
  }
}
