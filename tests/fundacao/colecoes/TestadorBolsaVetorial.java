package fundacao.colecoes;

// IMPORTAÇÕES - FRACTAL LOTUS //
import fractallotus.fundacao.cerne.IEnumeravel;
import fractallotus.fundacao.colecoes.BolsaVetorial;

public class TestadorBolsaVetorial extends TestadorBolsa
{
  // CONSTRUTOR //
  public TestadorBolsaVetorial()
  {
    super();
  }

  protected IEnumeravel<Integer> criarColecao()
  {
    return new BolsaVetorial<Integer>(vetor.total());
  }
}
