package fundacao.colecoes;

// IMPORTAÇÕES - JUNIT //
import org.junit.Test;
import org.junit.Assert;

// IMPORTAÇÕES - FRACTAL LOTUS //
import fractallotus.fundacao.cerne.IEnumeravel;
import fractallotus.fundacao.cerne.IEnfileiravel;

public abstract class TestadorFila extends TestadorEnumeravel
{
  // ATRIBUTOS //
  protected final IEnfileiravel<Integer> fila;

  // CONSTRUTOR //
  public TestadorFila()
  {
    super();

    fila = (IEnfileiravel<Integer>)colecao;
  }

  protected boolean validarEnfileiramento(Integer[] itens)
  {
    return vetor.validarConteudo(itens);
  }

  protected boolean validarDesenfileiramento(Integer[] itens)
  {
    return vetor.validarValores(itens);
  }

  // TESTES //
  @Test
  public void testarEnfileiramento()
  {
    fila.limpar();

    if (!fila.vazio())
      Assert.fail("A coleção devia estar vazia.");

    for (int valor : vetor.valores())
      fila.enfileirar(valor);

    Assert.assertTrue(validarEnfileiramento(fila.toArray(Integer.class)));
  }

  @Test
  public void testarDesempilhamento()
  {
    fila.limpar();

    if (!fila.vazio())
      Assert.fail("A coleção devia estar vazia.");

    for (int valor : vetor.valores())
      fila.enfileirar(valor);

    Integer[] itens = new Integer[fila.total()];
    int       index = 0;

    while (!fila.vazio())
      itens[index++] = fila.desenfileirar();

    Assert.assertTrue(validarDesenfileiramento(itens));
  }
}
