package fundacao.colecoes;

// IMPORTAÇÕES - FRACTAL LOTUS //
import fractallotus.fundacao.cerne.IEnumeravel;
import fractallotus.fundacao.colecoes.ConjuntoOrdenadoEncadeado;

public class TestadorConjuntoOrdenadoEncadeado extends TestadorConjuntoOrdenado
{
  // ASSISTENTES //
  protected IEnumeravel<Integer> criarColecao()
  {
    super.criarColecao();

    return new ConjuntoOrdenadoEncadeado<Integer>(comparador);
  }

  // CONSTRUTOR //
  public TestadorConjuntoOrdenadoEncadeado()
  {
    super();
  }
}
