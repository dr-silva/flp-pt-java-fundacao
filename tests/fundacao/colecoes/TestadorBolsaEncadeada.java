package fundacao.colecoes;

// IMPORTAÇÕES - FRACTAL LOTUS //
import fractallotus.fundacao.cerne.IEnumeravel;
import fractallotus.fundacao.colecoes.BolsaEncadeada;

public class TestadorBolsaEncadeada extends TestadorBolsa
{
  // CONSTRUTOR //
  public TestadorBolsaEncadeada()
  {
    super();
  }

  protected IEnumeravel<Integer> criarColecao()
  {
    return new BolsaEncadeada<Integer>();
  }
}
