package fundacao.colecoes;

// IMPORTAÇÕES - FRACTAL LOTUS //
import fractallotus.fundacao.cerne.IEnumeravel;
import fractallotus.fundacao.colecoes.FilaPrioridadeMinimaVetorial;

public class TestadorFilaPrioridadeMinimaVetorial extends TestadorFilaPrioridadeMinima
{
  public TestadorFilaPrioridadeMinimaVetorial()
  {
    super();
  }

  protected IEnumeravel<Integer> criarColecao()
  {
    super.criarColecao();

    return new FilaPrioridadeMinimaVetorial<Integer>(vetor.total(), comparador);
  }
}
