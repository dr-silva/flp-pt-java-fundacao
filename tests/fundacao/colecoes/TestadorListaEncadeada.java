package fundacao.colecoes;

// IMPORTAÇÕES - FRACTAL LOTUS //
import fractallotus.fundacao.cerne.IEnumeravel;
import fractallotus.fundacao.colecoes.ListaEncadeada;

public class TestadorListaEncadeada extends TestadorLista
{
  public TestadorListaEncadeada()
  {
    super();
  }

  protected IEnumeravel<Integer> criarColecao()
  {
    return new ListaEncadeada<Integer>();
  }
}
