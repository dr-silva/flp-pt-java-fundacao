package fundacao.colecoes;

// IMPORTAÇÕES - FRACTAL LOTUS //
import fractallotus.fundacao.cerne.IEnumeravel;
import fractallotus.fundacao.cerne.FabricaComparador;
import fractallotus.fundacao.colecoes.FilaPrioridadeMaximaEncadeada;

public class TestadorFilaPrioridadeMaximaEncadeada extends TestadorFilaPrioridadeMaxima
{
  public TestadorFilaPrioridadeMaximaEncadeada()
  {
    super();
  }

  protected IEnumeravel<Integer> criarColecao()
  {
    super.criarColecao();

    return new FilaPrioridadeMaximaEncadeada<Integer>(comparador);
  }
}
