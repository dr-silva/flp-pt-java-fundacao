package fundacao.colecoes;

import fractallotus.fundacao.cerne.IEnumeravel;
import fractallotus.fundacao.cerne.IComparadorIgualdade;
import fractallotus.fundacao.cerne.FabricaComparadorIgualdade;

public abstract class TestadorConjuntoHash extends TestadorConjunto
{
  // ATRIBUTOS //
  protected IComparadorIgualdade<Integer> comparador;

  // ASSISTENTES //
  protected boolean validarInsercao()
  {
    return vetor.validarConteudo(colecao.toArray(Integer.class));
  }

  protected IEnumeravel<Integer> criarColecao()
  {
    comparador = FabricaComparadorIgualdade.<Integer>criar();

    return null;
  }

  // CONSTRUTOR //
  public TestadorConjuntoHash()
  {
    super();
  }
}
