package fundacao.colecoes;

// IMPORTAÇÕES - FRACTAL LOTUS //
import fractallotus.fundacao.cerne.IEnumeravel;
import fractallotus.fundacao.colecoes.ConjuntoHashSondado;

public class TestadorConjuntoHashSondado extends TestadorConjuntoHash
{
  // ASSISTENTES //
  protected IEnumeravel<Integer> criarColecao()
  {
    super.criarColecao();

    return new ConjuntoHashSondado<Integer>(vetor.total(), comparador);
  }

  // CONSTRUTOR //
  public TestadorConjuntoHashSondado()
  {
    super();
  }
}
