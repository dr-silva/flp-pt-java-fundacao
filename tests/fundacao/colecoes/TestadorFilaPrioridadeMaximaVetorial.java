package fundacao.colecoes;

// IMPORTAÇÕES - FRACTAL LOTUS //
import fractallotus.fundacao.cerne.IEnumeravel;
import fractallotus.fundacao.colecoes.FilaPrioridadeMaximaVetorial;

public class TestadorFilaPrioridadeMaximaVetorial extends TestadorFilaPrioridadeMaxima
{
  public TestadorFilaPrioridadeMaximaVetorial()
  {
    super();
  }

  protected IEnumeravel<Integer> criarColecao()
  {
    super.criarColecao();

    return new FilaPrioridadeMaximaVetorial<Integer>(vetor.total(), comparador);
  }
}
