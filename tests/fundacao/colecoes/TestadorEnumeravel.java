package fundacao.colecoes;

// IMPORTAÇÕES - FRACTAL LOTUS //
import fractallotus.fundacao.cerne.IEnumeravel;

public abstract class TestadorEnumeravel
{
  // ATRIBUTOS //
  private static final int TOTAL = 100;

  protected final VetorValidacao       vetor;
  protected final IEnumeravel<Integer> colecao;

  // CONSTRUTOR //
  public TestadorEnumeravel()
  {
    vetor   = new VetorValidacao(TOTAL);
    colecao = criarColecao();
  }

  // ASSISTENTES //
  protected abstract IEnumeravel<Integer> criarColecao();
}
