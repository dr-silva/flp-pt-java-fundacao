package fundacao.colecoes;

import fractallotus.fundacao.cerne.IEnumeravel;
import fractallotus.fundacao.cerne.IComparador;
import fractallotus.fundacao.cerne.FabricaComparador;

public abstract class TestadorBolsaOrdenada extends TestadorBolsa
{
  // ATRIBUTOS //
  protected IComparador<Integer> comparador;

  // CONSTRUTOR //
  public TestadorBolsaOrdenada()
  {
    super();
  }

  // ASSISTENTES //
  protected IEnumeravel<Integer> criarColecao()
  {
    comparador = FabricaComparador.<Integer>criar();

    return null;
  }

  protected boolean validar()
  {
    return vetor.validarAscendente(colecao.toArray(Integer.class));
  }
}
