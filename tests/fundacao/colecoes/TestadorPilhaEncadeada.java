package fundacao.colecoes;

// IMPORTAÇÕES - FRACTAL LOTUS //
import fractallotus.fundacao.cerne.IEnumeravel;
import fractallotus.fundacao.colecoes.PilhaEncadeada;

public class TestadorPilhaEncadeada extends TestadorPilha
{
  protected IEnumeravel<Integer> criarColecao()
  {
    return new PilhaEncadeada<Integer>();
  }
}
