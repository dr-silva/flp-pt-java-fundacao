package fundacao.colecoes;

// IMPORTAÇÕES - FRACTAL LOTUS //
import fractallotus.fundacao.cerne.IDicionarizavel;
import fractallotus.fundacao.colecoes.DicionarioHashSondado;

public class TestadorDicionarioHashSondado extends TestadorDicionarioHash
{
  // ASSISTENTES //
  protected IDicionarizavel<Integer, Double> criarDicionario()
  {
    return new DicionarioHashSondado<Integer, Double>(vetor.total(), comparador);
  }

  // CONSTRUTOR //
  public TestadorDicionarioHashSondado()
  {
    super();
  }
}
