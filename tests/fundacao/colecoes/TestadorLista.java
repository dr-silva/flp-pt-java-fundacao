package fundacao.colecoes;

// IMPORTAÇÕES - JUNIT //
import org.junit.Test;
import org.junit.Assert;

// IMPORTAÇÕES - FRACTAL LOTUS //
import fractallotus.fundacao.cerne.IEnumeravel;
import fractallotus.fundacao.cerne.IListavel;

public abstract class TestadorLista extends TestadorEnumeravel
{
  // ATRIBUTOS //
  protected final IListavel<Integer> lista;

  // CONSTRUTOR //
  public TestadorLista()
  {
    super();

    lista = (IListavel<Integer>)colecao;
  }

  // TESTES //
  @Test
  public void testarInsercao()
  {
    lista.limpar();

    if (!lista.vazio())
      Assert.fail("A coleção devia estar vazia.");

    for (int valor : vetor.valores())
      lista.adicionar(valor);

    Assert.assertTrue(vetor.validarValores(lista));
  }

  @Test
  public void testarRemocao()
  {
    lista.limpar();

    if (!lista.vazio())
      Assert.fail("A coleção devia estar vazia.");

    for (int valor : vetor.valores())
      lista.adicionar(valor);

    Integer[] itens = new Integer[lista.total()];
    int       index = 0;

    for (int i = colecao.total() - 1; i >= 0; i--)
    {
      itens[index++] = lista.item(i);

      lista.remover(i);
    }

    if (!lista.vazio())
      Assert.fail("A coleção devia estar vazia.");

    Assert.assertTrue(vetor.validarInverso(itens));
  }
}
