package fundacao.colecoes;

public abstract class TestadorFilaPrioridadeMaxima extends TestadorFilaPrioridade
{
  // CONSTRUTOR //
  public TestadorFilaPrioridadeMaxima()
  {
    super();
  }

  protected boolean validarDesenfileiramento(Integer[] itens)
  {
    return vetor.validarDescendente(itens);
  }
}
