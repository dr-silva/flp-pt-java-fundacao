package fundacao.colecoes;

public abstract class TestadorFilaPrioridadeMinima extends TestadorFilaPrioridade
{
  // CONSTRUTOR //
  public TestadorFilaPrioridadeMinima()
  {
    super();
  }

  protected boolean validarDesenfileiramento(Integer[] itens)
  {
    return vetor.validarAscendente(itens);
  }
}
