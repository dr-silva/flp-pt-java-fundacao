package fundacao.colecoes;

import java.util.Arrays;
import java.util.Iterator;

import fractallotus.fundacao.cerne.IEnumeravel;
import fractallotus.fundacao.cerne.IComparador;
import fractallotus.fundacao.cerne.FabricaComparador;
import fractallotus.fundacao.cerne.IValidadorOrdenacao;
import fractallotus.fundacao.ordenacoes.ValidadorOrdenacaoAscendente;
import fractallotus.fundacao.ordenacoes.ValidadorOrdenacaoDescendente;
import fractallotus.fundacao.ordenacoes.OrdenacaoAleatoria;

public class VetorValidacao
{
  // ATRIBUTOS //
  private int                  fTotal;
          Integer[]            fValores;
  private IComparador<Integer> fComparador;

  // PROPRIEDADES //
  public int total()
  {
    return fTotal;
  }

  public Iterable<Integer> valores()
  {
    Integer[] indices = new Integer[fTotal];

    for (int i = 0; i < fTotal; i++)
      indices[i] = i;

    return criarIterable(fValores, indices);
  }

  public Iterable<Integer> inverso()
  {
    Integer[] indices = new Integer[fTotal];

    for (int i = 0; i < fTotal; i++)
      indices[i] = fTotal - i - 1;

    return criarIterable(fValores, indices);
  }

  public Iterable<Integer> aleatorio()
  {
    Integer[] indices = new Integer[fTotal];

    for (int i = 0; i < fTotal; i++)
      indices[i] = i;

    (new OrdenacaoAleatoria<Integer>()).ordenar(indices);

    return criarIterable(fValores, indices);
  }

  // CONSTRUTOR //
  public VetorValidacao(int total)
  {
    fTotal      = total;
    fValores    = new Integer[total];
    fComparador = FabricaComparador.<Integer>criar();

    for (int i = 0; i < total; i++)
      fValores[i] = i + 1;

    (new OrdenacaoAleatoria<Integer>()).ordenar(fValores);
  }

  // ASSISTENTES //
  private static Iterable<Integer> criarIterable(Integer[] array, Integer[] index)
  {
    return new Iterable<Integer>()
    {
      public Iterator<Integer> iterator()
      {
        return criarIterator(array, index);
      }
    };
  }

  private static Iterator<Integer> criarIterator(Integer[] array, Integer[] index)
  {
    return new Iterator<Integer>()
    {
      private final Integer[] vetor   = array,
                              indices = index;
      private int             indice  = 0;

      public boolean hasNext()
      {
        return indice < indices.length;
      }

      public Integer next()
      {
        return vetor[indices[indice++].intValue()];
      }

      public void remove()
      {
        throw new UnsupportedOperationException();
      }
    };
  }

  // VALIDAÇÕES - ENUMERÁVEIS //
  public boolean validarAscendente(IEnumeravel<Integer> colecao)
  {
    return validarAscendente(colecao.toArray(Integer.class));
  }

  public boolean validarDescendente(IEnumeravel<Integer> colecao)
  {
    return validarDescendente(colecao.toArray(Integer.class));
  }

  public boolean validarValores(IEnumeravel<Integer> colecao)
  {
    return validarValores(colecao.toArray(Integer.class));
  }

  public boolean validarInverso(IEnumeravel<Integer> colecao)
  {
    return validarValores(colecao.toArray(Integer.class));
  }

  public boolean validarConteudo(IEnumeravel<Integer> colecao)
  {
    return validarConteudo(colecao.toArray(Integer.class));
  }

  // VALIDAÇÕES - ARRAYS //
  private boolean validarOrdem(Integer[] vetor, IValidadorOrdenacao<Integer> validador)
  {
    return (vetor.length == fTotal) && validador.validar(vetor);
  }

  private boolean validarValores(Integer[] vetor, boolean inverso)
  {
    boolean result = vetor.length == fTotal;

    for (int i = 0; result && (i < fTotal); i++)
      result = vetor[i] == fValores[inverso ? fTotal - i - 1 : i];

    return result;
  }

  public boolean validarAscendente(Integer[] vetor)
  {
    return validarOrdem(vetor, new ValidadorOrdenacaoAscendente<Integer>(fComparador));
  }

  public boolean validarDescendente(Integer[] vetor)
  {
    return validarOrdem(vetor, new ValidadorOrdenacaoDescendente<Integer>(fComparador));
  }

  public boolean validarValores(Integer[] vetor)
  {
    return validarValores(vetor, false);
  }

  public boolean validarInverso(Integer[] vetor)
  {
    return validarValores(vetor, true);
  }

  public boolean validarConteudo(Integer[] vetor)
  {
    if (vetor.length != fTotal)
      return false;

    Arrays.<Integer>sort(vetor, fComparador);

    boolean result = true;

    for (int i = 0; result && (i < fTotal); i++)
      result = vetor[i] == i + 1;

    return result;
  }
}