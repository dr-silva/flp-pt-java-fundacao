package fundacao.colecoes;

// IMPORTAÇÕES - FRACTAL LOTUS //
import fractallotus.fundacao.cerne.IEnumeravel;
import fractallotus.fundacao.colecoes.ListaVetorial;

public class TestadorListaVetorial extends TestadorLista
{
  public TestadorListaVetorial()
  {
    super();
  }

  protected IEnumeravel<Integer> criarColecao()
  {
    return new ListaVetorial<Integer>(vetor.total());
  }
}
