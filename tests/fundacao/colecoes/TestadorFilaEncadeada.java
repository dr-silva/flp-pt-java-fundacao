package fundacao.colecoes;

// IMPORTAÇÕES - FRACTAL LOTUS //
import fractallotus.fundacao.cerne.IEnumeravel;
import fractallotus.fundacao.colecoes.FilaEncadeada;

public class TestadorFilaEncadeada extends TestadorFila
{
  public TestadorFilaEncadeada()
  {
    super();
  }

  protected IEnumeravel<Integer> criarColecao()
  {
    return new FilaEncadeada<Integer>();
  }
}
