package fundacao.colecoes;

import fractallotus.fundacao.cerne.IEnumeravel;
import fractallotus.fundacao.cerne.IComparadorIgualdade;
import fractallotus.fundacao.cerne.FabricaComparadorIgualdade;

public abstract class TestadorDicionarioHash extends TestadorDicionario
{
  // ATRIBUTOS //
  protected IComparadorIgualdade<Integer> comparador;

  // ASSISTENTES //
  protected boolean validarInsercao()
  {
    return vetor.validarConteudo(colecao.toArray(Integer.class));
  }

  protected IEnumeravel<Integer> criarColecao()
  {
    comparador = FabricaComparadorIgualdade.<Integer>criar();

    return super.criarColecao();
  }

  // CONSTRUTOR //
  public TestadorDicionarioHash()
  {
    super();
  }
}
