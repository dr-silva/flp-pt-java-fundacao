package fundacao.colecoes;

// IMPORTAÇÕES - FRACTAL LOTUS //
import fractallotus.fundacao.cerne.IEnumeravel;
import fractallotus.fundacao.colecoes.BolsaOrdenadaVetorial;

public class TestadorBolsaOrdenadaVetorial extends TestadorBolsaOrdenada
{
  // CONSTRUTOR //
  public TestadorBolsaOrdenadaVetorial()
  {
    super();
  }

  protected IEnumeravel<Integer> criarColecao()
  {
    super.criarColecao();

    return new BolsaOrdenadaVetorial<Integer>(vetor.total(), comparador);
  }
}
