package fundacao.colecoes;

// IMPORTAÇÕES - FRACTAL LOTUS //
import fractallotus.fundacao.cerne.IEnumeravel;
import fractallotus.fundacao.colecoes.FilaVetorial;

public class TestadorFilaVetorial extends TestadorFila
{
  public TestadorFilaVetorial()
  {
    super();
  }

  protected IEnumeravel<Integer> criarColecao()
  {
    return new FilaVetorial<Integer>(vetor.total());
  }
}
