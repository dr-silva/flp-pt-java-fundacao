package fundacao.colecoes;

import fractallotus.fundacao.cerne.IEnumeravel;
import fractallotus.fundacao.cerne.IComparador;
import fractallotus.fundacao.cerne.FabricaComparador;

public abstract class TestadorConjuntoOrdenado extends TestadorConjunto
{
  // ATRIBUTOS //
  protected IComparador<Integer> comparador;

  // ASSISTENTES //
  protected boolean validarInsercao()
  {
    return vetor.validarAscendente(colecao.toArray(Integer.class));
  }

  protected IEnumeravel<Integer> criarColecao()
  {
    comparador = FabricaComparador.<Integer>criar();

    return null;
  }

  // CONSTRUTOR //
  public TestadorConjuntoOrdenado()
  {
    super();
  }
}
