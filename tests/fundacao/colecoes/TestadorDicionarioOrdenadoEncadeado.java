package fundacao.colecoes;

// IMPORTAÇÕES - FRACTAL LOTUS //
import fractallotus.fundacao.cerne.IDicionarizavel;
import fractallotus.fundacao.colecoes.DicionarioOrdenadoEncadeado;

public class TestadorDicionarioOrdenadoEncadeado extends TestadorDicionarioOrdenado
{
  // ASSISTENTES //
  protected IDicionarizavel<Integer, Double> criarDicionario()
  {
    return new DicionarioOrdenadoEncadeado<Integer, Double>(comparador);
  }

  // CONSTRUTOR //
  public TestadorDicionarioOrdenadoEncadeado()
  {
    super();
  }
}
