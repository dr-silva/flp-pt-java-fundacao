package fundacao.colecoes;

// IMPORTAÇÕES - FRACTAL LOTUS //
import fractallotus.fundacao.cerne.IEnumeravel;
import fractallotus.fundacao.colecoes.BolsaOrdenadaEncadeada;

public class TestadorBolsaOrdenadaEncadeada extends TestadorBolsaOrdenada
{
  // CONSTRUTOR //
  public TestadorBolsaOrdenadaEncadeada()
  {
    super();
  }

  protected IEnumeravel<Integer> criarColecao()
  {
    super.criarColecao();

    return new BolsaOrdenadaEncadeada<Integer>(comparador);
  }
}
