package fundacao.colecoes;

// IMPORTAÇÕES - FRACTAL LOTUS //
import fractallotus.fundacao.cerne.IEnumeravel;
import fractallotus.fundacao.colecoes.FilaPrioridadeMinimaEncadeada;

public class TestadorFilaPrioridadeMinimaEncadeada extends TestadorFilaPrioridadeMinima
{
  public TestadorFilaPrioridadeMinimaEncadeada()
  {
    super();
  }

  protected IEnumeravel<Integer> criarColecao()
  {
    super.criarColecao();

    return new FilaPrioridadeMinimaEncadeada<Integer>(comparador);
  }
}
