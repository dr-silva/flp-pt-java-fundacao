/*******************************************************************************
 *
 * Arquivo  : Primos.java
 * Autor    : Daniel Raimundo da Silva
 * Data     : 2021-07-01 <yyyy-mm-dd>
 * Licença  : Este arquivo está sob licença Non-Comercial Creative Commons
 * Descrição: Define os valores de números primos para o cálculo de hashes.
 *
 *******************************************************************************/
package fractallotus.fundacao.colecoes;

public class Primos
{
  // ATRIBUTOS //
  private static final int[] valores = {
            13, // índice = 04
            31, // índice = 05
            61, // índice = 06
           127, // índice = 07
           251, // índice = 08
           509, // índice = 09
          1021, // índice = 10
          2039, // índice = 11
          4093, // índice = 12
          8191, // índice = 13
         16381, // índice = 14
         32749, // índice = 15
         65521, // índice = 16
        131071, // índice = 17
        262139, // índice = 18
        524287, // índice = 19
       1048573, // índice = 20
       2097143, // índice = 21
       4194301, // índice = 22
       8388593, // índice = 23
      16777213, // índice = 24
      33554393, // índice = 25
      67108859, // índice = 26
     134217689, // índice = 27
     268435399, // índice = 28
     536870909, // índice = 29
    1073741789, // índice = 30
    2147483647  // índice = 31
  };

  // PROPRIEDADES //
  public static int valor(int indice) throws IndexOutOfBoundsException
  {
    if ((indice < 4) || (indice > 31))
      throw new IndexOutOfBoundsException();

    return valores[indice - 4];
  }

  // CONSTRUTOR //
  public Primos() throws Exception
  {
    throw new Exception();
  }
}