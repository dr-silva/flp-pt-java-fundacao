/*******************************************************************************
 *
 * Arquivo  : ArvoreRubronegraOrdenada.java
 * Autor    : Daniel Raimundo da Silva
 * Data     : 2021-06-30 <yyyy-mm-dd>
 * Licença  : Este arquivo está sob licença Non-Comercial Creative Commons
 * Descrição: Define a classe base das coleções que implementam uma árvore biná-
 *            ria balanceada ordenada.
 *
 *******************************************************************************/
package fractallotus.fundacao.colecoes;

import java.util.Iterator;
import fractallotus.fundacao.cerne.IOrdenavel;
import fractallotus.fundacao.cerne.IEnumeravel;
import fractallotus.fundacao.cerne.IComparador;
import fractallotus.fundacao.cerne.ParChaveValor;
import fractallotus.fundacao.recursos.Excecoes;

public abstract class ArvoreRubronegraOrdenada<TChave, TValor> extends ArvoreRubronegra<TChave, TValor> implements IOrdenavel<TChave>
{
  // PROPRIEDADES //
  public TChave minimo() throws IllegalStateException
  {
    if (vazio())
      throw Excecoes.colecaoVazia();

    return buscarNoMinimo(raiz()).chave;
  }

  public TChave maximo() throws IllegalStateException
  {
    if (vazio())
      throw Excecoes.colecaoVazia();

    return buscarNoMaximo(raiz()).chave;
  }

  // CONSTRUTORES //
  public ArvoreRubronegraOrdenada(IComparador<TChave> comparador)
  {
    super(comparador);
  }

  // ORDENÁVEL //
  public TChave piso(TChave chave) throws IndexOutOfBoundsException
  {
    No no = buscarNoPiso(chave);

    if (no == null)
      throw new IndexOutOfBoundsException
      (
        "O parâmetro \"chave\" é menor que a chave mínima."
      );

    return no.chave;
  }

  public TChave teto(TChave chave) throws IndexOutOfBoundsException
  {
    No no = buscarNoTeto(chave);

    if (no == null)
      throw new IndexOutOfBoundsException
      (
        "O parâmetro \"chave\" é maior que a chave máxima."
      );

    return no.chave;
  }

  public int indexar(TChave chave)
  {
    No no = buscarNoTeto(chave);

    return (no == null) ? total() : indexarNo(no) - 1;
  }

  public TChave desindexar(int indice) throws IndexOutOfBoundsException
  {
    No no = desindexarNo(indice);

    if (no == null)
      throw Excecoes.indiceForaLimites("indice", 0, total() - 1);

    return no.chave;
  }

  public int contar(TChave inferior, TChave superior)
  {
    No primeiro = buscarNoTeto(inferior),
       ultimo   = buscarNoPiso(superior);

    int total;

    if ((primeiro == null) || (ultimo == null))
      total = 0;
    else if (primeiro == ultimo)
      total = 1;
    else if (comparar(primeiro, ultimo) <= 0)
      total = indexarNo(ultimo) - indexarNo(primeiro) + 1;
    else
      total = indexarNo(ultimo) - indexarNo(primeiro) + 1;

    return total;
  }

  public IEnumeravel<TChave> obterChaves(TChave inferior, TChave superior)
  {
    No primeiro = buscarNoTeto(inferior),
       ultimo   = buscarNoPiso(superior);

    return new ColecaoChaves(this, primeiro, ultimo);
  }
}