/*******************************************************************************
 *
 * Arquivo  : FilaPrioridadeVetorial.java
 * Autor    : Daniel Raimundo da Silva
 * Data     : 2021-06-27 <yyyy-mm-dd>
 * Licença  : Este arquivo está sob licença Non-Comercial Creative Commons
 * Descrição: Define uma coleção vetorial que implementa a interface IEnfileirá-
 *            vel, mas os elementos têm alguma prioridade para que se enfileirem.
 *
 *******************************************************************************/
package fractallotus.fundacao.colecoes;

import java.lang.Class;
import fractallotus.fundacao.cerne.IEnfileiravel;
import fractallotus.fundacao.cerne.IComparador;
import fractallotus.fundacao.recursos.Excecoes;

public abstract class FilaPrioridadeVetorial<T> extends HeapVetorial<T> implements IEnfileiravel<T>
{
  // PROPRIEDADES //
  public boolean somenteLeitura() { return false; }

  // CONSTRUTORES //
  public FilaPrioridadeVetorial(int capacidade, IComparador<T> comparador)
  {
    super(capacidade, comparador);
  }

  // INTERFACE //
  public void enfileirar(T item) throws IllegalStateException
  {
    inserir(item);
  }

  public T desenfileirar() throws IllegalStateException
  {
    return excluir(0);
  }

  public T espiar() throws IllegalStateException
  {
    if (vazio())
      throw Excecoes.colecaoVazia();

    return item(0);
  }

  public void limpar()
  {
    excluirTodos();
  }
}