/*******************************************************************************
 *
 * Arquivo  : Vetorizacao.java
 * Autor    : Daniel Raimundo da Silva
 * Data     : 2021-06-27 <yyyy-mm-dd>
 * Licença  : Este arquivo está sob licença Non-Comercial Creative Commons
 * Descrição: Define uma classe básica para coleções vetoriais não ordenadas.
 *
 *******************************************************************************/
package fractallotus.fundacao.colecoes;

import java.util.Iterator;

import fractallotus.fundacao.cerne.IEnumeravel;
import fractallotus.fundacao.recursos.Excecoes;
import fractallotus.fundacao.extensores.ExtensorPadrao;

public abstract class Vetorizacao<T> implements IEnumeravel<T>
{
  // ATRIBUTOS //
  private int      fCapacidade;
  private Object[] fItens;

  // PROPRIEDADES //
  protected int capacidade()
  {
    return fCapacidade;
  }

  protected T item(int indice)
  {
    @SuppressWarnings("unchecked")
    T valor = (T)fItens[indice];

    return valor;
  }

  protected void item(int indice, T valor)
  {
    fItens[indice] = valor;
  }

  public abstract int     total();
  public abstract boolean vazio();
  public abstract boolean cheio();
  public abstract boolean somenteLeitura();

  // CONSTRUTORES //
  public Vetorizacao(int capacidade, int comprimento)
  {
    if (capacidade <= 0)
      throw Excecoes.argumentoNegativo("capacidade");
    if (comprimento < capacidade)
      throw Excecoes.argumentoNegativo("comprimento");

    fCapacidade = capacidade;
    fItens      = new Object[comprimento];
  }

  public Vetorizacao(int capacidade)
  {
    this(capacidade, capacidade);
  }

  // INTERFACE //
  public String toString()
  {
    return ExtensorPadrao.<T>toString(this);
  }

  public T[] toArray(Class<T> tipo)
  {
    return ExtensorPadrao.<T>toArray(tipo, this);
  }

  public abstract Iterator<T> iterator();

  protected Iterator<T> iterator(int indice, int total)
  {
    return new Iterator<T>()
    {
      private int _indice = indice;
      private int _total  = total;

      public boolean hasNext()
      {
        boolean result = (_total > 0);

        if (result)
        {
          _total--;
          _indice++;

          if (_indice >= fCapacidade)
            _indice = 0;
        }

        return result;
      }

      public T next()
      {
        return item(_indice);
      }

      public void remove()
      {
        throw new UnsupportedOperationException();
      }
    };
  }
}