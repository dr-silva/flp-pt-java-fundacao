/*******************************************************************************
 *
 * Arquivo  : VetorizacaoOrdenada.java
 * Autor    : Daniel Raimundo da Silva
 * Data     : 2021-06-28 <yyyy-mm-dd>
 * Licença  : Este arquivo está sob licença Non-Comercial Creative Commons
 * Descrição: Define uma classe básica para coleções vetoriais ordenadas.
 *
 *******************************************************************************/
package fractallotus.fundacao.colecoes;

import java.util.Iterator;
import fractallotus.fundacao.cerne.IOrdenavel;
import fractallotus.fundacao.cerne.IEnumeravel;
import fractallotus.fundacao.cerne.IComparador;
import fractallotus.fundacao.cerne.ParChaveValor;
import fractallotus.fundacao.recursos.Excecoes;
import fractallotus.fundacao.extensores.ExtensorPadrao;

public abstract class VetorizacaoOrdenada<TChave, TValor> implements IOrdenavel<TChave>
{
  // ATRIBUTOS //
  private int                 fCapacidade,
                              fTotal;
  private Object[]            fItens;
  private IComparador<TChave> fComparador;

  // PROPRIEDADES //
  protected ParChaveValor<TChave, TValor> par(int indice)
  {
    @SuppressWarnings("unchecked")
    ParChaveValor<TChave, TValor> par = (ParChaveValor<TChave, TValor>)fItens[indice];

    return par;
  }

  protected void par(int indice, TChave chave, TValor valor)
  {
    fItens[indice] = new ParChaveValor<TChave, TValor>(chave, valor);
  }

  protected TChave chave(int indice)
  {
    @SuppressWarnings("unchecked")
    ParChaveValor<TChave, TValor> par = (ParChaveValor<TChave, TValor>)fItens[indice];

    return par.chave();
  }

  protected TValor valor(int indice)
  {
    @SuppressWarnings("unchecked")
    ParChaveValor<TChave, TValor> par = (ParChaveValor<TChave, TValor>)fItens[indice];

    return par.valor();
  }

  protected int capacidade()
  {
    return fCapacidade;
  }

  public int     total() { return fTotal;                }
  public boolean vazio() { return fTotal == 0;           }
  public boolean cheio() { return fTotal == fCapacidade; }

  public abstract boolean somenteLeitura();

  public TChave minimo() throws IllegalStateException
  {
    if (vazio())
      throw Excecoes.colecaoVazia();

    return desindexar(0);
  }

  public TChave maximo() throws IllegalStateException
  {
    if (vazio())
      throw Excecoes.colecaoVazia();

    return desindexar(fTotal - 1);
  }

  // CONSTRUTORES //
  public VetorizacaoOrdenada(int capacidade, IComparador<TChave> comparador)
  {
    if (capacidade <= 0)
      throw Excecoes.argumentoNegativo("capacidade");

    if (comparador == null)
      throw Excecoes.argumentoNulo("comparador");

    fTotal      = 0;
    fCapacidade = capacidade;
    fItens      = new Object[capacidade];
    fComparador = comparador;
  }

  // ASSISTENTES //
  protected int comparar(TChave chave, int indice)
  {
    return fComparador.compare(chave, chave(indice));
  }

  protected void inserirValor(int indice, TChave chave, TValor valor)
  {
    for (int i = fTotal - 1; i >= indice; i--)
      fItens[i + 1] = fItens[i];

    par(indice, chave, valor);
    fTotal++;
  }

  protected void excluirValor(int indice)
  {
    for (int i = indice + 1; i < fTotal; i++)
      fItens[i - 1] = fItens[i];

    fTotal--;
  }

  protected void excluirTodos()
  {
    fTotal = 0;
  }

  protected boolean contemChave(TChave chave)
  {
    return (!vazio()) && (comparar(chave, indexar(chave)) == 0);
  }

  // INTERFACE - ORDENÁVEL //
  public TChave piso(TChave chave) throws IndexOutOfBoundsException
  {
    int i = indexar(chave);

    if (comparar(chave, i) < 0)
      i--;

    return desindexar(i);
  }

  public TChave teto(TChave chave) throws IndexOutOfBoundsException
  {
    int i = indexar(chave);

    return desindexar(i);
  }

  public int indexar(TChave chave)
  {
    int minimo = 0,
        maximo = fTotal - 1,
        result = -1;

    while ((minimo < maximo) && (result < 0))
    {
      int mediana    = (minimo + maximo) / 2,
          comparacao = comparar(chave, mediana);

      if (comparacao < 0)
        maximo = mediana - 1;
      else if (comparacao > 0)
        minimo = mediana + 1;
      else
        result = mediana;
    }

    if (result < 0)
      result = minimo;

    return result;
  }

  public TChave desindexar(int indice) throws IndexOutOfBoundsException
  {
    if ((indice < 0) || (indice >= fTotal))
      throw Excecoes.indiceForaLimites("indice", 0, fTotal - 1);

    return chave(indice);
  }

  public int contar(TChave inferior, TChave superior)
  {
    int idxInf = indexar(inferior),
        idxSup = indexar(superior);

    if ((idxSup > 0) && (comparar(superior, idxSup) == 0))
      idxSup--;

    int result = idxSup - idxInf;

    return (result >= 0) ? result + 1 : result - 1;
  }

  public IEnumeravel<TChave> obterChaves(TChave inferior, TChave superior)
  {
    return null;
  }

  // ENUMERADORES //
  protected abstract class Enumerador<T> implements Iterator<T>
  {
    private int indice,
                total,
                contador;
    private VetorizacaoOrdenada<TChave, TValor> colecao;

    public Enumerador
    (
      VetorizacaoOrdenada<TChave, TValor> colecao, int primeiro, int total
    )
    {
      this.colecao  = colecao;
      this.contador = (total >= 0) ? +1 : -1;
      this.total    = (total >= 0) ? +total : -total;
      this.indice   = primeiro - contador;
    }

    public boolean hasNext()
    {
      boolean result = total > 0;

      if (result)
      {
        indice += contador;
        total  -= 1;
      }

      return result;
    }

    public T next()
    {
      return next(colecao.par(indice));
    }

    public void remove()
    {
      throw new UnsupportedOperationException();
    }

    protected abstract T next(ParChaveValor<TChave, TValor> par);
  }

  protected class EnumeradorChaves extends Enumerador<TChave>
  {
    public EnumeradorChaves
    (
      VetorizacaoOrdenada<TChave, TValor> colecao, int primeiro, int total
    )
    {
      super(colecao, primeiro, total);
    }

    protected TChave next(ParChaveValor<TChave, TValor> par)
    {
      return par.chave();
    }
  }

  protected class EnumeradorValores extends Enumerador<TValor>
  {
    public EnumeradorValores
    (
      VetorizacaoOrdenada<TChave, TValor> colecao, int primeiro, int total
    )
    {
      super(colecao, primeiro, total);
    }

    protected TValor next(ParChaveValor<TChave, TValor> par)
    {
      return par.valor();
    }
  }

  protected class EnumeradorPares extends Enumerador<ParChaveValor<TChave, TValor>>
  {
    public EnumeradorPares
    (
      VetorizacaoOrdenada<TChave, TValor> colecao, int primeiro, int total
    )
    {
      super(colecao, primeiro, total);
    }

    protected ParChaveValor<TChave, TValor> next(ParChaveValor<TChave, TValor> par)
    {
      return par;
    }
  }

  // COLEÇÕES //
  protected abstract class ColecaoVetorial<T> implements IEnumeravel<T>
  {
    private int primeiro,
                ultimo;
    private VetorizacaoOrdenada<TChave, TValor> colecao;

    public ColecaoVetorial
    (
      VetorizacaoOrdenada<TChave, TValor> colecao, int primeiro, int ultimo
    )
    {
      this.primeiro = primeiro;
      this.ultimo   = ultimo;
      this.colecao  = colecao;
    }

    public String toString()
    {
      return ExtensorPadrao.<T>toString(this);
    }

    public T[] toArray(Class<T> tipo)
    {
      return ExtensorPadrao.<T>toArray(tipo, this);
    }

    public int total()
    {
      return ((primeiro < 0) || (ultimo < 0)) ? 0 : Math.abs(ultimo - primeiro) + 1;
    }

    public boolean vazio()
    {
      return (total() == 0);
    }

    public boolean cheio()
    {
      return (total() == colecao.total());
    }

    public boolean somenteLeitura()
    {
      return true;
    }

    public Iterator<T> iterator()
    {
      return iterator(colecao, primeiro, total());
    }

    protected abstract Iterator<T> iterator
    (
      VetorizacaoOrdenada<TChave, TValor> colecao, int primeiro, int total
    );
  }

  protected class ColecaoChaves extends ColecaoVetorial<TChave>
  {
    public ColecaoChaves
    (
      VetorizacaoOrdenada<TChave, TValor> colecao, int primeiro, int ultimo
    )
    {
      super(colecao, primeiro, ultimo);
    }

    protected Iterator<TChave> iterator
    (
      VetorizacaoOrdenada<TChave, TValor> colecao, int primeiro, int total
    )
    {
      return new EnumeradorChaves(colecao, primeiro, total);
    }
  }

  protected class ColecaoValores extends ColecaoVetorial<TValor>
  {
    public ColecaoValores
    (
      VetorizacaoOrdenada<TChave, TValor> colecao, int primeiro, int ultimo
    )
    {
      super(colecao, primeiro, ultimo);
    }

    protected Iterator<TValor> iterator
    (
      VetorizacaoOrdenada<TChave, TValor> colecao, int primeiro, int total
    )
    {
      return new EnumeradorValores(colecao, primeiro, total);
    }
  }
}