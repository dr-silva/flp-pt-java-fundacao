/*******************************************************************************
 *
 * Arquivo  : FilaEncadeada.java
 * Autor    : Daniel Raimundo da Silva
 * Data     : 2021-06-27 <yyyy-mm-dd>
 * Licença  : Este arquivo está sob licença Non-Comercial Creative Commons
 * Descrição: Define uma coleção de lista ligada que implementa a interface IEn-
 *            fileirável.
 *
 *******************************************************************************/
package fractallotus.fundacao.colecoes;

import java.lang.Class;
import java.util.Iterator;
import fractallotus.fundacao.cerne.IEnfileiravel;
import fractallotus.fundacao.recursos.Excecoes;

public class FilaEncadeada<T> extends Encadeamento<T> implements IEnfileiravel<T>
{
  // ATRIBUTOS //
  private Item fPrimeiro,
               fUltimo;

  // PROPRIEDADES //
  public boolean somenteLeitura() { return false; }

  // CONSTRUTORES //
  public FilaEncadeada()
  {
    super();

    fPrimeiro = fUltimo = null;
  }

  // INTERFACE //
  public void enfileirar(T valor) throws IllegalStateException
  {
    if (cheio())
      throw Excecoes.colecaoCheia();

    Item item = new Item(valor);

    if (!vazio())
      fUltimo.proximo = item;

    fUltimo = item;

    if (vazio())
      fPrimeiro = item;

    total(+1);
  }

  public T desenfileirar() throws IllegalStateException
  {
    if (vazio())
      throw Excecoes.colecaoVazia();

    Item item    = fPrimeiro;
    fPrimeiro    = item.proximo;
    item.proximo = null;

    total(-1);

    return item.valor;
  }

  public T espiar() throws IllegalStateException
  {
    if (vazio())
      throw Excecoes.colecaoVazia();

    return fPrimeiro.valor;
  }

  public void limpar()
  {
    fPrimeiro = fUltimo = null;
    total(-total());
  }

  public Iterator<T> iterator()
  {
    return iterator(fPrimeiro);
  }
}