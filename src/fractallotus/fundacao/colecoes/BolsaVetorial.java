/*******************************************************************************
 *
 * Arquivo  : BolsaVetorial.java
 * Autor    : Daniel Raimundo da Silva
 * Data     : 2021-06-27 <yyyy-mm-dd>
 * Licença  : Este arquivo está sob licença Non-Comercial Creative Commons
 * Descrição: Define uma coleção vetorial que implementa a interface IGuardável.
 *
 *******************************************************************************/
package fractallotus.fundacao.colecoes;

import java.lang.Class;
import java.util.Iterator;
import fractallotus.fundacao.cerne.IEnumeravel;
import fractallotus.fundacao.cerne.IGuardavel;
import fractallotus.fundacao.recursos.Excecoes;

public class BolsaVetorial<T> extends Vetorizacao<T> implements IGuardavel<T>
{
  // ATRIBUTOS //
  private int fTotal;

  // PROPRIEDADES //
  public int     total()          { return fTotal; }
  public boolean vazio()          { return fTotal == 0; }
  public boolean cheio()          { return fTotal == capacidade(); }
  public boolean somenteLeitura() { return false; }

  public T primeiro() throws IllegalStateException
  {
    if (vazio())
      throw Excecoes.colecaoVazia();

    return item(0);
  }

  public T ultimo() throws IllegalStateException
  {
    if (vazio())
      throw Excecoes.colecaoVazia();

    return item(fTotal - 1);
  }

  // CONSTRUTORES //
  public BolsaVetorial(int capacidade)
  {
    super(capacidade);

    fTotal = 0;
  }

  // INTERFACE //
  public void guardar(T item) throws IllegalStateException
  {
    if (cheio())
      throw Excecoes.colecaoCheia();

    item(fTotal++, item);
  }

  public void guardar(T[] itens) throws IllegalStateException
  {
    for (int i = 0; i < itens.length; i++)
      guardar(itens[i]);
  }

  public void guardar(IEnumeravel<T> itens) throws IllegalStateException
  {
    for (T item : itens)
      guardar(item);
  }

  public Iterator<T> iterator()
  {
    return iterator(-1, fTotal);
  }
}