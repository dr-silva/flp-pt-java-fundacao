/*******************************************************************************
 *
 * Arquivo  : ConjuntoHashEncadeado.java
 * Autor    : Daniel Raimundo da Silva
 * Data     : 2021-07-01 <yyyy-mm-dd>
 * Licença  : Este arquivo está sob licença Non-Comercial Creative Commons
 * Descrição: Define uma coleção do tipo hash encadeado que implementa a inter-
 *            face IDistinguível.
 *
 *******************************************************************************/
package fractallotus.fundacao.colecoes;

import java.util.Iterator;

import fractallotus.fundacao.cerne.IEnumeravel;
import fractallotus.fundacao.cerne.IComparadorIgualdade;
import fractallotus.fundacao.cerne.IDistinguivel;
import fractallotus.fundacao.extensores.ExtensorPadrao;

public class ConjuntoHashEncadeado<TChave> extends HashEncadeado<TChave, Object> implements IDistinguivel<TChave>
{
  // PROPRIEDADES //
  public boolean somenteLeitura() { return false; }

  // CONSTRUTORES //
  public ConjuntoHashEncadeado(IComparadorIgualdade<TChave> comparador)
  {
    super(comparador);
  }

  public ConjuntoHashEncadeado(int capacidade, IComparadorIgualdade<TChave> comparador)
  {
    super(capacidade, comparador);
  }

  // INTERFACE - ENUMERÁVEL //
  public String toString()
  {
    return ExtensorPadrao.<TChave>toString(this);
  }

  public TChave[] toArray(Class<TChave> tipo)
  {
    return ExtensorPadrao.<TChave>toArray(tipo, this);
  }

  public Iterator<TChave> iterator()
  {
    return new EnumeradorChaves(this);
  }

  // INTERFACE - DISTINGUÍVEL //
  public boolean adicionar(TChave chave)
  {
    boolean result = (!cheio()) && (buscarItem(chave) == null);

    if (result)
      inserirItem(chave, null);

    return result;
  }

  public boolean remover(TChave chave)
  {
    Item item = vazio() ? null : buscarItem(chave);

    if (item != null)
      excluirItem(item);

    return (item != null);
  }

  public boolean contem(TChave chave)
  {
    return (!vazio()) && (buscarItem(chave) != null);
  }

  public void limpar()
  {
    if (!vazio())
    {
      excluirTodos();
      redimensionar((byte)4);
    }
  }

  public void unir(IEnumeravel<TChave> colecao)
  {
    if (colecao != null)
      for (TChave valor : colecao)
        adicionar(valor);
  }

  public void excetuar(IEnumeravel<TChave> colecao)
  {
    if (colecao != null)
      for (TChave valor : colecao)
        remover(valor);
  }

  public void interseccionar(IEnumeravel<TChave> colecao)
  {
    int      total  = (colecao != null) ? colecao.total() : 0;
    Object[] novos  = new Object[Math.min(total(), total)];
    int      indice = -1;

    if (colecao != null)
      for (TChave valor : colecao)
      {
        Item item = buscarItem(valor);

        if (item != null)
        {
          extrairItem(item);
          novos[++indice] = item;
        }
      }

    excluirTodos();
    // A merda do JAVA não calcula log numa base específica, então
    // tem de calcular o log na base 2 por conversão de base usando
    // o logaritmo natural (ln = Math.log).
    redimensionar((byte)Math.floor(Math.log(indice + 1) / Math.log(2) + 1));

    while (indice >= 0)
    {
      @SuppressWarnings("unchecked")
      Item item = (Item)novos[indice--];

      inserirItem(item);
    }
  }
}