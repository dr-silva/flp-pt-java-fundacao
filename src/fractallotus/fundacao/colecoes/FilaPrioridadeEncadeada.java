/*******************************************************************************
 *
 * Arquivo  : FilaPrioridadeEncadeada.java
 * Autor    : Daniel Raimundo da Silva
 * Data     : 2021-06-27 <yyyy-mm-dd>
 * Licença  : Este arquivo está sob licença Non-Comercial Creative Commons
 * Descrição: Define uma coleção de lista ligada que implementa a interface IEn-
 *            fileirável, mas os elementos têm alguma prioridade para que se en-
 *            fileirem.
 *
 *******************************************************************************/
package fractallotus.fundacao.colecoes;

import java.lang.Class;
import fractallotus.fundacao.cerne.IEnfileiravel;
import fractallotus.fundacao.cerne.IComparador;
import fractallotus.fundacao.recursos.Excecoes;

public abstract class FilaPrioridadeEncadeada<T> extends HeapEncadeado<T> implements IEnfileiravel<T>
{
  // PROPRIEDADES //
  public boolean somenteLeitura() { return false; }

  // CONSTRUTORES //
  public FilaPrioridadeEncadeada(IComparador<T> comparador)
  {
    super(comparador);
  }

  // INTERFACE //
  public void enfileirar(T item) throws IllegalStateException
  {
    inserir(item);
  }

  public T desenfileirar() throws IllegalStateException
  {
    return excluir(0);
  }

  public T espiar() throws IllegalStateException
  {
    if (vazio())
      throw Excecoes.colecaoVazia();

    return primeiro().dado;
  }

  public void limpar()
  {
    excluirTodos();
  }
}