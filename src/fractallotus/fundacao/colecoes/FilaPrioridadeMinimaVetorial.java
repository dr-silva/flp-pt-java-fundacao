/*******************************************************************************
 *
 * Arquivo  : FilaPrioridadeMinimaVetorial.java
 * Autor    : Daniel Raimundo da Silva
 * Data     : 2021-06-27 <yyyy-mm-dd>
 * Licença  : Este arquivo está sob licença Non-Comercial Creative Commons
 * Descrição: Define uma coleção vetorial que implementa a interface IEnfileirá-
 *            vel, mas os elementos têm prioridade do maior para o menor para
 *            que se enfileirem.
 *
 *******************************************************************************/
package fractallotus.fundacao.colecoes;

import java.lang.Class;
import fractallotus.fundacao.cerne.IEnfileiravel;
import fractallotus.fundacao.cerne.IComparador;

public class FilaPrioridadeMinimaVetorial<T> extends FilaPrioridadeVetorial<T>
{
  // CONSTRUTORES //
  public FilaPrioridadeMinimaVetorial(int capacidade, IComparador<T> comparador)
  {
    super(capacidade, comparador);
  }

  // ASSISTENTES //
  public boolean eOrdenavel(int comparacao)
  {
    return (comparacao > 0);
  }
}