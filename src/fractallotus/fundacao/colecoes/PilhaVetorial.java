/*******************************************************************************
 *
 * Arquivo  : PilhaVetorial.java
 * Autor    : Daniel Raimundo da Silva
 * Data     : 2021-06-27 <yyyy-mm-dd>
 * Licença  : Este arquivo está sob licença Non-Comercial Creative Commons
 * Descrição: Define uma coleção vetorial que implementa a interface IEmpilhável.
 *
 *******************************************************************************/
package fractallotus.fundacao.colecoes;

import java.lang.Class;
import java.util.Iterator;
import fractallotus.fundacao.cerne.IEmpilhavel;
import fractallotus.fundacao.recursos.Excecoes;

public class PilhaVetorial<T> extends Vetorizacao<T> implements IEmpilhavel<T>
{
  // ATRIBUTOS //
  private int fTopo;

  // PROPRIEDADES //
  public int     total()          { return fTopo + 1; }
  public boolean vazio()          { return fTopo == -1; }
  public boolean cheio()          { return fTopo + 1 == capacidade(); }
  public boolean somenteLeitura() { return false; }

  // CONSTRUTORES //
  public PilhaVetorial(int capacidade)
  {
    super(capacidade);

    fTopo = -1;
  }

  // INTERFACE //
  public void empilhar(T item) throws IllegalStateException
  {
    if (cheio())
      throw Excecoes.colecaoCheia();

    item(++fTopo, item);
  }

  public T desempilhar() throws IllegalStateException
  {
    if (vazio())
      throw Excecoes.colecaoVazia();

    return item(fTopo--);
  }

  public T espiar() throws IllegalStateException
  {
    if (vazio())
      throw Excecoes.colecaoVazia();

    return item(fTopo);
  }

  public void limpar()
  {
    fTopo = -1;
  }

  public Iterator<T> iterator()
  {
    return iterator(-1, total());
  }
}