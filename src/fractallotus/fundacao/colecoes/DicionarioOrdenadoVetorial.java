/*******************************************************************************
 *
 * Arquivo  : DicionárioOrdenadaVetorial.java
 * Autor    : Daniel Raimundo da Silva
 * Data     : 2021-06-29 <yyyy-mm-dd>
 * Licença  : Este arquivo está sob licença Non-Comercial Creative Commons
 * Descrição: Define uma coleção vetorial ordenada que implementa a interface
 *            IDicionarizável.
 *
 *******************************************************************************/
package fractallotus.fundacao.colecoes;

import java.util.Iterator;

import fractallotus.fundacao.cerne.IEnumeravel;
import fractallotus.fundacao.cerne.IComparador;
import fractallotus.fundacao.cerne.IDicionarizavel;
import fractallotus.fundacao.cerne.ParChaveValor;
import fractallotus.fundacao.recursos.Excecoes;
import fractallotus.fundacao.extensores.ExtensorPadrao;

public class DicionarioOrdenadoVetorial<TChave, TValor> extends VetorizacaoOrdenada<TChave, TValor> implements IDicionarizavel<TChave, TValor>
{
  // ATRIBUTOS //
  private IEnumeravel<TChave> fChaves;
  private IEnumeravel<TValor> fValores;

  // PROPRIEDADES //
  public boolean somenteLeitura() { return false; }

  public TValor valor(TChave chave)
  {
    if (vazio())
      throw Excecoes.colecaoVazia();

    int indice = indexar(chave);

    if (comparar(chave, indice) != 0)
      throw Excecoes.chaveDicionario();

    return super.valor(indice);
  }

  public void valor(TChave chave, TValor valor)
  {
    adicionar(chave, valor);
  }

  public IEnumeravel<TChave> chaves()
  {
    return fChaves;
  }

  public IEnumeravel<TValor> valores()
  {
    return fValores;
  }

  // CONSTRUTORES //
  public DicionarioOrdenadoVetorial(int capacidade, IComparador<TChave> comparador)
  {
    super(capacidade, comparador);

    fChaves  = criarChaves (this);
    fValores = criarValores(this);
  }

  // INTERFACE - ENUMERÁVEL //
  public String toString()
  {
    return ExtensorPadrao.<ParChaveValor<TChave, TValor>>toString(this);
  }

  public ParChaveValor<TChave, TValor>[] toArray(Class<ParChaveValor<TChave, TValor>> tipo)
  {
    return ExtensorPadrao.<ParChaveValor<TChave, TValor>>toArray(tipo, this);
  }

  public Iterator<ParChaveValor<TChave, TValor>> iterator()
  {
    return new EnumeradorPares(this, 0, total());
  }

  // INTERFACE - DICIONARIZÁVEL //
  public void adicionar(TChave chave, TValor valor)
  {
    if (vazio())
      inserirValor(0, chave, valor);
    else
    {
      int indice     = indexar (chave),
          comparacao = comparar(chave, indice);

      if (comparacao == 0)
        super.par(indice, chave, valor);
      else
      {
        if (cheio())
          throw Excecoes.chaveDicionario();

        inserirValor(indice + (comparacao < 0 ? 0 : 1), chave, valor);
      }
    }
  }

  public void adicionar(IDicionarizavel<TChave, TValor> dicionario)
  {
    for (ParChaveValor<TChave, TValor> par : dicionario)
      adicionar(par.chave(), par.valor());
  }

  public boolean remover(TChave chave)
  {
    if (vazio())
      return false;

    int     indice = indexar (chave);
    boolean result = comparar(chave, indice) == 0;

    if (result)
      excluirValor(indice);

    return result;
  }

  public boolean contem(TChave chave)
  {
    return contemChave(chave);
  }

  public void limpar()
  {
    excluirTodos();
  }

  private IEnumeravel<TChave> criarChaves(DicionarioOrdenadoVetorial<TChave, TValor> dicionario)
  {
    return new IEnumeravel<TChave>()
    {
      // ATRIBUTOS //
      private DicionarioOrdenadoVetorial<TChave, TValor> colecao = dicionario;
      // PROPRIEDADES //
      public int     total() { return colecao.total(); }
      public boolean vazio() { return colecao.vazio(); }
      public boolean cheio() { return colecao.cheio(); }
      public boolean somenteLeitura() { return true; }

      public String           toString()                   { return ExtensorPadrao.<TChave>toString(this);       }
      public TChave[]         toArray (Class<TChave> tipo) { return ExtensorPadrao.<TChave>toArray (tipo, this); }
      public Iterator<TChave> iterator()                   { return new EnumeradorChaves(colecao, 0, total());   }
    };
  }

  private IEnumeravel<TValor> criarValores(DicionarioOrdenadoVetorial<TChave, TValor> dicionario)
  {
    return new IEnumeravel<TValor>()
    {
      // ATRIBUTOS //
      private DicionarioOrdenadoVetorial<TChave, TValor> colecao = dicionario;
      // PROPRIEDADES //
      public int     total() { return colecao.total(); }
      public boolean vazio() { return colecao.vazio(); }
      public boolean cheio() { return colecao.cheio(); }
      public boolean somenteLeitura() { return true; }

      public String           toString()                   { return ExtensorPadrao.<TValor>toString(this);       }
      public TValor[]         toArray (Class<TValor> tipo) { return ExtensorPadrao.<TValor>toArray (tipo, this); }
      public Iterator<TValor> iterator()                   { return new EnumeradorValores(colecao, 0, total());  }
    };
  }
}