/*******************************************************************************
 *
 * Arquivo  : ConjuntoOrdenadaVetorial.java
 * Autor    : Daniel Raimundo da Silva
 * Data     : 2021-06-29 <yyyy-mm-dd>
 * Licença  : Este arquivo está sob licença Non-Comercial Creative Commons
 * Descrição: Define uma coleção vetorial ordenada que implementa a interface
 *            IDistinguível.
 *
 *******************************************************************************/
package fractallotus.fundacao.colecoes;

import java.util.Iterator;

import fractallotus.fundacao.cerne.IEnumeravel;
import fractallotus.fundacao.cerne.IComparador;
import fractallotus.fundacao.cerne.IDistinguivel;
import fractallotus.fundacao.extensores.ExtensorPadrao;

public class ConjuntoOrdenadoVetorial<TChave> extends VetorizacaoOrdenada<TChave, Object> implements IDistinguivel<TChave>
{
  // PROPRIEDADES //
  public boolean somenteLeitura() { return false; }

  // CONSTRUTORES //
  public ConjuntoOrdenadoVetorial(int capacidade, IComparador<TChave> comparador)
  {
    super(capacidade, comparador);
  }

  // INTERFACE - ENUMERÁVEL //
  public String toString()
  {
    return ExtensorPadrao.<TChave>toString(this);
  }

  public TChave[] toArray(Class<TChave> tipo)
  {
    return ExtensorPadrao.<TChave>toArray(tipo, this);
  }

  public Iterator<TChave> iterator()
  {
    return new EnumeradorChaves(this, 0, total());
  }

  // INTERFACE - GUARDÁVEL //
  public boolean adicionar(TChave chave)
  {
    if (cheio())
      return false;
    else if (vazio())
    {
      inserirValor(0, chave, null);
      return true;
    }
    else
    {
      int indice     = indexar (chave),
          comparacao = comparar(chave, indice);

      if (comparacao < 0)
        inserirValor(indice, chave, null);
      else if (comparacao > 0)
        inserirValor(indice + 1, chave, null);

      return (comparacao != 0);
    }
  }

  public boolean remover(TChave chave)
  {
    if (vazio())
      return false;

    int     indice = indexar (chave);
    boolean result = comparar(chave, indice) == 0;

    if (result)
      excluirValor(indice);

    return result;
  }

  public boolean contem(TChave chave)
  {
    return contemChave(chave);
  }

  public void limpar()
  {
    excluirTodos();
  }

  public void unir(IEnumeravel<TChave> colecao)
  {
    if (colecao != null)
      for (TChave valor : colecao)
        adicionar(valor);
  }

  public void excetuar(IEnumeravel<TChave> colecao)
  {
    if (colecao != null)
      for (TChave valor : colecao)
        remover(valor);
  }

  public void interseccionar(IEnumeravel<TChave> colecao)
  {
    int      total  = (colecao != null) ? colecao.total() : 0;
    Object[] novos  = new Object[Math.min(total(), total)];
    int      indice = -1;

    if (colecao != null)
      for (TChave valor : colecao)
        if (contemChave(valor))
          novos[++indice] = valor;

    excluirTodos();

    while (indice >= 0)
    {
      @SuppressWarnings("unchecked")
      TChave valor = (TChave)novos[indice--];

      adicionar(valor);
    }
  }
}