/*******************************************************************************
 *
 * Arquivo  : BolsaOrdenadaEncadeada.java
 * Autor    : Daniel Raimundo da Silva
 * Data     : 2021-06-30 <yyyy-mm-dd>
 * Licença  : Este arquivo está sob licença Non-Comercial Creative Commons
 * Descrição: Define uma coleção encadeada (árvore binária) ordenada que imple0
 *            menta a interface IGuardável.
 *
 *******************************************************************************/
package fractallotus.fundacao.colecoes;

import java.util.Iterator;

import fractallotus.fundacao.cerne.IEnumeravel;
import fractallotus.fundacao.cerne.IComparador;
import fractallotus.fundacao.cerne.IGuardavel;
import fractallotus.fundacao.recursos.Excecoes;
import fractallotus.fundacao.extensores.ExtensorPadrao;

public class BolsaOrdenadaEncadeada<T> extends ArvoreRubronegraOrdenada<T, Object> implements IGuardavel<T>
{
  // PROPRIEDADES //
  public boolean somenteLeitura() { return false; }

  public T primeiro() throws IllegalStateException
  {
    return minimo();
  }

  public T ultimo() throws IllegalStateException
  {
    return maximo();
  }

  // CONSTRUTORES //
  public BolsaOrdenadaEncadeada(IComparador<T> comparador)
  {
    super(comparador);
  }

  // INTERFACE - ENUMERÁVEL //
  public String toString()
  {
    return ExtensorPadrao.<T>toString(this);
  }

  public T[] toArray(Class<T> tipo)
  {
    return ExtensorPadrao.<T>toArray(tipo, this);
  }

  public Iterator<T> iterator()
  {
    return new EnumeradorChaves
    (
      this, buscarNoMinimo(raiz()), buscarNoMaximo(raiz())
    );
  }

  // INTERFACE - GUARDÁVEL //
  public void guardar(T item) throws IllegalStateException
  {
    if (cheio())
      throw Excecoes.colecaoCheia();

    inserirNo( new No(item, null) );
  }

  public void guardar(T[] itens) throws IllegalStateException
  {
    for (int i = 0; i < itens.length; i++)
      guardar(itens[i]);
  }

  public void guardar(IEnumeravel<T> itens) throws IllegalStateException
  {
    for (T item : itens)
      guardar(item);
  }
}