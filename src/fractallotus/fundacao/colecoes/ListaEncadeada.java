/*******************************************************************************
 *
 * Arquivo  : ListaEncadeada.java
 * Autor    : Daniel Raimundo da Silva
 * Data     : 2021-07-03 <yyyy-mm-dd>
 * Licença  : Este arquivo está sob licença Non-Comercial Creative Commons
 * Descrição: Define uma coleção encadeada (árvore binária) indexada que imple-
 *            menta a interface IListável.
 *
 *******************************************************************************/
package fractallotus.fundacao.colecoes;

import java.util.Iterator;

import fractallotus.fundacao.cerne.IListavel;
import fractallotus.fundacao.cerne.IEnumeravel;
import fractallotus.fundacao.cerne.FabricaComparador;
import fractallotus.fundacao.recursos.Excecoes;
import fractallotus.fundacao.extensores.ExtensorPadrao;

public class ListaEncadeada<T> extends ArvoreRubronegra<Integer, T> implements IListavel<T>
{
  // ATRIBUTOS //
  private int fProximo;

  // PROPRIEDADES //
  public boolean somenteLeitura() { return false; }

  public T primeiro() throws IllegalStateException
  {
    if (vazio())
      throw Excecoes.colecaoVazia();

    return buscarNoMinimo(raiz()).valor;
  }

  public T ultimo() throws IllegalStateException
  {
    if (vazio())
      throw Excecoes.colecaoVazia();

    return buscarNoMaximo(raiz()).valor;
  }

  public T item(int indice) throws IndexOutOfBoundsException
  {
    No no = buscarNo(indice);

    if (no == null)
      throw Excecoes.chaveDicionario();

    return no.valor;
  }

  public void item(int indice, T valor) throws IndexOutOfBoundsException
  {
    inserir(indice, valor);
  }

  // CONSTRUTORES //
  public ListaEncadeada()
  {
    super(FabricaComparador.<Integer>criar());

    fProximo = 0;
  }

  // INTERFACE - ENUMERÁVEL //
  public String toString()
  {
    return ExtensorPadrao.<T>toString(this);
  }

  public T[] toArray(Class<T> tipo)
  {
    return ExtensorPadrao.<T>toArray(tipo, this);
  }

  public Iterator<T> iterator()
  {
    return new EnumeradorValores
    (
      this, buscarNoMinimo(raiz()), buscarNoMaximo(raiz())
    );
  }

  // INTERFACE - LISTÁVEL //
  public int adicionar(T item)
  {
    int result;

    if (cheio())
      result = -1;
    else
    {
      while (contem(fProximo))
        if (++fProximo < 0)
          fProximo = 0;

      result = fProximo++;
      inserir(result, item);
    }

    return result;
  }

  public int adicionar(T[] itens)
  {
    int result = -1;

    if (!cheio())
      for (int k = 0; k < itens.length; k++)
      {
        int i = adicionar(itens[k]);

        if (i < 0)
          break;
        if (result < 0)
          result = i;
      }

    return result;
  }

  public int adicionar(IEnumeravel<T> itens)
  {
    int result = -1;

    if (!cheio())
      for (T item : itens)
      {
        int i = adicionar(item);

        if (i < 0)
          break;
        if (result < 0)
          result = i;
      }

    return result;
  }

  public void inserir(int indice, T item) throws IndexOutOfBoundsException
  {
    if (indice < 0)
      throw Excecoes.indiceForaLimites("indice", 0, Integer.MAX_VALUE);

    No no = buscarNo(indice);

    if (no != null)
      no.valor = item;
    else
      inserirNo( new No(indice, item) );
  }

  public void inserir(int indice, T[] itens) throws IndexOutOfBoundsException
  {
    for (int i = indice; i < indice + itens.length; i++)
    {
      if (i < 0)
        break;

      inserir(i, itens[i - indice]);
    }
  }

  public void inserir(int indice, IEnumeravel<T> itens) throws IndexOutOfBoundsException
  {
    for (T item : itens)
    {
      if (indice < 0)
        break;

      inserir(indice++, item);
    }
  }

  public boolean remover(int indice)
  {
    No no = ((indice < 0) || vazio()) ? null : buscarNo(indice);

    if (no != null)
      excluirNo(no);

    return (no != null);
  }

  public boolean contem(int indice)
  {
    return (indice > 0) && (!vazio()) && (buscarNo(indice) != null);
  }

  public void limpar()
  {
    raiz(null);
  }

  public IEnumeravel<T> obterItens(int inferior, int superior) throws IndexOutOfBoundsException
  {
    if (inferior < 0)
      throw Excecoes.indiceForaLimites("inferior", 0, Integer.MAX_VALUE);
    if (superior < 0)
      throw Excecoes.indiceForaLimites("superior", 0, Integer.MAX_VALUE);

    No primeiro = (inferior <= superior) ? buscarNoTeto(inferior) : buscarNoTeto(superior),
       ultimo   = (inferior <= superior) ? buscarNoPiso(superior) : buscarNoPiso(inferior);

    return new ColecaoValores(this, primeiro, ultimo);
  }
}