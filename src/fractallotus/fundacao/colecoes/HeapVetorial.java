/*******************************************************************************
 *
 * Arquivo  : HeapVetorial.java
 * Autor    : Daniel Raimundo da Silva
 * Data     : 2021-06-27 <yyyy-mm-dd>
 * Licença  : Este arquivo está sob licença Non-Comercial Creative Commons
 * Descrição: Define uma classe básica para coleções vetoriais que funcionem co-
 *            mo um heap binário.
 *
 *******************************************************************************/
package fractallotus.fundacao.colecoes;

import java.util.Iterator;

import fractallotus.fundacao.cerne.IEnumeravel;
import fractallotus.fundacao.cerne.IComparador;
import fractallotus.fundacao.recursos.Excecoes;
import fractallotus.fundacao.extensores.ExtensorPadrao;

public abstract class HeapVetorial<T> implements IEnumeravel<T>
{
  // ATRIBUTOS //
  private int            fCapacidade,
                         fTotal;
  private Object[]       fItens;
  private IComparador<T> fComparador;

  // PROPRIEDADES //
  protected int capacidade()
  {
    return fCapacidade;
  }

  protected T item(int indice)
  {
    @SuppressWarnings("unchecked")
    T valor = (T)fItens[indice];

    return valor;
  }

  public int     total() { return fTotal;                }
  public boolean vazio() { return fTotal == 0;           }
  public boolean cheio() { return fTotal == fCapacidade; }

  public abstract boolean somenteLeitura();

  // CONSTRUTORES //
  public HeapVetorial(int capacidade, IComparador<T> comparador)
  {
    if (capacidade <= 0)
      throw Excecoes.argumentoNegativo("capacidade");

    if (comparador == null)
      throw Excecoes.argumentoNulo("comparador");

    fCapacidade = capacidade;
    fTotal      = 0;
    fItens      = new Object[capacidade];
    fComparador = comparador;
  }

  // ASSISTENTES //
  private void trocar(int i, int j)
  {
    if (i == j)
      return;

    Object temp = fItens[i];
    fItens[i]   = fItens[j];
    fItens[j]   = temp;
  }

  private void emergir(int indice)
  {
    int ancestral;

    while (indice > 0)
    {
      ancestral  = (indice + 1) / 2 - 1;

      if (eOrdenavel(ancestral, indice))
      {
        trocar(ancestral, indice);
        indice = ancestral;
      }
      else
        break;
    }
  }

  private void imergir(int indice)
  {
    int esquerda,
        direita,
        prioridade;

    while (indice < fTotal)
    {
      direita    = 2 * (indice + 1);
      esquerda   = direita - 1;
      prioridade = (esquerda < fTotal) && eOrdenavel(indice, esquerda) ? esquerda : indice;

      if ((direita < fTotal) && eOrdenavel(prioridade, direita))
        prioridade = direita;

      if (prioridade != indice)
      {
        trocar(indice, prioridade);
        indice = prioridade;
      }
      else
        break;
    }
  }

  protected abstract boolean eOrdenavel(int comparacao);

  protected boolean eOrdenavel(int esquerda, int direita)
  {
    T objE = item(esquerda),
      objD = item(direita);

    return eOrdenavel(fComparador.compare(objE, objD));
  }

  protected void inserir(T valor) throws IllegalStateException
  {
    if (cheio())
      throw Excecoes.colecaoCheia();

    fItens[fTotal] = valor;

    emergir(fTotal++);
  }

  protected T excluir(int indice) throws IllegalStateException
  {
    if (vazio())
      throw Excecoes.colecaoVazia();

    trocar(indice, --fTotal);

    T valor = item(fTotal);

    imergir(indice);

    return valor;
  }

  protected void excluirTodos()
  {
    fTotal = 0;
  }

  // INTERFACE //

  public String toString()
  {
    return ExtensorPadrao.<T>toString(this);
  }

  public T[] toArray(Class<T> tipo)
  {
    return ExtensorPadrao.<T>toArray(tipo, this);
  }

  public Iterator<T> iterator()
  {
    return new Iterator<T>()
    {
      private int indice = -1;

      public boolean hasNext()
      {
        return (++indice < fTotal);
      }

      public T next()
      {
        return item(indice);
      }

      public void remove()
      {
        throw new UnsupportedOperationException();
      }
    };
  }
}