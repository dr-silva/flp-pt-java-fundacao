/*******************************************************************************
 *
 * Arquivo  : HeapEncadeado.java
 * Autor    : Daniel Raimundo da Silva
 * Data     : 2021-06-27 <yyyy-mm-dd>
 * Licença  : Este arquivo está sob licença Non-Comercial Creative Commons
 * Descrição: Define uma classe básica para coleções de lista ligada que funcio-
 *            nem como um heap binário.
 *
 *******************************************************************************/
package fractallotus.fundacao.colecoes;

import java.util.Iterator;

import fractallotus.fundacao.cerne.IEnumeravel;
import fractallotus.fundacao.cerne.IComparador;
import fractallotus.fundacao.recursos.Excecoes;
import fractallotus.fundacao.extensores.ExtensorPadrao;

public abstract class HeapEncadeado<T> implements IEnumeravel<T>
{
  // ATRIBUTOS //
  private int            fTotal;
  private No             fPrimeiro,
                         fUltimo;
  private IComparador<T> fComparador;

  // PROPRIEDADES //
  protected No primeiro()
  {
    return fPrimeiro;
  }

  protected No ultimo()
  {
    return fUltimo;
  }

  public int     total() { return fTotal;                      }
  public boolean vazio() { return fPrimeiro == null;           }
  public boolean cheio() { return fTotal == Integer.MAX_VALUE; }

  public abstract boolean somenteLeitura();

  // CONSTRUTORES //
  public HeapEncadeado(IComparador<T> comparador)
  {
    if (comparador == null)
      throw Excecoes.argumentoNulo("comparador");

    fPrimeiro   = null;
    fUltimo     = null;
    fTotal      = 0;
    fComparador = comparador;
  }

  // ASSISTENTES //
  private void trocar(No a, No b)
  {
    if ((a == b) || (a == null) || (b == null))
      return;

    T temp = a.dado;
    a.dado = b.dado;
    b.dado = temp;
  }

  private void emergir(No no)
  {
    while (no != fPrimeiro)
      if (eOrdenavel(no.ancestral, no))
      {
        trocar(no.ancestral, no);
        no = no.ancestral;
      }
      else
        break;
  }

  private void imergir(No no)
  {
    No prioritario = null;

    while (no != fUltimo)
    {
      prioritario = (no.esquerda != null) && eOrdenavel(no, no.esquerda) ? no.esquerda : no;

      if ((no.direita != null) && eOrdenavel(prioritario, no.direita))
        prioritario = no.direita;

      if (prioritario != no)
      {
        trocar(no, prioritario);
        no = prioritario;
      }
      else
        break;
    }
  }

  private No obterMinimo(No no)
  {
    if (no == null)
      return null;

    while (no.esquerda != null)
      no = no.esquerda;

    return no;
  }

  protected abstract boolean eOrdenavel(int comparacao);

  protected boolean eOrdenavel(No esquerda, No direita)
  {
    return eOrdenavel
    (
      fComparador.compare(esquerda.dado, direita.dado)
    );
  }

  protected No buscarNo(int indice)
  {
    No no = null;

    if (indice <= 1)
      no = fPrimeiro;
    else
    {
      no = buscarNo(indice / 2);
      no = (indice % 2 == 0) ? no.esquerda : no.direita;
    }

    return no;
  }

  protected void inserir(T valor) throws IllegalStateException
  {
    if (cheio())
      throw Excecoes.colecaoCheia();

    No no = new No(valor);

    fUltimo = no;
    fTotal++;

    if (fPrimeiro == null)
      fPrimeiro = no;
    else
    {
      No pai = buscarNo(fTotal / 2);

      if (fTotal % 2 == 0)
        pai.esquerda = no;
      else
        pai.direita = no;

      no.ancestral = pai;
      emergir(no);
    }
  }

  protected T excluir(int indice) throws IllegalStateException
  {
    if (vazio())
      throw Excecoes.colecaoVazia();

    No no    = buscarNo(indice);
    T  valor = no.dado;

    trocar(no, fUltimo);

    if (fUltimo.ancestral != null)
    {
      if (fUltimo.ancestral.esquerda == fUltimo)
        fUltimo.ancestral.esquerda = null;
      else
        fUltimo.ancestral.direita = null;
    }
    else
      fPrimeiro = null;

    fUltimo = buscarNo(--fTotal);
    imergir(no);

    return valor;
  }

  protected void excluirTodos()
  {
    fPrimeiro = fUltimo = null;
  }

  // INTERFACE //
  public String toString()
  {
    return ExtensorPadrao.<T>toString(this);
  }

  public T[] toArray(Class<T> tipo)
  {
    return ExtensorPadrao.<T>toArray(tipo, this);
  }

  public Iterator<T> iterator()
  {
    return new Iterator<T>()
    {
      private No atual   = null,
                 proximo = obterMinimo(fPrimeiro);

      public boolean hasNext()
      {
        boolean result = (proximo != null);

        if (result)
        {
          atual   = proximo;
          proximo = proximo.ancestral;

          if ((proximo != null) && (proximo.esquerda == atual) && (proximo.direita != null))
            proximo = obterMinimo(proximo.direita);
        }

        return result;
      }

      public T next()
      {
        return atual.dado;
      }

      public void remove()
      {
        throw new UnsupportedOperationException();
      }
    };
  }

  // NÓ //
  protected class No
  {
    public T  dado;
    public No ancestral,
              esquerda,
              direita;

    public No(T dado)
    {
      this(dado, null, null, null);
    }

    public No(T dado, No ancestral)
    {
      this(dado, ancestral, null, null);
    }

    public No(T dado, No ancestral, No esquerda, No direita)
    {
      this.dado      = dado;
      this.ancestral = ancestral;
      this.esquerda  = esquerda;
      this.direita   = direita;
    }
  }
}