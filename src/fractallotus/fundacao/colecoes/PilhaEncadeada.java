/*******************************************************************************
 *
 * Arquivo  : PilhaEncadeada.java
 * Autor    : Daniel Raimundo da Silva
 * Data     : 2021-06-27 <yyyy-mm-dd>
 * Licença  : Este arquivo está sob licença Non-Comercial Creative Commons
 * Descrição: Define uma coleção de lista ligada que implementa a interface IEm-
 *            pilhável.
 *
 *******************************************************************************/
package fractallotus.fundacao.colecoes;

import java.lang.Class;
import java.util.Iterator;
import fractallotus.fundacao.cerne.IEmpilhavel;
import fractallotus.fundacao.recursos.Excecoes;

public class PilhaEncadeada<T> extends Encadeamento<T> implements IEmpilhavel<T>
{
  // ATRIBUTOS //
  private Item fPrimeiro;

  // PROPRIEDADES //
  public boolean somenteLeitura() { return false; }

  // CONSTRUTORES //
  public PilhaEncadeada()
  {
    super();

    fPrimeiro = null;
  }

  // INTERFACE //
  public void empilhar(T item) throws IllegalStateException
  {
    if (cheio())
      throw Excecoes.colecaoCheia();

    fPrimeiro = new Item(item, fPrimeiro);
    total(+1);
  }

  public T desempilhar() throws IllegalStateException
  {
    if (vazio())
      throw Excecoes.colecaoVazia();

    Item item    = fPrimeiro;
    fPrimeiro    = item.proximo;
    item.proximo = null;

    total(-1);

    return item.valor;
  }

  public T espiar() throws IllegalStateException
  {
    if (vazio())
      throw Excecoes.colecaoVazia();

    return fPrimeiro.valor;
  }

  public void limpar()
  {
    fPrimeiro = null;
    total(-total());
  }

  public Iterator<T> iterator()
  {
    return iterator(fPrimeiro);
  }
}