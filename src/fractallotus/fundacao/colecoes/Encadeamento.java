/*******************************************************************************
 *
 * Arquivo  : Encadeamento.java
 * Autor    : Daniel Raimundo da Silva
 * Data     : 2021-06-27 <yyyy-mm-dd>
 * Licença  : Este arquivo está sob licença Non-Comercial Creative Commons
 * Descrição: Define uma classe básica para coleções encadeadas não ordenadas.
 *
 *******************************************************************************/
package fractallotus.fundacao.colecoes;

import java.util.Iterator;

import fractallotus.fundacao.cerne.IEnumeravel;
import fractallotus.fundacao.extensores.ExtensorPadrao;

public abstract class Encadeamento<T> implements IEnumeravel<T>
{
  // ATRIBUTOS //
  private int fTotal;

  // PROPRIEDADES //
  public int     total() { return fTotal; }
  public boolean vazio() { return fTotal == 0; }
  public boolean cheio() { return fTotal == Integer.MAX_VALUE; }
  public abstract boolean somenteLeitura();

  protected void total(int valor)
  {
    fTotal += valor;
  }

  // CONSTRUTORES //
  public Encadeamento()
  {
    fTotal = 0;
  }

  // INTERFACE //
  public String toString()
  {
    return ExtensorPadrao.<T>toString(this);
  }

  public T[] toArray(Class<T> tipo)
  {
    return ExtensorPadrao.<T>toArray(tipo, this);
  }

  public abstract Iterator<T> iterator();

  protected Iterator<T> iterator(Item proximo)
  {
    return new Iterator<T>()
    {
      private Item _atual   = null,
                   _proximo = proximo;

      public boolean hasNext()
      {
        boolean result = (_proximo != null);

        if (result)
        {
          _atual   = _proximo;
          _proximo = _proximo.proximo;
        }

        return result;
      }

      public T next()
      {
        return _atual.valor;
      }

      public void remove()
      {
        throw new UnsupportedOperationException();
      }
    };
  }

  protected class Item
  {
    public T    valor;
    public Item proximo;

    public Item(T valor)
    {
      this(valor, null);
    }

    public Item(T valor, Item proximo)
    {
      this.valor   = valor;
      this.proximo = proximo;
    }
  }
}