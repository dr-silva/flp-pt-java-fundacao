/*******************************************************************************
 *
 * Arquivo  : ConjuntoOrdenadaEncadeado.java
 * Autor    : Daniel Raimundo da Silva
 * Data     : 2021-06-30 <yyyy-mm-dd>
 * Licença  : Este arquivo está sob licença Non-Comercial Creative Commons
 * Descrição: Define uma coleção encadeada (árvore binária) ordenada que imple-
 *            menta a interface IDistinguível.
 *
 *******************************************************************************/
package fractallotus.fundacao.colecoes;

import java.util.Iterator;

import fractallotus.fundacao.cerne.IEnumeravel;
import fractallotus.fundacao.cerne.IComparador;
import fractallotus.fundacao.cerne.IDistinguivel;
import fractallotus.fundacao.extensores.ExtensorPadrao;

public class ConjuntoOrdenadoEncadeado<TChave> extends ArvoreRubronegraOrdenada<TChave, Object> implements IDistinguivel<TChave>
{
  // PROPRIEDADES //
  public boolean somenteLeitura() { return false; }

  // CONSTRUTORES //
  public ConjuntoOrdenadoEncadeado(IComparador<TChave> comparador)
  {
    super(comparador);
  }

  // INTERFACE - ENUMERÁVEL //
  public String toString()
  {
    return ExtensorPadrao.<TChave>toString(this);
  }

  public TChave[] toArray(Class<TChave> tipo)
  {
    return ExtensorPadrao.<TChave>toArray(tipo, this);
  }

  public Iterator<TChave> iterator()
  {
    return new EnumeradorChaves
    (
      this, buscarNoMinimo(raiz()), buscarNoMaximo(raiz())
    );
  }

  // INTERFACE - GUARDÁVEL //
  public boolean adicionar(TChave chave)
  {
    boolean result = (!cheio()) && (buscarNo(chave) == null);

    if (result)
      inserirNo( new No(chave, null) );

    return result;
  }

  public boolean remover(TChave chave)
  {
    No no = vazio() ? null : buscarNo(chave);

    if (no != null)
      excluirNo(no);

    return (no != null);
  }

  public boolean contem(TChave chave)
  {
    return (!vazio()) && (buscarNo(chave) != null);
  }

  public void limpar()
  {
    raiz(null);
  }

  public void unir(IEnumeravel<TChave> colecao)
  {
    if (colecao != null)
      for (TChave valor : colecao)
        adicionar(valor);
  }

  public void excetuar(IEnumeravel<TChave> colecao)
  {
    if (colecao != null)
      for (TChave valor : colecao)
        remover(valor);
  }

  public void interseccionar(IEnumeravel<TChave> colecao)
  {
    int      total  = (colecao != null) ? colecao.total() : 0;
    Object[] novos  = new Object[Math.min(total(), total)];
    int      indice = -1;

    if (colecao != null)
      for (TChave valor : colecao)
        if (contem(valor))
          novos[++indice] = valor;

    limpar();

    while (indice >= 0)
    {
      @SuppressWarnings("unchecked")
      TChave valor = (TChave)novos[indice--];

      inserirNo( new No(valor, null) );
    }
  }
}