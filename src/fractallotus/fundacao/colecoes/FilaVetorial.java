/*******************************************************************************
 *
 * Arquivo  : FilaVetorial.java
 * Autor    : Daniel Raimundo da Silva
 * Data     : 2021-06-27 <yyyy-mm-dd>
 * Licença  : Este arquivo está sob licença Non-Comercial Creative Commons
 * Descrição: Define uma coleção vetorial que implementa a interface IEnfilerá-
 *            vel.
 *
 *******************************************************************************/
package fractallotus.fundacao.colecoes;

import java.lang.Class;
import java.util.Iterator;
import fractallotus.fundacao.cerne.IEnfileiravel;
import fractallotus.fundacao.recursos.Excecoes;

public class FilaVetorial<T> extends Vetorizacao<T> implements IEnfileiravel<T>
{
  // ATRIBUTOS //
  private int fInicio,
              fFim;

  // PROPRIEDADES //
  public int total()
  {
    return fFim - fInicio + (fFim >= fInicio ? 0 : capacidade());
  }
  public boolean vazio()
  {
    return fFim == fInicio;
  }
  public boolean cheio()
  {
    return (fInicio == fFim + 1) || (fFim == capacidade() && fInicio == 0);
  }
  public boolean somenteLeitura()
  {
    return false;
  }

  // CONSTRUTORES //
  public FilaVetorial(int capacidade)
  {
    super(capacidade, capacidade + 1);

    fInicio = fFim = 0;
  }

  // INTERFACE //
  public void enfileirar(T item) throws IllegalStateException
  {
    if (cheio())
      throw Excecoes.colecaoCheia();

    item(fFim++, item);

    if (fFim > capacidade())
      fFim = 0;
  }

  public T desenfileirar() throws IllegalStateException
  {
    if (vazio())
      throw Excecoes.colecaoVazia();

    T valor = item(fInicio++);

    if (fInicio > capacidade())
      fInicio = 0;

    return valor;
  }

  public T espiar() throws IllegalStateException
  {
    if (vazio())
      throw Excecoes.colecaoVazia();

    return item(fInicio);
  }

  public void limpar()
  {
    fInicio = fFim = 0;
  }

  public Iterator<T> iterator()
  {
    return iterator(fInicio - 1, total());
  }
}