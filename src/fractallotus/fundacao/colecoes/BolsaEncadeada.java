/*******************************************************************************
 *
 * Arquivo  : BolsaEncadeada.java
 * Autor    : Daniel Raimundo da Silva
 * Data     : 2021-06-27 <yyyy-mm-dd>
 * Licença  : Este arquivo está sob licença Non-Comercial Creative Commons
 * Descrição: Define uma coleção de lista ligada que implementa a interface I-
 *            Guardável.
 *
 *******************************************************************************/
package fractallotus.fundacao.colecoes;

import java.lang.Class;
import java.util.Iterator;
import fractallotus.fundacao.cerne.IEnumeravel;
import fractallotus.fundacao.cerne.IGuardavel;
import fractallotus.fundacao.recursos.Excecoes;

public class BolsaEncadeada<T> extends Encadeamento<T> implements IGuardavel<T>
{
  // ATRIBUTOS //
  private Item fPrimeiro,
               fUltimo;

  // PROPRIEDADES //
  public boolean somenteLeitura() { return false; }

  public T primeiro() throws IllegalStateException
  {
    if (vazio())
      throw Excecoes.colecaoVazia();

    return fPrimeiro.valor;
  }

  public T ultimo() throws IllegalStateException
  {
    if (vazio())
      throw Excecoes.colecaoVazia();

    return fPrimeiro.valor;
  }

  // CONSTRUTORES //
  public BolsaEncadeada()
  {
    super();

    fPrimeiro = fUltimo = null;
  }

  // INTERFACE //
  public void guardar(T valor) throws IllegalStateException
  {
    if (cheio())
      throw Excecoes.colecaoCheia();

    Item item = new Item(valor);

    if (!vazio())
      fUltimo.proximo = item;

    fUltimo = item;

    if (vazio())
      fPrimeiro = item;

    total(+1);
  }

  public void guardar(T[] itens) throws IllegalStateException
  {
    for (int i = 0; i < itens.length; i++)
      guardar(itens[i]);
  }

  public void guardar(IEnumeravel<T> itens) throws IllegalStateException
  {
    for (T item : itens)
      guardar(item);
  }

  public Iterator<T> iterator()
  {
    return iterator(fPrimeiro);
  }
}