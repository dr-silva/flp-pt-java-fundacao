/*******************************************************************************
 *
 * Arquivo  : FilaPrioridadeMaximaEncadeada.java
 * Autor    : Daniel Raimundo da Silva
 * Data     : 2021-06-27 <yyyy-mm-dd>
 * Licença  : Este arquivo está sob licença Non-Comercial Creative Commons
 * Descrição: Define uma coleção de árvore binária que implementa a interface
 *            IEnfileirável, mas os elementos têm prioridade do maior para o
 *            menor para que se enfileirem.
 *
 *******************************************************************************/
package fractallotus.fundacao.colecoes;

import fractallotus.fundacao.cerne.IComparador;

public class FilaPrioridadeMaximaEncadeada<T> extends FilaPrioridadeEncadeada<T>
{
  // CONSTRUTORES //
  public FilaPrioridadeMaximaEncadeada(IComparador<T> comparador)
  {
    super(comparador);
  }

  // ASSISTENTES //
  public boolean eOrdenavel(int comparacao)
  {
    return (comparacao < 0);
  }
}