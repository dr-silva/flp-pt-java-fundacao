/*******************************************************************************
 *
 * Arquivo  : HashEncadeado.java
 * Autor    : Daniel Raimundo da Silva
 * Data     : 2021-07-01 <yyyy-mm-dd>
 * Licença  : Este arquivo está sob licença Non-Comercial Creative Commons
 * Descrição: Define a classe base para as coleções do tipo hash com resolução
 *            por encadeamento.
 *
 *******************************************************************************/
package fractallotus.fundacao.colecoes;

import java.util.Random;
import java.util.Iterator;
import fractallotus.fundacao.cerne.IEnumeravel;
import fractallotus.fundacao.cerne.ParChaveValor;
import fractallotus.fundacao.cerne.IComparadorIgualdade;
import fractallotus.fundacao.recursos.Excecoes;
import fractallotus.fundacao.extensores.ExtensorPadrao;

public abstract class HashEncadeado<TChave, TValor>
{
  // ATRIBUTOS //
  private byte fLgCapacidade;
  private int  fCapacidade,
               fParcela,
               fFator,
               fPrimo,
               fTotal;

  private Random   fAleatorio;
  private Object[] fItens;

  private IComparadorIgualdade<TChave> fComparador;

  // PROPRIEDADES //
  private Item item(int indice)
  {
    @SuppressWarnings("unchecked")
    Item item = (Item)fItens[indice];

    return item;
  }

  public int     total() { return fTotal; }
  public boolean vazio() { return fTotal == 0; }
  public boolean cheio() { return fTotal == Integer.MAX_VALUE; }

  // CONSTRUTORES //
  public HashEncadeado(IComparadorIgualdade<TChave> comparador)
  {
    if (comparador == null)
      throw Excecoes.argumentoNulo("comparador");

    fAleatorio  = new Random();
    fComparador = comparador;

    redimensionar((byte)4);
  }

  public HashEncadeado(int capacidade, IComparadorIgualdade<TChave> comparador)
  {
    if (capacidade <= 0)
      throw Excecoes.argumentoNegativo("capacidade");
    if (comparador == null)
      throw Excecoes.argumentoNulo("comparador");

    fAleatorio  = new Random();
    fComparador = comparador;

    int dimensao = (int)Math.ceil(Math.log(capacidade / 3) / Math.log(2));

    redimensionar((byte)dimensao);
  }

  // ASSISTENTES //
  private int calcularHash(TChave chave)
  {
    int valor = fComparador.obterHash(chave) & 0x7FFFFFFF,
        fator = (fFator % 2 == 0) ? fFator + 1 : fFator,
        hash  = ((fator * valor + fParcela) % fPrimo) & (fCapacidade - 1);

    return hash;
  }

  // INTERFACE //
  protected void redimensionar(byte dimensao)
  {
    byte valor = (dimensao < 04) ? (byte)04 :
                 (dimensao > 31) ? (byte)31 : dimensao;

    if (valor == fLgCapacidade)
      return;

    Object[] antigos          = fItens;
    int      capacidadeAntiga = fCapacidade;
             fLgCapacidade    = valor;
             fCapacidade      = 1 << valor;
             fPrimo           = Primos.valor(valor + 1);
             fFator           = fAleatorio.nextInt(fPrimo) + 1;
             fParcela         = fAleatorio.nextInt(fPrimo);
             fItens           = new Object[fCapacidade];

    if (fTotal == 0)
      return;

    fTotal = 0;
    for (int i = 0; i < capacidadeAntiga; i++)
    {
      @SuppressWarnings("unchecked")
      Item item = (Item)antigos[i];

      while (item != null)
      {
        Item temp = item;
             item = item.ulterior;

        inserirItem(temp);
      }
    }
  }

  protected Item buscarItem(TChave chave)
  {
    int  indice = calcularHash(chave);
    Item item   = item(indice);

    while (item != null)
      if (fComparador.eIgual(chave, item.chave))
        break;
      else
        item = item.ulterior;

    return item;
  }

  protected void inserirItem(Item item)
  {
    if (item == null)
      return;

    item.indice   = calcularHash(item.chave);
    item.anterior = null;
    item.ulterior = item(item.indice);

    if (item.ulterior != null)
      item.ulterior.anterior = item;

    fItens[item.indice] = item;
    fTotal++;
  }

  protected void inserirItem(TChave chave, TValor valor)
  {
    if (fTotal > 3 * fCapacidade)
      redimensionar((byte)(fLgCapacidade + 1));

    inserirItem( new Item(chave, valor) );
  }

  protected void extrairItem(Item item)
  {
    if (item.anterior != null)
      item.anterior.ulterior = item.ulterior;
    if (item.ulterior != null)
      item.ulterior.anterior = item.anterior;
    if (fItens[item.indice] == item)
      fItens[item.indice] = item.ulterior;

    item.anterior = item.ulterior = null;
    fTotal--;
  }

  protected void excluirItem(Item item)
  {
    if (item == null)
      return;

    extrairItem(item);

    if (fTotal < fCapacidade / 8)
      redimensionar((byte)(fLgCapacidade - 1));
  }

  protected void excluirTodos()
  {
    fTotal = 0;
    fItens = new Object[fCapacidade];
  }

  // SUBCLASSES - ITEM //
  protected class Item
  {
    public TChave chave;
    public TValor valor;
    public int    indice;
    public Item   anterior,
                  ulterior;

    public Item(TChave chave, TValor valor)
    {
      this.chave    = chave;
      this.valor    = valor;
      this.indice   = 0;
      this.anterior = null;
      this.ulterior = null;
    }
  }

  // SUBCLASSES - ENUMERADORES //
  protected abstract class Enumerador<T> implements Iterator<T>
  {
    // ATRIBUTOS //
    private int  indice,
                 total;
    private Item atual;

    private HashEncadeado<TChave, TValor> colecao;

    // CONSTRUTOR //
    public Enumerador(HashEncadeado<TChave, TValor> colecao)
    {
      this.colecao = colecao;
      this.indice  = -1;
      this.total   = colecao.total();
      this.atual   = null;
    }

    // ASSISTENTE //
    protected abstract T next(Item proximo);

    // INTERFACE //
    public boolean hasNext()
    {
      boolean result = (total > 0);

      if (result)
      {
        do
        {
          atual = (atual == null) ? colecao.item(++indice) : atual.ulterior;
        } while (atual == null);

        total--;
      }

      return result;
    }

    public T next()
    {
      return next(atual);
    }

    public void remove()
    {
      throw new UnsupportedOperationException();
    }
  }

  protected class EnumeradorChaves extends Enumerador<TChave>
  {
    public EnumeradorChaves(HashEncadeado<TChave, TValor> colecao)
    {
      super(colecao);
    }

    protected TChave next(Item proximo)
    {
      return proximo.chave;
    }
  }

  protected class EnumeradorValores extends Enumerador<TValor>
  {
    public EnumeradorValores(HashEncadeado<TChave, TValor> colecao)
    {
      super(colecao);
    }

    protected TValor next(Item proximo)
    {
      return proximo.valor;
    }
  }

  protected class EnumeradorPares extends Enumerador<ParChaveValor<TChave, TValor>>
  {
    public EnumeradorPares(HashEncadeado<TChave, TValor> colecao)
    {
      super(colecao);
    }

    protected ParChaveValor<TChave, TValor> next(Item proximo)
    {
      return new ParChaveValor<TChave, TValor>(proximo.chave, proximo.valor);
    }
  }

  // SUBCLASSES - COLEÇÕES //
  public abstract class ColecaoHash<T> implements IEnumeravel<T>
  {
    // ATRIBUTOS //
    private HashEncadeado<TChave, TValor> colecao;

    // PROPRIEDADES //
    public int     total() { return colecao.total(); }
    public boolean vazio() { return colecao.vazio(); }
    public boolean cheio() { return colecao.cheio(); }
    public boolean somenteLeitura() { return true; }

    // CONSTRUTOR //
    public ColecaoHash(HashEncadeado<TChave, TValor> colecao)
    {
      this.colecao = colecao;
    }

    // ASSISTENTE //
    protected abstract Iterator<T> iterator(HashEncadeado<TChave, TValor> colecao);

    // INTERFACE //
    public String toString()
    {
      return ExtensorPadrao.<T>toString(this);
    }

    public T[] toArray(Class<T> tipo)
    {
      return ExtensorPadrao.<T>toArray(tipo, this);
    }

    public Iterator<T> iterator()
    {
      return iterator(colecao);
    }
  }

  public class ColecaoChaves extends ColecaoHash<TChave>
  {
    public ColecaoChaves(HashEncadeado<TChave, TValor> colecao)
    {
      super(colecao);
    }

    protected Iterator<TChave> iterator(HashEncadeado<TChave, TValor> colecao)
    {
      return new EnumeradorChaves(colecao);
    }
  }

  public class ColecaoValores extends ColecaoHash<TValor>
  {
    public ColecaoValores(HashEncadeado<TChave, TValor> colecao)
    {
      super(colecao);
    }

    protected Iterator<TValor> iterator(HashEncadeado<TChave, TValor> colecao)
    {
      return new EnumeradorValores(colecao);
    }
  }
}