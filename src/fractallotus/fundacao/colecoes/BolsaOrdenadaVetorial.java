/*******************************************************************************
 *
 * Arquivo  : BolsaOrdenadaVetorial.java
 * Autor    : Daniel Raimundo da Silva
 * Data     : 2021-06-27 <yyyy-mm-dd>
 * Licença  : Este arquivo está sob licença Non-Comercial Creative Commons
 * Descrição: Define uma coleção vetorial ordenada que implementa a interface
 *            IGuardável.
 *
 *******************************************************************************/
package fractallotus.fundacao.colecoes;

import java.util.Iterator;

import fractallotus.fundacao.cerne.IEnumeravel;
import fractallotus.fundacao.cerne.IComparador;
import fractallotus.fundacao.cerne.IGuardavel;
import fractallotus.fundacao.recursos.Excecoes;
import fractallotus.fundacao.extensores.ExtensorPadrao;

public class BolsaOrdenadaVetorial<T> extends VetorizacaoOrdenada<T, Object> implements IGuardavel<T>
{
  // PROPRIEDADES //
  public boolean somenteLeitura() { return false; }

  public T primeiro() throws IllegalStateException
  {
    return minimo();
  }

  public T ultimo() throws IllegalStateException
  {
    return maximo();
  }

  // CONSTRUTORES //
  public BolsaOrdenadaVetorial(int capacidade, IComparador<T> comparador)
  {
    super(capacidade, comparador);
  }

  // INTERFACE - ENUMERÁVEL //
  public String toString()
  {
    return ExtensorPadrao.<T>toString(this);
  }

  public T[] toArray(Class<T> tipo)
  {
    return ExtensorPadrao.<T>toArray(tipo, this);
  }

  public Iterator<T> iterator()
  {
    return new EnumeradorChaves(this, 0, total());
  }

  // INTERFACE - GUARDÁVEL //
  public void guardar(T item) throws IllegalStateException
  {
    if (cheio())
      throw Excecoes.colecaoCheia();
    else if (vazio())
      inserirValor(0, item, null);
    else
    {
      int i = indexar(item);

      inserirValor(comparar(item, i) < 0 ? i : i + 1, item, null);
    }
  }

  public void guardar(T[] itens) throws IllegalStateException
  {
    for (int i = 0; i < itens.length; i++)
      guardar(itens[i]);
  }

  public void guardar(IEnumeravel<T> itens) throws IllegalStateException
  {
    for (T item : itens)
      guardar(item);
  }
}