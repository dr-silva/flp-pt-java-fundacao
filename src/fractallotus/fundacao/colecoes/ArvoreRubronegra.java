/*******************************************************************************
 *
 * Arquivo  : ArvoreRubronegra.java
 * Autor    : Daniel Raimundo da Silva
 * Data     : 2021-06-30 <yyyy-mm-dd>
 * Licença  : Este arquivo está sob licença Non-Comercial Creative Commons
 * Descrição: Define a classe base das coleções que implementam uma árvore biná-
 *            ria balanceada.
 *
 *******************************************************************************/
package fractallotus.fundacao.colecoes;

import java.util.Iterator;
import fractallotus.fundacao.cerne.IEnumeravel;
import fractallotus.fundacao.cerne.IComparador;
import fractallotus.fundacao.cerne.ParChaveValor;
import fractallotus.fundacao.recursos.Excecoes;
import fractallotus.fundacao.extensores.ExtensorPadrao;

public abstract class ArvoreRubronegra<TChave, TValor>
{
  // CONSTANTES //
  private final boolean VERMELHO = false;
  private final boolean PRETO    = true;

  // ATRIBUTOS //
  private No                  fRaiz;
  private IComparador<TChave> fComparador;

  // PROPRIEDADES //
  protected No raiz()
  {
    return fRaiz;
  }
  protected void raiz(No valor)
  {
    fRaiz = valor;
  }

  public int     total() { return fRaiz != null ? fRaiz.tamanho : 0; }
  public boolean vazio() { return fRaiz == null; }
  public boolean cheio() { return total() == Integer.MAX_VALUE; }

  // CONSTRUTORES //
  public ArvoreRubronegra(IComparador<TChave> comparador)
  {
    if (comparador == null)
      throw Excecoes.argumentoNulo("comparador");

    fRaiz       = null;
    fComparador = comparador;
  }

  // ASSISTENTES //
  private boolean eVermelho(No no)
  {
    return (no != null) && (no.cor == VERMELHO);
  }

  private boolean ePreto(No no)
  {
    return (no == null) || (no.cor == PRETO);
  }

  private int contar(No no)
  {
    int result = 0;

    if (no != null)
    {
      result += 1;

      if (no.esquerda != null)
        result += no.esquerda.tamanho;
      else if (no.direita != null)
        result += no.direita.tamanho;
    }

    return result;
  }

  private void girarParaEsquerda(No no)
  {
    if ((no == null) || (no.direita == null))
      return;

    No pivo           = no  .direita;
       no  .direita   = pivo.esquerda;
       pivo.ancestral = no  .ancestral;

    if (pivo.esquerda != null)
      pivo.esquerda.ancestral = no;

    if (no.ancestral == null)
      fRaiz = pivo;
    else if (no == no.ancestral.esquerda)
      no.ancestral.esquerda = pivo;
    else
      no.ancestral.direita = pivo;

    pivo.esquerda  = no;
    pivo.tamanho   = no.tamanho;
    no  .ancestral = pivo;
    no  .tamanho   = contar(no);
  }

  private void girarParaDireita(No no)
  {
    if ((no == null) || (no.esquerda == null))
      return;

    No pivo           = no  .esquerda;
       no  .esquerda  = pivo.direita;
       pivo.ancestral = no  .ancestral;

    if (pivo.direita != null)
      pivo.direita.ancestral = no;

    if (no.ancestral == null)
      fRaiz = pivo;
    else if (no == no.ancestral.esquerda)
      no.ancestral.esquerda = pivo;
    else
      no.ancestral.direita = pivo;

    pivo.direita   = no;
    pivo.tamanho   = no.tamanho;
    no  .ancestral = pivo;
    no  .tamanho   = contar(no);
  }

  private void consertarInsercao(No no)
  {
    No avo = null,
       pai = no.ancestral;

    while (eVermelho(pai))
    {
      avo = pai.ancestral;

      if (avo == null)
        break;

      if (pai == avo.esquerda)
        no = consertarInsercaoEsquerda(no);
      else
        no = consertarInsercaoDireita(no);

      pai = no.ancestral;
    }

    fRaiz.cor = PRETO;
  }

  private No consertarInsercaoEsquerda(No no)
  {
    No result = null,
       tio    = no.ancestral.ancestral.direita;

    if (eVermelho(tio))
    {
      no .ancestral          .cor = PRETO;
      tio                    .cor = PRETO;
      no .ancestral.ancestral.cor = VERMELHO;
      result                      = no.ancestral.ancestral;
    }
    else
    {
      if (no == no.ancestral.direita)
      {
        no = no.ancestral;
        girarParaEsquerda(no);
      }

      no.ancestral          .cor = PRETO;
      no.ancestral.ancestral.cor = VERMELHO;

      girarParaDireita(no.ancestral.ancestral);

      result = no;
    }

    return result;
  }

  private No consertarInsercaoDireita(No no)
  {
    No result = null,
       tio    = no.ancestral.ancestral.esquerda;

    if (eVermelho(tio))
    {
      no .ancestral          .cor = PRETO;
      tio                    .cor = PRETO;
      no .ancestral.ancestral.cor = VERMELHO;
      result                      = no.ancestral.ancestral;
    }
    else
    {
      if (no == no.ancestral.esquerda)
      {
        no = no.ancestral;
        girarParaDireita(no);
      }

      no.ancestral          .cor = PRETO;
      no.ancestral.ancestral.cor = VERMELHO;

      girarParaEsquerda(no.ancestral.ancestral);

      result = no;
    }

    return result;
  }

  private void consertarExclusao(No no, No pai)
  {
    while ((no != fRaiz) && ePreto(no))
    {
      No neoPai = (no == null) ? pai : no.ancestral;

      if (no == neoPai.esquerda)
        no = consertarExclusaoEsquerda(no, neoPai);
      else
        no = consertarExclusaoDireita(no, neoPai);
    }

    if (no != null)
      no.cor = PRETO;
  }

  private No consertarExclusaoEsquerda(No no, No pai)
  {
    No result = null,
       irmao  = pai.direita;

    if (eVermelho(irmao))
    {
      irmao.cor = PRETO;
      pai  .cor = VERMELHO;

      girarParaEsquerda(pai);

      irmao = pai.direita;
    }

    if ((irmao == null) || (ePreto(irmao.esquerda) && ePreto(irmao.direita)))
    {
      if (irmao != null)
        irmao.cor = VERMELHO;

      result = pai;
    }
    else
    {
      if (ePreto(irmao.direita))
      {
        if (irmao.esquerda != null)
          irmao.esquerda.cor = PRETO;

        irmao.cor = VERMELHO;

        girarParaDireita(irmao);

        irmao = pai.direita;
      }

      irmao        .cor = pai.cor;
      pai          .cor = PRETO;
      irmao.direita.cor = PRETO;

      girarParaEsquerda(pai);

      result = fRaiz;
    }

    return result;
  }

  private No consertarExclusaoDireita(No no, No pai)
  {
    No result = null,
       irmao  = pai.esquerda;

    if (eVermelho(irmao))
    {
      irmao.cor = PRETO;
      pai  .cor = VERMELHO;

      girarParaDireita(pai);

      irmao = pai.esquerda;
    }

    if ((irmao == null) || (ePreto(irmao.esquerda) && ePreto(irmao.direita)))
    {
      if (irmao != null)
        irmao.cor = VERMELHO;

      result = pai;
    }
    else
    {
      if (ePreto(irmao.esquerda))
      {
        if (irmao.direita != null)
          irmao.direita.cor = PRETO;

        irmao.cor = VERMELHO;

        girarParaEsquerda(irmao);

        irmao = pai.esquerda;
      }

      irmao         .cor = pai.cor;
      pai           .cor = PRETO;
      irmao.esquerda.cor = PRETO;

      girarParaDireita(pai);

      result = fRaiz;
    }

    return result;
  }

  // BUSCADORES //
  protected int comparar(No esquerda, No direita)
  {
    return fComparador.compare(esquerda.chave, direita.chave);
  }

  protected No buscarNoMinimo(No no)
  {
    while ((no != null) && (no.esquerda != null))
      no = no.esquerda;

    return no;
  }

  protected No buscarNoMaximo(No no)
  {
    while ((no != null) && (no.direita != null))
      no = no.direita;

    return no;
  }

  protected No buscarNoPredecessor(No no)
  {
    if (no == null)
      return null;

    No result = null;

    if (no.esquerda != null)
      result = buscarNoMaximo(no.esquerda);
    else
    {
      result = no.ancestral;

      while ((result != null) && (no == result.esquerda))
      {
        no     = result;
        result = no.ancestral;
      }
    }

    return result;
  }

  protected No buscarNoSucessor(No no)
  {
    if (no == null)
      return null;

    No result = null;

    if (no.direita != null)
      result = buscarNoMinimo(no.direita);
    else
    {
      result = no.ancestral;

      while ((result != null) && (no == result.direita))
      {
        no     = result;
        result = no.ancestral;
      }
    }

    return result;
  }

  protected No buscarNo(TChave chave)
  {
    No result = null,
       no     = fRaiz;

    while ((no != null) && (result == null))
    {
      int comparacao = fComparador.compare(chave, no.chave);

      if (comparacao < 0)
        no = no.esquerda;
      else if (comparacao > 0)
        no = no.direita;
      else
        result = no;
    }

    return result;
  }

  protected No buscarNoPiso(TChave chave)
  {
    No result = null,
       no     = fRaiz;

    while (no != null)
    {
      int comparacao = fComparador.compare(chave, no.chave);

      if (comparacao < 0)
        no = no.esquerda;
      else
      {
        result = no;
        no     = (comparacao > 0) ? no.direita : null;
      }
    }

    return result;
  }

  protected No buscarNoTeto(TChave chave)
  {
    No result = null,
       no     = fRaiz;

    while (no != null)
    {
      int comparacao = fComparador.compare(chave, no.chave);

      if (comparacao > 0)
        no = no.direita;
      else
      {
        result = no;
        no     = (comparacao < 0) ? no.esquerda : null;
      }
    }

    return result;
  }

  // INDEXADORES //
  protected int indexarNo(No no)
  {
    int indice;

    if (no == null)
      indice = 0;
    else if (no.esquerda == null)
      indice = 1;
    else
      indice = no.esquerda.tamanho + 1;

    while ((no != null) && (no != fRaiz))
    {
      if (no == no.ancestral.direita)
        indice += (no.ancestral.esquerda == null) ? 1 : no.ancestral.esquerda.tamanho + 1;

      no = no.ancestral;
    }

    return indice;
  }

  protected No desindexarNo(int indice)
  {
    No result = null,
       no     = fRaiz;

    if ((indice < 1) || (indice > total()))
      return result;

    while (indice > 0)
    {
      int i = (no.esquerda == null) ? 1 : no.esquerda.tamanho + 1;

      if (indice == i)
      {
        result = no;
        break;
      }
      else if (indice < i)
        no = no.esquerda;
      else
      {
        indice -= i;
        no      = no.direita;
      }
    }

    return result;
  }

  // ESTRUTURAÇÃO //
  protected void inserirNo(No no)
  {
    if (no == null)
      return;

    No pai   = null,
       traco = fRaiz;

    while (traco != null)
    {
      traco.tamanho = traco.tamanho + 1;
      pai           = traco;
      traco         = (fComparador.compare(no.chave, traco.chave) < 0) ? traco.esquerda : traco.direita;
    }

    no.ancestral = pai;

    if (pai == null)
      fRaiz = no;
    else if (fComparador.compare(no.chave, pai.chave) < 0)
      pai.esquerda = no;
    else
      pai.direita = no;

    consertarInsercao(no);
  }

  protected void excluirNo(No no)
  {
    if (no == null)
      return;

    No extraido = ((no.esquerda == null) || (no.direita == null)) ? no : buscarNoSucessor(no),
       x        = (extraido.esquerda != null) ? extraido.esquerda : extraido.direita;

    if (x != null)
      x.ancestral = extraido.ancestral;

    if (extraido.ancestral == null)
      fRaiz = x;
    else if (extraido == extraido.ancestral.esquerda)
      extraido.ancestral.esquerda = x;
    else
      extraido.ancestral.direita = x;

    if (extraido != no)
    {
      no.chave = extraido.chave;
      no.valor = extraido.valor;
    }

    no = extraido.ancestral;
    while (no != null)
    {
      no.tamanho--;
      no = no.ancestral;
    }

    if (extraido.cor == PRETO)
      consertarExclusao(x, extraido.ancestral);

    extraido.ancestral = extraido.esquerda = extraido.direita = null;
  }

  // SUBCLASSES - NÓ //
  protected class No
  {
    public TChave  chave;
    public TValor  valor;
    public boolean cor;
    public int     tamanho;
    public No      ancestral,
                   esquerda,
                   direita;

    public No(TChave chave, TValor valor)
    {
      this.chave     = chave;
      this.valor     = valor;
      this.cor       = VERMELHO;
      this.tamanho   = 1;
      this.ancestral = null;
      this.esquerda  = null;
      this.direita   = null;
    }
  }

  // SUBCLASSES - ENUMERADORES //
  protected abstract class Enumerador<T> implements Iterator<T>
  {
    // ATRIBUTOS //
    private boolean ascendente;
    private No      atual,
                    proximo,
                    ultimo;
    private ArvoreRubronegra<TChave, TValor> colecao;

    public Enumerador
    (
      ArvoreRubronegra<TChave, TValor> colecao, No primeiro, No ultimo
    )
    {
      this.colecao = colecao;
      this.atual   = null;
      this.proximo = primeiro;
      this.ultimo  = ultimo;

      if ((primeiro == null) || (ultimo == null))
        this.proximo = this.ultimo = null;
      else
        this.ascendente = (colecao.fComparador.compare(primeiro.chave, ultimo.chave) <= 0);
    }

    public boolean hasNext()
    {
      boolean result = (proximo != null);

      if (result)
      {
        atual = proximo;

        if (proximo == ultimo)
          proximo = null;
        else if (ascendente)
          proximo = colecao.buscarNoSucessor(proximo);
        else
          proximo = colecao.buscarNoPredecessor(proximo);
      }

      return result;
    }

    public T next()
    {
      return next(atual);
    }

    public void remove()
    {
      throw new UnsupportedOperationException();
    }

    protected abstract T next(No atual);
  }

  protected class EnumeradorChaves extends Enumerador<TChave>
  {
    public EnumeradorChaves
    (
      ArvoreRubronegra<TChave, TValor> colecao, No primeiro, No ultimo
    )
    {
      super(colecao, primeiro, ultimo);
    }

    protected TChave next(No atual)
    {
      return atual.chave;
    }
  }

  protected class EnumeradorValores extends Enumerador<TValor>
  {
    public EnumeradorValores
    (
      ArvoreRubronegra<TChave, TValor> colecao, No primeiro, No ultimo
    )
    {
      super(colecao, primeiro, ultimo);
    }

    protected TValor next(No atual)
    {
      return atual.valor;
    }
  }

  protected class EnumeradorPares extends Enumerador<ParChaveValor<TChave, TValor>>
  {
    public EnumeradorPares
    (
      ArvoreRubronegra<TChave, TValor> colecao, No primeiro, No ultimo
    )
    {
      super(colecao, primeiro, ultimo);
    }

    protected ParChaveValor<TChave, TValor> next(No atual)
    {
      return new ParChaveValor<>(atual.chave, atual.valor);
    }
  }

  // SUBCLASSES - COLEÇÕES //
  protected abstract class ColecaoArborea<T> implements IEnumeravel<T>
  {
    private No primeiro,
               ultimo;
    private ArvoreRubronegra<TChave, TValor> colecao;

    // PROPRIEDADES //
    public int total()
    {
      if ((primeiro == null) || (ultimo == null))
        return 0;

      int idxPrimeiro = colecao.indexarNo(primeiro),
          idxUltimo   = colecao.indexarNo(ultimo);

      return Math.abs(idxUltimo - idxPrimeiro) + 1;
    }

    public boolean vazio()
    {
      return (total() == 0);
    }

    public boolean cheio()
    {
      return (total() == colecao.total());
    }

    public boolean somenteLeitura()
    {
      return true;
    }

    // CONSTRUTOR //
    public ColecaoArborea
    (
      ArvoreRubronegra<TChave, TValor> colecao, No primeiro, No ultimo
    )
    {
      this.primeiro = primeiro;
      this.ultimo   = ultimo;
      this.colecao  = colecao;
    }

    // INTERFACE //
    public String toString()
    {
      return ExtensorPadrao.<T>toString(this);
    }

    public T[] toArray(Class<T> tipo)
    {
      return ExtensorPadrao.<T>toArray(tipo, this);
    }

    public Iterator<T> iterator()
    {
      return iterator(colecao, primeiro, ultimo);
    }

    protected abstract Iterator<T> iterator
    (
      ArvoreRubronegra<TChave, TValor> colecao, No proximo, No ultimo
    );
  }

  protected class ColecaoChaves extends ColecaoArborea<TChave>
  {
    public ColecaoChaves
    (
      ArvoreRubronegra<TChave, TValor> colecao, No primeiro, No ultimo
    )
    {
      super(colecao, primeiro, ultimo);
    }

    protected Iterator<TChave> iterator
    (
      ArvoreRubronegra<TChave, TValor> colecao, No proximo, No ultimo
    )
    {
      return new EnumeradorChaves(colecao, proximo, ultimo);
    }
  }

  protected class ColecaoValores extends ColecaoArborea<TValor>
  {
    public ColecaoValores
    (
      ArvoreRubronegra<TChave, TValor> colecao, No primeiro, No ultimo
    )
    {
      super(colecao, primeiro, ultimo);
    }

    protected Iterator<TValor> iterator
    (
      ArvoreRubronegra<TChave, TValor> colecao, No proximo, No ultimo
    )
    {
      return new EnumeradorValores(colecao, proximo, ultimo);
    }
  }
}