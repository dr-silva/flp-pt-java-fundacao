/*******************************************************************************
 *
 * Arquivo  : HashSondado.java
 * Autor    : Daniel Raimundo da Silva
 * Data     : 2021-07-01 <yyyy-mm-dd>
 * Licença  : Este arquivo está sob licença Non-Comercial Creative Commons
 * Descrição: Define a classe base para as coleções do tipo hash com resolução
 *            por sondagem.
 *
 *******************************************************************************/
package fractallotus.fundacao.colecoes;

import java.util.Random;
import java.util.Iterator;
import fractallotus.fundacao.cerne.IEnumeravel;
import fractallotus.fundacao.cerne.ParChaveValor;
import fractallotus.fundacao.cerne.IComparadorIgualdade;
import fractallotus.fundacao.recursos.Excecoes;
import fractallotus.fundacao.extensores.ExtensorPadrao;

public abstract class HashSondado<TChave, TValor>
{
  // ATRIBUTOS //
  private static final int MAX_VALUE = 3 * 2 << 29;

  private byte fLgCapacidade;
  private int  fCapacidade,
               fPrimo,
               fTotal;

  private Item     fExcluido;
  private Random   fAleatorio;
  private Object[] fItens;

  private IComparadorIgualdade<TChave> fComparador;

  // PROPRIEDADES //
  private Item item(int indice)
  {
    @SuppressWarnings("unchecked")
    Item item = (Item)fItens[indice];

    return item;
  }

  public int     total() { return fTotal; }
  public boolean vazio() { return fTotal == 0; }
  public boolean cheio() { return fTotal == MAX_VALUE; }

  // CONSTRUTORES //
  public HashSondado(IComparadorIgualdade<TChave> comparador)
  {
    if (comparador == null)
      throw Excecoes.argumentoNulo("comparador");

    fExcluido   = new Item(null, null);
    fAleatorio  = new Random();
    fComparador = comparador;

    redimensionar((byte)4);
  }

  public HashSondado(int capacidade, IComparadorIgualdade<TChave> comparador)
  {
    if (capacidade <= 0)
      throw Excecoes.argumentoNegativo("capacidade");
    if (comparador == null)
      throw Excecoes.argumentoNulo("comparador");

    fExcluido   = new Item(null, null);
    fAleatorio  = new Random();
    fComparador = comparador;

    int dimensao = (int)Math.ceil(Math.log(4 * capacidade / 3) / Math.log(2));

    redimensionar((byte)dimensao);
  }

  // ASSISTENTES //
  private int calcularHash(int chave, int indice)
  {
    int valor   = chave & 0x7FFFFFFF,
        parcela = valor & (fCapacidade - 1), // valor mod capacidade
        fator   = 1 + (valor % fPrimo);

    return (parcela + indice * (fator % 2 == 0 ? fator + 1 : fator)) & (fCapacidade - 1);
  }

  private boolean haItem(int indice)
  {
    return (fItens[indice] != null) && (fItens[indice] != fExcluido);
  }

  // INTERFACE //
  protected void redimensionar(byte dimensao)
  {
    byte valor = (dimensao < 04) ? (byte)04 :
                 (dimensao > 31) ? (byte)31 : dimensao;

    if (valor == fLgCapacidade)
      return;

    Object[] antigos          = fItens;
    int      capacidadeAntiga = fCapacidade;
             fLgCapacidade    = valor;
             fCapacidade      = 1 << valor;
             fPrimo           = Primos.valor(valor);
             fItens           = new Object[fCapacidade];

    if (fTotal == 0)
      return;

    fTotal = 0;
    for (int i = 0; i < capacidadeAntiga; i++)
      if ((antigos[i] != null) &&(antigos[i] != fExcluido))
      {
        @SuppressWarnings("unchecked")
        Item item = (Item)antigos[i];

        inserirItem(item);
      }
  }

  protected Item buscarItem(TChave chave)
  {
    int  valor  = fComparador.obterHash(chave),
         indice = 0,
         hash   = calcularHash(valor, indice);
    Item item   = null;

    while ((fItens[hash] != null) && (indice < fCapacidade))
    {
      if ((fItens[hash] != fExcluido) && fComparador.eIgual(chave, item(hash).chave))
      {
        item = item(hash);
        break;
      }

      hash = calcularHash(valor, ++indice);
    }

    return item;
  }

  protected void inserirItem(Item item)
  {
    if (item == null)
      return;

    int valor  = fComparador.obterHash(item.chave),
        indice = 0,
        hash;

    do
    {
      hash = calcularHash(valor, indice++);

      if (!haItem(hash))
      {
        item.hash    = hash;
        fItens[hash] = item;
        fTotal++;
        break;
      }
    } while (indice < fCapacidade);
  }

  protected void inserirItem(TChave chave, TValor valor)
  {
    if (fTotal > 3 * fCapacidade / 4)
      redimensionar((byte)(fLgCapacidade + 1));

    inserirItem( new Item(chave, valor) );
  }

  protected void extrairItem(Item item)
  {
    if ((item != null) && (item != fExcluido))
    {
      fItens[item.hash] = fExcluido;
      fTotal--;
    }
  }

  protected void excluirItem(Item item)
  {
    extrairItem(item);

    if (fTotal < fCapacidade / 8)
      redimensionar((byte)(fLgCapacidade - 1));
  }

  protected void excluirTodos()
  {
    fTotal = 0;
    fItens = new Object[fCapacidade];
  }

  // SUBCLASSES - ITEM //
  protected class Item
  {
    public TChave chave;
    public TValor valor;
    public int    hash;

    public Item(TChave chave, TValor valor)
    {
      this.chave = chave;
      this.valor = valor;
      this.hash  = -1;
    }
  }

  // SUBCLASSES - ENUMERADORES //
  protected abstract class Enumerador<T> implements Iterator<T>
  {
    // ATRIBUTOS //
    private int  indice,
                 total;
    private Item atual;

    private HashSondado<TChave, TValor> colecao;

    // CONSTRUTOR //
    public Enumerador(HashSondado<TChave, TValor> colecao)
    {
      this.colecao = colecao;
      this.indice  = -1;
      this.total   = colecao.total();
    }

    // ASSISTENTE //
    protected abstract T next(Item proximo);

    // INTERFACE //
    public boolean hasNext()
    {
      boolean result = (total > 0);

      if (result)
      {
        while (!colecao.haItem(++indice)) ;

        total--;
      }

      return result;
    }

    public T next()
    {
      return next(colecao.item(indice));
    }

    public void remove()
    {
      throw new UnsupportedOperationException();
    }
  }

  protected class EnumeradorChaves extends Enumerador<TChave>
  {
    public EnumeradorChaves(HashSondado<TChave, TValor> colecao)
    {
      super(colecao);
    }

    protected TChave next(Item proximo)
    {
      return proximo.chave;
    }
  }

  protected class EnumeradorValores extends Enumerador<TValor>
  {
    public EnumeradorValores(HashSondado<TChave, TValor> colecao)
    {
      super(colecao);
    }

    protected TValor next(Item proximo)
    {
      return proximo.valor;
    }
  }

  protected class EnumeradorPares extends Enumerador<ParChaveValor<TChave, TValor>>
  {
    public EnumeradorPares(HashSondado<TChave, TValor> colecao)
    {
      super(colecao);
    }

    protected ParChaveValor<TChave, TValor> next(Item proximo)
    {
      return new ParChaveValor<TChave, TValor>(proximo.chave, proximo.valor);
    }
  }

  // SUBCLASSES - COLEÇÕES //
  public abstract class ColecaoHash<T> implements IEnumeravel<T>
  {
    // ATRIBUTOS //
    private HashSondado<TChave, TValor> colecao;

    // PROPRIEDADES //
    public int     total() { return colecao.total(); }
    public boolean vazio() { return colecao.vazio(); }
    public boolean cheio() { return colecao.cheio(); }
    public boolean somenteLeitura() { return true; }

    // CONSTRUTOR //
    public ColecaoHash(HashSondado<TChave, TValor> colecao)
    {
      this.colecao = colecao;
    }

    // ASSISTENTE //
    protected abstract Iterator<T> iterator(HashSondado<TChave, TValor> colecao);

    // INTERFACE //
    public String toString()
    {
      return ExtensorPadrao.<T>toString(this);
    }

    public T[] toArray(Class<T> tipo)
    {
      return ExtensorPadrao.<T>toArray(tipo, this);
    }

    public Iterator<T> iterator()
    {
      return iterator(colecao);
    }
  }

  public class ColecaoChaves extends ColecaoHash<TChave>
  {
    public ColecaoChaves(HashSondado<TChave, TValor> colecao)
    {
      super(colecao);
    }

    protected Iterator<TChave> iterator(HashSondado<TChave, TValor> colecao)
    {
      return new EnumeradorChaves(colecao);
    }
  }

  public class ColecaoValores extends ColecaoHash<TValor>
  {
    public ColecaoValores(HashSondado<TChave, TValor> colecao)
    {
      super(colecao);
    }

    protected Iterator<TValor> iterator(HashSondado<TChave, TValor> colecao)
    {
      return new EnumeradorValores(colecao);
    }
  }

  private TChave chave(int indice)
  {
    Item item = item(indice);

    return (item != null) ? item.chave : null;
  }
}