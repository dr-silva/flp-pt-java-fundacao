/*******************************************************************************
 *
 * Arquivo  : DicionárioOrdenadaEncadeado.java
 * Autor    : Daniel Raimundo da Silva
 * Data     : 2021-06-30 <yyyy-mm-dd>
 * Licença  : Este arquivo está sob licença Non-Comercial Creative Commons
 * Descrição: Define uma coleção encadeada (árvore binária) ordenada que imple-
 *            menta a interface IDicionarizável.
 *
 *******************************************************************************/
package fractallotus.fundacao.colecoes;

import java.util.Iterator;

import fractallotus.fundacao.cerne.IEnumeravel;
import fractallotus.fundacao.cerne.IComparador;
import fractallotus.fundacao.cerne.IDicionarizavel;
import fractallotus.fundacao.cerne.ParChaveValor;
import fractallotus.fundacao.recursos.Excecoes;
import fractallotus.fundacao.extensores.ExtensorPadrao;

public class DicionarioOrdenadoEncadeado<TChave, TValor> extends ArvoreRubronegraOrdenada<TChave, TValor> implements IDicionarizavel<TChave, TValor>
{
  // ATRIBUTOS //
  private IEnumeravel<TChave> fChaves;
  private IEnumeravel<TValor> fValores;

  // PROPRIEDADES //
  public boolean somenteLeitura() { return false; }

  public TValor valor(TChave chave) throws IllegalStateException, IndexOutOfBoundsException
  {
    if (vazio())
      throw Excecoes.colecaoVazia();

    No no = buscarNo(chave);

    if (no == null)
      throw Excecoes.chaveDicionario();

    return no.valor;
  }

  public void valor(TChave chave, TValor valor)
  {
    adicionar(chave, valor);
  }

  public IEnumeravel<TChave> chaves()
  {
    return fChaves;
  }

  public IEnumeravel<TValor> valores()
  {
    return fValores;
  }

  // CONSTRUTORES //
  public DicionarioOrdenadoEncadeado(IComparador<TChave> comparador)
  {
    super(comparador);

    fChaves  = criarChaves (this);
    fValores = criarValores(this);
  }

  // INTERFACE - ENUMERÁVEL //
  public String toString()
  {
    return ExtensorPadrao.<ParChaveValor<TChave, TValor>>toString(this);
  }

  public ParChaveValor<TChave, TValor>[] toArray(Class<ParChaveValor<TChave, TValor>> tipo)
  {
    return ExtensorPadrao.<ParChaveValor<TChave, TValor>>toArray(tipo, this);
  }

  public Iterator<ParChaveValor<TChave, TValor>> iterator()
  {
    return new EnumeradorPares
    (
      this, buscarNoMinimo(raiz()), buscarNoMaximo(raiz())
    );
  }

  // INTERFACE - DICIONARIZÁVEL //
  public void adicionar(TChave chave, TValor valor) throws IllegalStateException
  {
    No no = buscarNo(chave);

    if (no != null)
      no.valor = valor;
    else
    {
      if (cheio())
        throw Excecoes.colecaoCheia();

      inserirNo( new No(chave, valor) );
    }
  }

  public void adicionar(IDicionarizavel<TChave, TValor> dicionario)
  {
    for (ParChaveValor<TChave, TValor> par : dicionario)
      adicionar(par.chave(), par.valor());
  }

  public boolean remover(TChave chave)
  {
    No no = vazio() ? null : buscarNo(chave);

    if (no != null)
      excluirNo(no);

    return (no != null);
  }

  public boolean contem(TChave chave)
  {
    return (!vazio()) && (buscarNo(chave) != null);
  }

  public void limpar()
  {
    raiz(null);
  }

  private IEnumeravel<TChave> criarChaves(DicionarioOrdenadoEncadeado<TChave, TValor> dicionario)
  {
    return new IEnumeravel<TChave>()
    {
      // ATRIBUTOS //
      private DicionarioOrdenadoEncadeado<TChave, TValor> colecao = dicionario;
      // PROPRIEDADES //
      public int     total() { return colecao.total(); }
      public boolean vazio() { return colecao.vazio(); }
      public boolean cheio() { return colecao.cheio(); }
      public boolean somenteLeitura() { return true; }

      public String toString()
      {
        return ExtensorPadrao.<TChave>toString(this);
      }

      public TChave[] toArray(Class<TChave> tipo)
      {
        return ExtensorPadrao.<TChave>toArray(tipo, this);
      }

      public Iterator<TChave> iterator()
      {
        return new EnumeradorChaves
        (
          colecao, colecao.buscarNoMinimo(raiz()), colecao.buscarNoMaximo(raiz())
        );
      }
    };
  }

  private IEnumeravel<TValor> criarValores(DicionarioOrdenadoEncadeado<TChave, TValor> dicionario)
  {
    return new IEnumeravel<TValor>()
    {
      // ATRIBUTOS //
      private DicionarioOrdenadoEncadeado<TChave, TValor> colecao = dicionario;
      // PROPRIEDADES //
      public int     total() { return colecao.total(); }
      public boolean vazio() { return colecao.vazio(); }
      public boolean cheio() { return colecao.cheio(); }
      public boolean somenteLeitura() { return true; }

      public String toString()
      {
        return ExtensorPadrao.<TValor>toString(this);
      }

      public TValor[] toArray(Class<TValor> tipo)
      {
        return ExtensorPadrao.<TValor>toArray(tipo, this);
      }

      public Iterator<TValor> iterator()
      {
        return new EnumeradorValores
        (
          colecao, colecao.buscarNoMinimo(raiz()), colecao.buscarNoMaximo(raiz())
        );
      }
    };
  }
}