/*******************************************************************************
 *
 * Arquivo  : ListaVetorial.java
 * Autor    : Daniel Raimundo da Silva
 * Data     : 2021-07-03 <yyyy-mm-dd>
 * Licença  : Este arquivo está sob licença Non-Comercial Creative Commons
 * Descrição: Define uma coleção vetorial indexada que implementa a interface
 *            IListável.
 *
 *******************************************************************************/
package fractallotus.fundacao.colecoes;

import java.util.Iterator;

import fractallotus.fundacao.cerne.IListavel;
import fractallotus.fundacao.cerne.IEnumeravel;
import fractallotus.fundacao.cerne.ParChaveValor;
import fractallotus.fundacao.cerne.FabricaComparador;
import fractallotus.fundacao.recursos.Excecoes;
import fractallotus.fundacao.extensores.ExtensorPadrao;

public class ListaVetorial<T> extends VetorizacaoOrdenada<Integer, T> implements IListavel<T>
{
  // ATRIBUTOS //
  private int fProximo;

  // PROPRIEDADES //
  public boolean somenteLeitura() { return false; }

  public T primeiro() throws IllegalStateException
  {
    if (vazio())
      throw Excecoes.colecaoVazia();

    return valor(0);
  }

  public T ultimo() throws IllegalStateException
  {
    if (vazio())
      throw Excecoes.colecaoVazia();

    return valor(total() -1);
  }

  public T item(int indice) throws IndexOutOfBoundsException
  {
    if ((indice < 0) || (indice >= capacidade()))
      throw Excecoes.indiceForaLimites("indice", 0, capacidade() - 1);

    int real = indexar(indice);

    if (comparar(real, indice) != 0)
      throw Excecoes.chaveDicionario();

    return valor(real);
  }

  public void item(int indice, T valor) throws IndexOutOfBoundsException
  {
    inserir(indice, valor);
  }

  // CONSTRUTORES //
  public ListaVetorial(int capacidade)
  {
    super(capacidade, FabricaComparador.<Integer>criar());

    fProximo = 0;
  }

  // INTERFACE - ENUMERÁVEL //
  public String toString()
  {
    return ExtensorPadrao.<T>toString(this);
  }

  public T[] toArray(Class<T> tipo)
  {
    return ExtensorPadrao.<T>toArray(tipo, this);
  }

  public Iterator<T> iterator()
  {
    return new EnumeradorValores(this, 0, total());
  }

  // INTERFACE - LISTÁVEL //
  public int adicionar(T item)
  {
    int result;

    if (cheio())
      result = -1;
    else
    {
      while (contemChave(fProximo))
        if (++fProximo < 0)
          fProximo = 0;

      result = fProximo++;
      inserir(result, item);
    }

    return result;
  }

  public int adicionar(T[] itens)
  {
    int result = -1;

    if (!cheio())
      for (int k = 0; k < itens.length; k++)
      {
        int i = adicionar(itens[k]);

        if (i < 0)
          break;
        if (result < 0)
          result = i;
      }

    return result;
  }

  public int adicionar(IEnumeravel<T> itens)
  {
    int result = -1;

    if (!cheio())
      for (T item : itens)
      {
        int i = adicionar(item);

        if (i < 0)
          break;
        if (result < 0)
          result = i;
      }

    return result;
  }

  public void inserir(int indice, T item) throws IndexOutOfBoundsException
  {
    if ((indice < 0) || (indice >= capacidade()))
      throw Excecoes.indiceForaLimites("indice", 0, capacidade() - 1);
    else if (vazio())
      inserirValor(0, indice, item);
    else
    {
      int indiceReal = indexar (indice),
          comparacao = comparar(indice, indiceReal);

      if (comparacao == 0)
        super.par(indiceReal, indice, item);
      else
        inserirValor(indiceReal + (comparacao < 0 ? 0 : 1), indice, item);
    }
  }

  public void inserir(int indice, T[] itens) throws IndexOutOfBoundsException
  {
    for (int i = indice; i < indice + itens.length; i++)
    {
      if (i >= capacidade())
        break;

      inserir(i, itens[i - indice]);
    }
  }

  public void inserir(int indice, IEnumeravel<T> itens) throws IndexOutOfBoundsException
  {
    for (T item : itens)
    {
      if (indice >= capacidade())
        break;

      inserir(indice++, item);
    }
  }

  public boolean remover(int indice)
  {
    if ((indice < 0) || (indice >= capacidade()))
      return false;

    int     real   = indexar (indice);
    boolean result = comparar(indice, real) == 0;

    if (result)
      excluirValor(real);

    return result;
  }

  public boolean contem(int indice)
  {
    return contemChave(indice);
  }

  public void limpar()
  {
    excluirTodos();
  }

  public IEnumeravel<T> obterItens(int inferior, int superior) throws IndexOutOfBoundsException
  {
    if ((inferior < 0) || (inferior >= capacidade()))
      throw Excecoes.indiceForaLimites("inferior", 0, capacidade() - 1);
    if ((superior < 0) || (superior >= capacidade()))
      throw Excecoes.indiceForaLimites("superior", 0, capacidade() - 1);

    int primeiro = (inferior <= superior) ? teto(inferior) : teto(superior),
        ultimo   = (inferior <= superior) ? piso(superior) : piso(inferior);

    return new ColecaoValores(this, primeiro, ultimo);
  }
}