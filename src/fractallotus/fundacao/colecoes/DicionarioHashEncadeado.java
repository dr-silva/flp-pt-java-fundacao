/*******************************************************************************
 *
 * Arquivo  : DicionárioHashEncadeado.java
 * Autor    : Daniel Raimundo da Silva
 * Data     : 2021-07-01 <yyyy-mm-dd>
 * Licença  : Este arquivo está sob licença Non-Comercial Creative Commons
 * Descrição: Define uma coleção do tipo hash encadeado, não ordenada, que imple-
 *            menta a interface IDicionarizável.
 *
 *******************************************************************************/
package fractallotus.fundacao.colecoes;

import java.util.Iterator;

import fractallotus.fundacao.cerne.IEnumeravel;
import fractallotus.fundacao.cerne.ParChaveValor;
import fractallotus.fundacao.cerne.IDicionarizavel;
import fractallotus.fundacao.cerne.IComparadorIgualdade;
import fractallotus.fundacao.recursos.Excecoes;
import fractallotus.fundacao.extensores.ExtensorPadrao;

public class DicionarioHashEncadeado<TChave, TValor> extends    HashEncadeado  <TChave, TValor>
                                                     implements IDicionarizavel<TChave, TValor>
{
  // ATRIBUTOS //
  private IEnumeravel<TChave> fChaves;
  private IEnumeravel<TValor> fValores;

  // PROPRIEDADES //
  public boolean somenteLeitura() { return false; }

  public TValor valor(TChave chave) throws IllegalStateException, IndexOutOfBoundsException
  {
    if (vazio())
      throw Excecoes.colecaoVazia();

    Item item = buscarItem(chave);

    if (item == null)
      throw Excecoes.chaveDicionario();

    return item.valor;
  }

  public void valor(TChave chave, TValor valor)
  {
    adicionar(chave, valor);
  }

  public IEnumeravel<TChave> chaves()
  {
    return fChaves;
  }

  public IEnumeravel<TValor> valores()
  {
    return fValores;
  }

  // CONSTRUTORES //
  public DicionarioHashEncadeado(IComparadorIgualdade<TChave> comparador)
  {
    super(comparador);

    fChaves  = new ColecaoChaves (this);
    fValores = new ColecaoValores(this);
  }

  public DicionarioHashEncadeado(int capacidade, IComparadorIgualdade<TChave> comparador)
  {
    super(capacidade, comparador);

    fChaves  = new ColecaoChaves (this);
    fValores = new ColecaoValores(this);
  }

  // INTERFACE - ENUMERÁVEL //
  public String toString()
  {
    return ExtensorPadrao.<ParChaveValor<TChave, TValor>>toString(this);
  }

  public ParChaveValor<TChave, TValor>[] toArray(Class<ParChaveValor<TChave, TValor>> tipo)
  {
    return ExtensorPadrao.<ParChaveValor<TChave, TValor>>toArray(tipo, this);
  }

  public Iterator<ParChaveValor<TChave, TValor>> iterator()
  {
    return new EnumeradorPares(this);
  }

  // INTERFACE - DICIONARIZÁVEL //
  public void adicionar(TChave chave, TValor valor) throws IllegalStateException
  {
    Item item = buscarItem(chave);

    if (item != null)
      item.valor = valor;
    else
    {
      if (cheio())
        throw Excecoes.colecaoCheia();

      inserirItem(chave, valor);
    }
  }

  public void adicionar(IDicionarizavel<TChave, TValor> dicionario)
  {
    for (ParChaveValor<TChave, TValor> par : dicionario)
      adicionar(par.chave(), par.valor());
  }

  public boolean remover(TChave chave)
  {
    Item item = vazio() ? null : buscarItem(chave);

    if (item != null)
      excluirItem(item);

    return (item != null);
  }

  public boolean contem(TChave chave)
  {
    return (!vazio()) && (buscarItem(chave) != null);
  }

  public void limpar()
  {
    if (!vazio())
    {
      excluirTodos();
      redimensionar((byte)4);
    }
  }
}