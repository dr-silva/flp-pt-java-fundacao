/*******************************************************************************
 *
 * Arquivo  : IListavel.java
 * Autor    : Daniel Raimundo da Silva
 * Data     : 2020-08-09 <yyyy-mm-dd>
 * Licença  : Este arquivo está sob licença Non-Comercial Creative Commons
 * Descrição: Define uma interface para coleções que possam ser acessadas por
 *            índices.
 *
 *******************************************************************************/
package fractallotus.fundacao.cerne;

public interface IListavel<T> extends IEnumeravel<T>
{
  // MÉTODOS //
  int adicionar(T item);
  int adicionar(T[] itens);
  int adicionar(IEnumeravel<T> itens);

  void inserir(int indice, T item)               throws IndexOutOfBoundsException;
  void inserir(int indice, T[] itens)            throws IndexOutOfBoundsException;
  void inserir(int indice, IEnumeravel<T> itens) throws IndexOutOfBoundsException;

  boolean contem (int indice);
  boolean remover(int indice);

  void limpar();

  IEnumeravel<T> obterItens(int inferior, int superior) throws IndexOutOfBoundsException;

  // PROPRIEDADES //
  T primeiro() throws IllegalStateException;
  T ultimo()   throws IllegalStateException;

  T    item(int indice)          throws IndexOutOfBoundsException;
  void item(int indice, T valor) throws IndexOutOfBoundsException;
}
