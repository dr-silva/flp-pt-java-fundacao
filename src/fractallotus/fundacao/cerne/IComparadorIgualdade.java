/*******************************************************************************
 *
 * Arquivo  : IComparadorIgualdade.java
 * Autor    : Daniel Raimundo da Silva
 * Data     : 2020-08-09 <yyyy-mm-dd>
 * Licença  : Este arquivo está sob licença Non-Comercial Creative Commons
 * Descrição: Define um protocolo que deve ser implementado para classes que
 *            necessitem de suporte à comparação de igualdade e/ou precisem do
 *            código hash.
 *
 *******************************************************************************/
package fractallotus.fundacao.cerne;

public interface IComparadorIgualdade<T>
{
  boolean eIgual(T esquerda, T direita);
  int     obterHash(T valor);
}
