/*******************************************************************************
 *
 * Arquivo  : CorrelacionamentoPadrao.java
 * Autor    : Daniel Raimundo da Silva
 * Data     : 2020-08-10 <yyyy-mm-dd>
 * Licença  : Este arquivo está sob licença Non-Comercial Creative Commons
 * Descrição: Define uma classe para ser usada como instancia do ICorrelaciona-
 *            mento.
 *
 *******************************************************************************/
package fractallotus.fundacao.cerne;

import fractallotus.fundacao.recursos.Excecoes;

class CorrelacionamentoPadrao<TEsquerda, TDireita, TChave, TResult> implements
  ICorrelacionamento<TEsquerda, TDireita, TResult>
{
  // ATRIBUTOS //
  private TResult                               fCorrelacao;
  private IProjetor<TEsquerda, TChave>          fProjetorEsquerda;
  private IProjetor<TDireita , TChave>          fProjetorDireita;
  private IComparadorIgualdade<TChave>          fComparador;
  private Funcao2<TEsquerda, TDireita, TResult> fFuncaoResultante;

  // MÉTODOS - CONSTRUTORES //
  public CorrelacionamentoPadrao
  (
    IProjetor<TEsquerda, TChave>          projetorEsquerda,
    IProjetor<TDireita , TChave>          projetorDireita,
    IComparadorIgualdade<TChave>          comparador,
    Funcao2<TEsquerda, TDireita, TResult> funcaoResultante
  )
  {
    if (projetorEsquerda == null)
      throw Excecoes.argumentoNulo("projetorEsquerda");

    if (projetorDireita == null)
      throw Excecoes.argumentoNulo("projetoDireita");

    if (comparador == null)
      throw Excecoes.argumentoNulo("comparador");

    if (funcaoResultante == null)
      throw Excecoes.argumentoNulo("funcaoResultante");

    fProjetorEsquerda = projetorEsquerda;
    fProjetorDireita  = projetorDireita;
    fComparador       = comparador;
    fFuncaoResultante = funcaoResultante;
  }

  // MÉTODOS - INTERFACE //
  public boolean correlacionar(TEsquerda esquerda, TDireita direita)
  {
    TChave chaveEsquerda = fProjetorEsquerda.projetar(esquerda),
           chaveDireita  = fProjetorDireita .projetar(direita );

    boolean result = fComparador.eIgual(chaveEsquerda, chaveDireita);

    fCorrelacao = (result) ? fFuncaoResultante.invocar(esquerda, direita) : null;

    return result;
  }

  // PROPRIEDADES - INTERFACE //
  public TResult correlacao() { return fCorrelacao; }
}
