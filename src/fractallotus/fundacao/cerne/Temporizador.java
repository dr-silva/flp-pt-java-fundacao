/*******************************************************************************
 *
 * Arquivo  : Temporizador.java
 * Autor    : Daniel Raimundo da Silva
 * Data     : 2020-08-09 <yyyy-mm-dd>
 * Licença  : Este arquivo está sob licença Non-Comercial Creative Commons
 * Descrição: Esta classe tem o objetivo de contar o tempo entre o agora e sua
 *            inicialização, que pode ser o momento de sua instancialização ou
 *            a última vez que foi reiniciada.
 *
 *******************************************************************************/
package fractallotus.fundacao.cerne;

public class Temporizador
{
  // ATRIBUTOS //
  private long fInicio;

  // MÉTODOS //
  public Temporizador()
  {
    reiniciar();
  }

  public void reiniciar()
  {
    fInicio = System.currentTimeMillis();
  }

  public Periodo decorrer()
  {
    return new Periodo(fInicio, System.currentTimeMillis());
  }
}
