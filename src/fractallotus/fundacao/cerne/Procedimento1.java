/*******************************************************************************
 *
 * Arquivo  : Procedimento1.java
 * Autor    : Daniel Raimundo da Silva
 * Data     : 2020-08-10 <yyyy-mm-dd>
 * Licença  : Este arquivo está sob licença Non-Comercial Creative Commons
 * Descrição: Define um procedimento genérico com 1 parâmetro.
 *
 *******************************************************************************/
package fractallotus.fundacao.cerne;

public interface Procedimento1<T>
{
  void invocar(T arg);
}
