/*******************************************************************************
 *
 * Arquivo  : IEnfileiravel.java
 * Autor    : Daniel Raimundo da Silva
 * Data     : 2020-08-09 <yyyy-mm-dd>
 * Licença  : Este arquivo está sob licença Non-Comercial Creative Commons
 * Descrição: Define uma interface para coleções que se baseiam no princípio
 *            First In, First Out (FIFO);
 *
 *******************************************************************************/
package fractallotus.fundacao.cerne;

public interface IEnfileiravel<T> extends IEnumeravel<T>
{
  void enfileirar(T item) throws IllegalStateException;
  T    desenfileirar()    throws IllegalStateException;
  T    espiar()           throws IllegalStateException;
  void limpar();
}
