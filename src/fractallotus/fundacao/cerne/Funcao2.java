/*******************************************************************************
 *
 * Arquivo  : Funcao2.java
 * Autor    : Daniel Raimundo da Silva
 * Data     : 2020-08-10 <yyyy-mm-dd>
 * Licença  : Este arquivo está sob licença Non-Comercial Creative Commons
 * Descrição: Define uma função genérica com 2 parâmetros.
 *
 *******************************************************************************/
package fractallotus.fundacao.cerne;

public interface Funcao2<T1, T2, TResult>
{
  TResult invocar(T1 arg1, T2 arg2);
}
