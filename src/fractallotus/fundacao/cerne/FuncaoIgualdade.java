/*******************************************************************************
 *
 * Arquivo  : FuncaoIgualdade.java
 * Autor    : Daniel Raimundo da Silva
 * Data     : 2020-08-10 <yyyy-mm-dd>
 * Licença  : Este arquivo está sob licença Non-Comercial Creative Commons
 * Descrição: Define uma função usada para verificar a igualdade entre dois
 *            valores do mesmo tipo genérico T.
 *
 *******************************************************************************/
package fractallotus.fundacao.cerne;

public interface FuncaoIgualdade<T>
{
  boolean invocar(T esquerda, T direita);
}
