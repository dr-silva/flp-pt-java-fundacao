/*******************************************************************************
 *
 * Arquivo  : IComparador.java
 * Autor    : Daniel Raimundo da Silva
 * Data     : 2020-08-09 <yyyy-mm-dd>
 * Licença  : Este arquivo está sob licença Non-Comercial Creative Commons
 * Descrição: Define um protocolo que deve ser implementado para classes que
 *            necessitem duma relação de ordem.
 *
 *******************************************************************************/
package fractallotus.fundacao.cerne;

import java.util.Comparator;

public interface IComparador<T> extends Comparator<T>
{
  //
}
