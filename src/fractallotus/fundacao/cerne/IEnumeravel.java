/*******************************************************************************
 *
 * Arquivo  : IEnumeravel.java
 * Autor    : Daniel Raimundo da Silva
 * Data     : 2020-08-09 <yyyy-mm-dd>
 * Licença  : Este arquivo está sob licença Non-Comercial Creative Commons
 * Descrição: Define uma interface padrão ao projeto para ser alvo da declaração
 *            "foreach".
 *
 *******************************************************************************/
package fractallotus.fundacao.cerne;

public interface IEnumeravel<T> extends Iterable<T>
{
  // MÉTODOS //
  String toString();
  T[]    toArray(Class<T> tipo);

  // PROPRIEDADES //
  int     total();
  boolean vazio();
  boolean cheio();
  boolean somenteLeitura();
}
