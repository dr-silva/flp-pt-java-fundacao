/*******************************************************************************
 *
 * Arquivo  : IGuardavel.java
 * Autor    : Daniel Raimundo da Silva
 * Data     : 2020-08-09 <yyyy-mm-dd>
 * Licença  : Este arquivo está sob licença Non-Comercial Creative Commons
 * Descrição: Define uma interface para coleções em que não seja necessário
 *            suporte à remoção de itens. Seu propósito é fornecer uma coleção
 *            que possa guardar itens e, então, iterá-los.
 *
 *******************************************************************************/
package fractallotus.fundacao.cerne;

public interface IGuardavel<T> extends IEnumeravel<T>
{
  // MÉTODOS //
  void guardar(T item)               throws IllegalStateException;
  void guardar(T[] itens)            throws IllegalStateException;
  void guardar(IEnumeravel<T> itens) throws IllegalStateException;

  // PROPRIEDADES //
  T primeiro() throws IllegalStateException;
  T ultimo()   throws IllegalStateException;
}
