/*******************************************************************************
 *
 * Arquivo  : FabricaComparador.java
 * Autor    : Daniel Raimundo da Silva
 * Data     : 2020-08-10 <yyyy-mm-dd>
 * Licença  : Este arquivo está sob licença Non-Comercial Creative Commons
 * Descrição: Classe para criar instancias da interface IComparador.
 *
 *******************************************************************************/
package fractallotus.fundacao.cerne;

import java.util.Comparator;
import fractallotus.fundacao.recursos.Excecoes;

public class FabricaComparador
{
  public static <T extends Comparable<T>> IComparador<T> criar()
  {
    return new IComparador<T>()
    {
      public int compare(T esquerda, T direita)
      {
        int result;

        if ((esquerda == null) && (direita == null))
          result = 0;
        else if (esquerda == null)
          result = +1;
        else if (direita == null)
          result = -1;
        else
          result = esquerda.compareTo(direita);

        return result;
      }
    };
  }

  public static <T> IComparador<T> criar(Comparator<T> comparator)
  {
    if (comparator == null)
      throw Excecoes.argumentoNulo("comparador");

    return new IComparador<T>()
    {
      private Comparator<T> fComparator = comparator;

      public int compare(T esquerda, T direita)
      {
        return fComparator.compare(esquerda, direita);
      }
    };
  }
}
