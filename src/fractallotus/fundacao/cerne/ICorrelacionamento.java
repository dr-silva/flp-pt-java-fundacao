/*******************************************************************************
 *
 * Arquivo  : ICorrelacionamento.java
 * Autor    : Daniel Raimundo da Silva
 * Data     : 2020-08-09 <yyyy-mm-dd>
 * Licença  : Este arquivo está sob licença Non-Comercial Creative Commons
 * Descrição: Define uma interface que deve ser implementada para classes que
 *            precisem estabelecer uma relação entre dois valores de tipos
 *            genéricos distintos (TEsquerda e TDireita). Caso essa relação seja
 *            confirmada, eles servirão de base para criar um valor resultante.
 *
 *******************************************************************************/
package fractallotus.fundacao.cerne;

public interface ICorrelacionamento<TEsquerda, TDireita, TResult>
{
  // MÉTODOS //
  boolean correlacionar(TEsquerda esquerda, TDireita direita);
  // PROPRIEDADES //
  TResult correlacao();
}
