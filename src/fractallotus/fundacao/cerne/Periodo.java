/*******************************************************************************
 *
 * Arquivo  : Periodo.java
 * Autor    : Daniel Raimundo da Silva
 * Data     : 2020-08-09 <yyyy-mm-dd>
 * Licença  : Este arquivo está sob licença Non-Comercial Creative Commons
 * Descrição: Estrutura para armazenar informações dum intervalo de tempo.
 *
 *******************************************************************************/
package fractallotus.fundacao.cerne;

import java.util.Date;
import fractallotus.fundacao.recursos.Excecoes;

public class Periodo implements Comparable<Periodo>
{
  // ATRIBUTOS //
  private int   fDias;
  private short fHoras,
                fMinutos,
                fSegundos,
                fMilissegundos;

  // MÉTODOS - CONSTRUTORES //
  public Periodo
  (
    int dias, short horas, short minutos, short segundos, short milissegundos
  )
  {
    Anel anel;

    anel           = anel(milissegundos, 1000);
    fMilissegundos = (short)anel.resultado;

    anel      = anel(segundos + anel.adicional, 60);
    fSegundos = (short)anel.resultado;

    anel     = anel(minutos + anel.adicional, 60);
    fMinutos = (short)anel.resultado;

    anel   = anel(horas + anel.adicional, 24);
    fHoras = (short)anel.resultado;
    fDias  = anel.adicional + dias;
  }

  public Periodo(long inicio, long fim)
  {
    periodizar(fim - inicio, this);
  }

  // MÉTODOS - CONSULTAS //
  public boolean equals(Object obj)
  {
    // return (obj is Periodo) ? comparar(this, (Periodo)obj) == 0 : false;
    return false;
  }

  public int compareTo(Periodo obj)
  {
    return comparar(this, obj);
  }

  public int hashCode()
  {
    return (int)valorar(this);
  }

  public String toString()
  {
    return String.format
    (
      "%d:%02d:%02d:%02d.%03d", fDias, fHoras, fMinutos, fSegundos, fMilissegundos
    );
  }

  // MÉTODOS - ASSISTENTES //
  public static boolean eIgual(Periodo esquerda, Periodo direita)
  {
    return comparar(esquerda, direita) == 0;
  }

  public static int comparar(Periodo esquerda, Periodo direita)
  {
    int result;

    if ((esquerda == null) && (direita == null))
      result = 0;
    else if (esquerda == null)
      result = +1;
    else if (direita == null)
      result = -1;
    else
    {
      long lEsquerda = valorar(esquerda),
           lDireita  = valorar(direita);

      if (lEsquerda < lDireita)
        result = -1;
      else if (lEsquerda > lDireita)
        result = +1;
      else
        result = 0;
    }

    return result;
  }

  public static String toString(Periodo periodo)
  {
    String result;

    if (periodo == null)
      result = new String();
    else
      result = periodo.toString();

    return result;
  }

  public static Periodo parse(String str)
  {
    if (!canParse(str))
      throw Excecoes.periodoInvalido("str");

    String[] valores = str.split("(:|.)", 5);

    return new Periodo(Integer.parseInt(valores[0]),
                       Short.parseShort(valores[1]),
                       Short.parseShort(valores[2]),
                       Short.parseShort(valores[3]),
                       Short.parseShort(valores[4]));
  }

  public static boolean canParse(String str)
  {
    return str.matches
    (
      "(+|-)?[0-9]+:([01][0-9]|2[0-3])(:[0-5][0-9]){2}.[0-9]{3}"
    );
  }

  private static long valorar(Periodo periodo)
  {
    return  periodo.fMilissegundos              +
           (periodo.fSegundos * 1000)           +
           (periodo.fMinutos  * 1000 * 60)      +
           (periodo.fHoras    * 1000 * 60 * 60) +
           (periodo.fDias     * 1000 * 60 * 60 * 24);
  }

  private static void periodizar(long valor, Periodo periodo)
  {
    Anel anel;

    anel                   = anel(valor, 1000);
    periodo.fMilissegundos = (short)anel.resultado;

    anel              = anel(anel.adicional, 60);
    periodo.fSegundos = (short)anel.resultado;

    anel             = anel(anel.adicional, 60);
    periodo.fMinutos = (short)anel.resultado;

    anel           = anel(anel.adicional, 24);
    periodo.fHoras = (short)anel.resultado;
    periodo.fDias  = anel.adicional;
  }

  private static class Anel
  {
    public int resultado,
               adicional;
  }

  private static Anel anel(long valor, int modulo)
  {
    Anel    anel      = new Anel();
    boolean eNegativo = (valor < 0);
    long    neoValor  = (eNegativo) ? -valor : valor;

    anel.adicional = (int)(neoValor / modulo);
    anel.resultado = (int)(neoValor % modulo);

    if (eNegativo)
    {
      anel.resultado = modulo - anel.resultado;
      anel.adicional = -(anel.adicional + 1);
    }

    return anel;
  }

  // MÉTODOS - OPERADORES //
  public Periodo positivo()
  {
    return new Periodo(       +fDias,
                       (short)+fHoras,
                       (short)+fMinutos,
                       (short)+fSegundos,
                       (short)+fMilissegundos);
  }

  public Periodo negativo()
  {
    return new Periodo(       -fDias,
                       (short)-fHoras,
                       (short)-fMinutos,
                       (short)-fSegundos,
                       (short)-fMilissegundos);
  }

  public Periodo somar(Periodo parcela)
  {
    return new Periodo(        this.fDias          + parcela.fDias,
                       (short)(this.fHoras         + parcela.fHoras),
                       (short)(this.fMinutos       + parcela.fMinutos),
                       (short)(this.fSegundos      + parcela.fSegundos),
                       (short)(this.fMilissegundos + parcela.fMilissegundos));
  }

  public Periodo subtrair(Periodo parcela)
  {
    return new Periodo(        this.fDias          - parcela.fDias,
                       (short)(this.fHoras         - parcela.fHoras),
                       (short)(this.fMinutos       - parcela.fMinutos),
                       (short)(this.fSegundos      - parcela.fSegundos),
                       (short)(this.fMilissegundos - parcela.fMilissegundos));
  }

  // PROPRIEDADES //
  int   dias()          { return fDias;          }
  short horas()         { return fHoras;         }
  short minutos()       { return fMinutos;       }
  short segundos()      { return fSegundos;      }
  short milissegundos() { return fMilissegundos; }
}
