/*******************************************************************************
 *
 * Arquivo  : FuncaoHash.java
 * Autor    : Daniel Raimundo da Silva
 * Data     : 2020-08-10 <yyyy-mm-dd>
 * Licença  : Este arquivo está sob licença Non-Comercial Creative Commons
 * Descrição: Define uma função usada para obter o código hash dum valor do
 *            tipo genérico T.
 *
 *******************************************************************************/
package fractallotus.fundacao.cerne;

public interface FuncaoHash<T>
{
  int invocar(T valor);
}
