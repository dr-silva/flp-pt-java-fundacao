/*******************************************************************************
 *
 * Arquivo  : IValidadorOrdenacao.java
 * Autor    : Daniel Raimundo da Silva
 * Data     : 2020-08-09 <yyyy-mm-dd>
 * Licença  : Este arquivo está sob licença Non-Comercial Creative Commons
 * Descrição: Define um protocolo que deve ser implementado por classes que
 *            queiram averiguar se um array está numa determinada ordem.
 *
 *******************************************************************************/
package fractallotus.fundacao.cerne;

public interface IValidadorOrdenacao<T>
{
  boolean validar(T[] itens);
}
