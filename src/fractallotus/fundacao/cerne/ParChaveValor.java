/*******************************************************************************
 *
 * Arquivo  : ParChaveValor.java
 * Autor    : Daniel Raimundo da Silva
 * Data     : 2020-08-09 <yyyy-mm-dd>
 * Licença  : Este arquivo está sob licença Non-Comercial Creative Commons
 * Descrição: Define um tipo de dado par chave-valor.
 *
 *******************************************************************************/
package fractallotus.fundacao.cerne;

public class ParChaveValor<TChave, TValor>
{
  // ATRIBUTOS //
  private TChave fChave;
  private TValor fValor;

  // MÉTODOS //
  public ParChaveValor(TChave chave)
  {
    fChave = chave;
  }

  public ParChaveValor(TChave chave, TValor valor)
  {
    fChave = chave;
    fValor = valor;
  }

  // PROPRIEDADES //
  public TChave chave()
  {
    return fChave;
  }

  public TValor valor()
  {
    return fValor;
  }

  public void valor(TValor valor)
  {
    fValor = valor;
  }
}
