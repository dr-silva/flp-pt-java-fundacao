/*******************************************************************************
 *
 * Arquivo  : IDistinguivel.java
 * Autor    : Daniel Raimundo da Silva
 * Data     : 2020-08-09 <yyyy-mm-dd>
 * Licença  : Este arquivo está sob licença Non-Comercial Creative Commons
 * Descrição: Define uma interface para coleções cujos itens não possam ser du-
 *            plicados. A coleção é interpretada também como sendo um conjunto
 *            no sentido matemático.
 *
 *******************************************************************************/
package fractallotus.fundacao.cerne;

public interface IDistinguivel<TChave> extends IEnumeravel<TChave>
{
  boolean adicionar(TChave chave);
  boolean remover  (TChave chave);
  boolean contem   (TChave chave);

  void limpar();

  void unir          (IEnumeravel<TChave> colecao);
  void excetuar      (IEnumeravel<TChave> colecao);
  void interseccionar(IEnumeravel<TChave> colecao);
}
