/*******************************************************************************
 *
 * Arquivo  : FabricaCorrelacionamento.java
 * Autor    : Daniel Raimundo da Silva
 * Data     : 2020-08-10 <yyyy-mm-dd>
 * Licença  : Este arquivo está sob licença Non-Comercial Creative Commons
 * Descrição: Classe para criar instancias da interface ICorrelacionamento.
 *
 *******************************************************************************/
package fractallotus.fundacao.cerne;

public class FabricaCorrelacionamento
{
  public static <TFonte, TChave, TResult> ICorrelacionamento<TFonte, TFonte, TResult> criar
  (
    IProjetor<TFonte, TChave>        projecao,
    Funcao2<TFonte, TFonte, TResult> resultante
  )
  {
    return criar
    (
      projecao,
      resultante,
      FabricaComparadorIgualdade.criar()
    );
  }

  public static <TFonte, TChave, TResult> ICorrelacionamento<TFonte, TFonte, TResult> criar
  (
    IProjetor<TFonte, TChave>        projecao,
    Funcao2<TFonte, TFonte, TResult> resultante,
    FuncaoIgualdade<TChave>          igualdade
  )
  {
    return criar
    (
      projecao,
      resultante,
      FabricaComparadorIgualdade.criar(igualdade)
    );
  }

  public static <TFonte, TChave, TResult> ICorrelacionamento<TFonte, TFonte, TResult> criar
  (
    IProjetor<TFonte, TChave>        projecao,
    Funcao2<TFonte, TFonte, TResult> resultante,
    IComparadorIgualdade<TChave>     comparador
  )
  {
    return new CorrelacionamentoPadrao<TFonte, TFonte, TChave, TResult>
    (
      projecao,
      projecao,
      comparador,
      resultante
    );
  }

  public static <TEsquerda, TDireita, TChave, TResult> ICorrelacionamento<TEsquerda, TDireita, TResult> criar
  (
    IProjetor<TEsquerda, TChave>          projetorEsquerda,
    IProjetor<TDireita , TChave>          projetorDireita,
    Funcao2<TEsquerda, TDireita, TResult> resultante
  )
  {
    return criar
    (
      projetorEsquerda,
      projetorDireita,
      resultante,
      FabricaComparadorIgualdade.criar()
    );
  }

  public static <TEsquerda, TDireita, TChave, TResult> ICorrelacionamento<TEsquerda, TDireita, TResult> criar
  (
    IProjetor<TEsquerda, TChave>          projetorEsquerda,
    IProjetor<TDireita , TChave>          projetorDireita,
    Funcao2<TEsquerda, TDireita, TResult> resultante,
    FuncaoIgualdade<TChave>               igualdade
  )
  {
    return criar
    (
      projetorEsquerda,
      projetorDireita,
      resultante,
      FabricaComparadorIgualdade.criar(igualdade)
    );
  }

  public static <TEsquerda, TDireita, TChave, TResult> ICorrelacionamento<TEsquerda, TDireita, TResult> criar
  (
    IProjetor<TEsquerda, TChave>          projetorEsquerda,
    IProjetor<TDireita , TChave>          projetorDireita,
    Funcao2<TEsquerda, TDireita, TResult> resultante,
    IComparadorIgualdade<TChave>          comparador
  )
  {
    return new CorrelacionamentoPadrao<TEsquerda, TDireita, TChave, TResult>
    (
      projetorEsquerda,
      projetorDireita,
      comparador,
      resultante
    );
  }
}
