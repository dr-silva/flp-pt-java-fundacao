/*******************************************************************************
 *
 * Arquivo  : IDicionarizavel.java
 * Autor    : Daniel Raimundo da Silva
 * Data     : 2020-08-09 <yyyy-mm-dd>
 * Licença  : Este arquivo está sob licença Non-Comercial Creative Commons
 * Descrição: Define uma interface para uma estrutura de dados de pares chave-
 *            -valor.
 *
 *******************************************************************************/
package fractallotus.fundacao.cerne;

public interface IDicionarizavel<TChave, TValor> extends IEnumeravel<ParChaveValor<TChave, TValor>>
{
  // MÉTODOS //
  void adicionar(TChave chave, TValor valor)                 throws IllegalStateException;
  void adicionar(IDicionarizavel<TChave, TValor> dicionario) throws IllegalStateException;

  boolean remover(TChave chave);
  boolean contem (TChave chave);

  void limpar();

  // PROPRIEDADES //
  IEnumeravel<TChave> chaves();
  IEnumeravel<TValor> valores();

  TValor valor(TChave chave)                throws IllegalStateException, IndexOutOfBoundsException;
  void   valor(TChave chave, TValor valor);
}
