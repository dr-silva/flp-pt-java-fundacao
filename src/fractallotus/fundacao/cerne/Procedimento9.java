/*******************************************************************************
 *
 * Arquivo  : Procedimento9.java
 * Autor    : Daniel Raimundo da Silva
 * Data     : 2020-08-10 <yyyy-mm-dd>
 * Licença  : Este arquivo está sob licença Non-Comercial Creative Commons
 * Descrição: Define um procedimento genérico com 9 parâmetros.
 *
 *******************************************************************************/
package fractallotus.fundacao.cerne;

public interface Procedimento9<T1, T2, T3, T4, T5, T6, T7, T8, T9>
{
  void invocar
  (
    T1 arg1, T2 arg2, T3 arg3, T4 arg4, T5 arg5,
    T6 arg6, T7 arg7, T8 arg8, T9 arg9
  );
}
