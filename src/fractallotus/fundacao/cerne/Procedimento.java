/*******************************************************************************
 *
 * Arquivo  : Procedimento.java
 * Autor    : Daniel Raimundo da Silva
 * Data     : 2020-08-10 <yyyy-mm-dd>
 * Licença  : Este arquivo está sob licença Non-Comercial Creative Commons
 * Descrição: Define um procedimento genérico sem parâmetros.
 *
 *******************************************************************************/
package fractallotus.fundacao.cerne;

public interface Procedimento
{
  void invocar();
}
