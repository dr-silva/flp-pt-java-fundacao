/*******************************************************************************
 *
 * Arquivo  : IEmpilhavel.java
 * Autor    : Daniel Raimundo da Silva
 * Data     : 2020-08-09 <yyyy-mm-dd>
 * Licença  : Este arquivo está sob licença Non-Comercial Creative Commons
 * Descrição: Define uma interface de coleções que se baseiam no princípio Last
 *            In, First Out (LIFO).
 *
 *******************************************************************************/
package fractallotus.fundacao.cerne;

public interface IEmpilhavel<T> extends IEnumeravel<T>
{
  void empilhar(T item) throws IllegalStateException;
  T    desempilhar()    throws IllegalStateException;
  T    espiar()         throws IllegalStateException;
  void limpar();
}
