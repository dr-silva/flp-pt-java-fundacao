/*******************************************************************************
 *
 * Arquivo  : ComparadorIgualdadePadrao.java
 * Autor    : Daniel Raimundo da Silva
 * Data     : 2020-08-10 <yyyy-mm-dd>
 * Licença  : Este arquivo está sob licença Non-Comercial Creative Commons
 * Descrição: Define uma classe para ser usada como instancia do IComparadorI-
 *            gualdade.
 *
 *******************************************************************************/
package fractallotus.fundacao.cerne;

class ComparadorIgualdadePadrao<T> implements IComparadorIgualdade<T>
{
  // ATRIBUTOS //
  private FuncaoIgualdade<T> fIgualdade;
  private FuncaoHash<T>      fHash;

  // MÉTODOS - CONSTRUTORES //
  public ComparadorIgualdadePadrao
  (
    FuncaoIgualdade<T> igualdade, FuncaoHash<T> hash
  )
  {
    fIgualdade = igualdade;
    fHash      = hash;
  }
  // MÉTODOS - ASSISTENTES //
  private boolean eIgualInterno(T esquerda, T direita)
  {
    boolean result;

    if ((esquerda == null) && (direita == null))
      result = true;
    else if (esquerda != null)
      result = esquerda.equals(direita);
    else
      result = direita.equals(esquerda);

    return result;
  }

  private int obterHashInterno(T valor)
  {
    return (valor != null) ? valor.hashCode() : (new Integer(42)).hashCode();
  }

  // MÉTODOS - INTERFACE //
  public boolean eIgual(T esquerda, T direita)
  {
    boolean result;

    if (fIgualdade != null)
      result = fIgualdade.invocar(esquerda, direita);
    else
      result = eIgualInterno(esquerda, direita);

    return result;
  }

  public int obterHash(T valor)
  {
    int result;

    if (fHash != null)
      result = fHash.invocar(valor);
    else
      result = obterHashInterno(valor);

    return result;
  }
}
