/*******************************************************************************
 *
 * Arquivo  : IPredicado.java
 * Autor    : Daniel Raimundo da Silva
 * Data     : 2020-08-09 <yyyy-mm-dd>
 * Licença  : Este arquivo está sob licença Non-Comercial Creative Commons
 * Descrição: Define uma interface que deve ser implementada quando for neces-
 *            sário algum predicado para um tipo genérico T.
 *
 *******************************************************************************/
package fractallotus.fundacao.cerne;

public interface IPredicado<T>
{
  boolean validar(T valor);
}
