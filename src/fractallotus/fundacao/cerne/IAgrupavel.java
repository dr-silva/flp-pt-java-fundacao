/*******************************************************************************
 *
 * Arquivo  : IAgrupavel.java
 * Autor    : Daniel Raimundo da Silva
 * Data     : 2020-08-09 <yyyy-mm-dd>
 * Licença  : Este arquivo está sob licença Non-Comercial Creative Commons
 * Descrição: Define um protocoo padrão ao projeto para agrupamentos em cole-
 *            ções.
 *
 *******************************************************************************/
package fractallotus.fundacao.cerne;

public interface IAgrupavel<TChave, TValor> extends IEnumeravel<TValor>
{
  // PROPRIEDADES //
  TChave chave();
}
