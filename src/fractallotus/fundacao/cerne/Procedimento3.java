/*******************************************************************************
 *
 * Arquivo  : Procedimento3.java
 * Autor    : Daniel Raimundo da Silva
 * Data     : 2020-08-10 <yyyy-mm-dd>
 * Licença  : Este arquivo está sob licença Non-Comercial Creative Commons
 * Descrição: Define um procedimento genérico com 3 parâmetros.
 *
 *******************************************************************************/
package fractallotus.fundacao.cerne;

public interface Procedimento3<T1, T2, T3>
{
  void invocar(T1 arg1, T2 arg2, T3 arg3);
}
