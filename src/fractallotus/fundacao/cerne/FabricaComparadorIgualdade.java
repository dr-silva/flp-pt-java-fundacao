/*******************************************************************************
 *
 * Arquivo  : FabricaComparadorIgualdade.java
 * Autor    : Daniel Raimundo da Silva
 * Data     : 2020-08-09 <yyyy-mm-dd>
 * Licença  : Este arquivo está sob licença Non-Comercial Creative Commons
 * Descrição: Classe para criar instâncias da interface IComparadorIgualdade.
 *
 *******************************************************************************/
package fractallotus.fundacao.cerne;

public class FabricaComparadorIgualdade
{
  public static <T> IComparadorIgualdade<T> criar()
  {
    return criar(null, null);
  }

  public static <T> IComparadorIgualdade<T> criar(FuncaoIgualdade<T> igualdade)
  {
    return criar(igualdade, null);
  }

  public static <T> IComparadorIgualdade<T> criar(FuncaoHash<T> hash)
  {
    return criar(null, hash);
  }

  public static <T> IComparadorIgualdade<T> criar
  (
    FuncaoIgualdade<T> igualdade,
    FuncaoHash     <T> hash
  )
  {
    return new ComparadorIgualdadePadrao<T>(igualdade, hash);
  }
}
