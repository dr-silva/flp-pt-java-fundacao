/*******************************************************************************
 *
 * Arquivo  : IOrdenavel.java
 * Autor    : Daniel Raimundo da Silva
 * Data     : 2020-08-09 <yyyy-mm-dd>
 * Licença  : Este arquivo está sob licença Non-Comercial Creative Commons
 * Descrição: Define um protocolo que deve ser implementado por coleções que
 *            tenham itens que se relacionem ordenadamente entre si.
 *
 *******************************************************************************/
package fractallotus.fundacao.cerne;

public interface IOrdenavel<TChave>
{
  // MÉTODOS //
  TChave piso(TChave chave) throws IndexOutOfBoundsException;
  TChave teto(TChave chave) throws IndexOutOfBoundsException;

  int    indexar   (TChave chave);
  TChave desindexar(int    indice) throws IndexOutOfBoundsException;

  int                 contar     (TChave inferior, TChave superior);
  IEnumeravel<TChave> obterChaves(TChave inferior, TChave superior);

  // PROPRIEDADES //
  TChave minimo() throws IllegalStateException;
  TChave maximo() throws IllegalStateException;
}
