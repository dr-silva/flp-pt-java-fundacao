/*******************************************************************************
 *
 * Arquivo  : Procedimento2.java
 * Autor    : Daniel Raimundo da Silva
 * Data     : 2020-08-10 <yyyy-mm-dd>
 * Licença  : Este arquivo está sob licença Non-Comercial Creative Commons
 * Descrição: Define um procedimento genérico com 2 parâmetros.
 *
 *******************************************************************************/
package fractallotus.fundacao.cerne;

public interface Procedimento2<T1, T2>
{
  void invocar(T1 arg1, T2 arg2);
}
