/*******************************************************************************
 *
 * Arquivo  : Procedimento6.java
 * Autor    : Daniel Raimundo da Silva
 * Data     : 2020-08-10 <yyyy-mm-dd>
 * Licença  : Este arquivo está sob licença Non-Comercial Creative Commons
 * Descrição: Define um procedimento genérico com 6 parâmetros.
 *
 *******************************************************************************/
package fractallotus.fundacao.cerne;

public interface Procedimento6<T1, T2, T3, T4, T5, T6>
{
  void invocar(T1 arg1, T2 arg2, T3 arg3, T4 arg4, T5 arg5, T6 arg6);
}
