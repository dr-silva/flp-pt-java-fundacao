/*******************************************************************************
 *
 * Arquivo  : IProjetor.java
 * Autor    : Daniel Raimundo da Silva
 * Data     : 2020-08-09 <yyyy-mm-dd>
 * Licença  : Este arquivo está sob licença Non-Comercial Creative Commons
 * Descrição: Define um protocolo que deve ser implementado para classes que
 *            queiram realizar algum tipo de "conversão" do tipo genérico T
 *            para o tipo genérico TResult.
 *
 *******************************************************************************/
package fractallotus.fundacao.cerne;

public interface IProjetor<T, TResult>
{
  TResult projetar(T valor);
}
