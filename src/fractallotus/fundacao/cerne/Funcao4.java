/*******************************************************************************
 *
 * Arquivo  : Funcao4.java
 * Autor    : Daniel Raimundo da Silva
 * Data     : 2020-08-10 <yyyy-mm-dd>
 * Licença  : Este arquivo está sob licença Non-Comercial Creative Commons
 * Descrição: Define uma função genérica com 4 parâmetros.
 *
 *******************************************************************************/
package fractallotus.fundacao.cerne;

public interface Funcao4<T1, T2, T3, T4, TResult>
{
  TResult invocar(T1 arg1, T2 arg2, T3 arg3, T4 arg4);
}
