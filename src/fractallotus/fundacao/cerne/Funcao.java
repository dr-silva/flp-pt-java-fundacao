/*******************************************************************************
 *
 * Arquivo  : Funcao.java
 * Autor    : Daniel Raimundo da Silva
 * Data     : 2020-08-10 <yyyy-mm-dd>
 * Licença  : Este arquivo está sob licença Non-Comercial Creative Commons
 * Descrição: Define uma função genérica sem parâmetros.
 *
 *******************************************************************************/
package fractallotus.fundacao.cerne;

public interface Funcao<TResult>
{
  TResult invocar();
}
