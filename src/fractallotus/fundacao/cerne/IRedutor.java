/*******************************************************************************
 *
 * Arquivo  : IRedutor.java
 * Autor    : Daniel Raimundo da Silva
 * Data     : 2020-08-09 <yyyy-mm-dd>
 * Licença  : Este arquivo está sob licença Non-Comercial Creative Commons
 * Descrição: Define uma interface que deve ser implementada quando for preciso
 *            fazer uma agregação dalguma coleção do tipo genérico TFonte.
 *            O tipo genérico TAcumulador representa o tipo de retorno da
 &            agragação.
 *
 *******************************************************************************/
package fractallotus.fundacao.cerne;

public interface IRedutor<TAcumulador, TFonte>
{
  TAcumulador reduzir(TAcumulador valor, TFonte item);
}
