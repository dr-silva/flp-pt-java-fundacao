/*******************************************************************************
 *
 * Arquivo  : Funcao1.java
 * Autor    : Daniel Raimundo da Silva
 * Data     : 2020-08-10 <yyyy-mm-dd>
 * Licença  : Este arquivo está sob licença Non-Comercial Creative Commons
 * Descrição: Define uma função genérica com 1 parâmetro.
 *
 *******************************************************************************/
package fractallotus.fundacao.cerne;

public interface Funcao1<T, TResult>
{
  TResult invocar(T arg);
}
