/*******************************************************************************
 *
 * Arquivo  : ExtensorPadrao.java
 * Autor    : Daniel Raimundo da Silva
 * Data     : 2020-09-20 <yyyy-mm-dd>
 * Licença  : Este arquivo está sob licença Non-Comercial Creative Commons
 * Descrição: Classe para gerar arrays e strings como representações de IEnume-
 *            ráveis.
 *
 *******************************************************************************/
package fractallotus.fundacao.extensores;

import java.lang.reflect.Array;
import java.lang.StringBuilder;
import fractallotus.fundacao.cerne.IEnumeravel;
import fractallotus.fundacao.cerne.IProjetor;

public class ExtensorPadrao
{
  @SuppressWarnings("unchecked")
  public static <T> T[] toArray(Class<T> tipo, IEnumeravel<T> colecao)
  {
    if (colecao == null)
      return null;

    int indice = 0;
    T[] result = (T[]) Array.newInstance(tipo, colecao.total());

    for (T item : colecao)
      result[indice++] = item;

    return result;
  }

  public static <T> String toString(IEnumeravel<T> colecao)
  {
    IProjetor<T, String> projetor = null;

    if (colecao == null)
      projetor = new IProjetor<T, String>()
      {
        public String projetar(T valor)
        {
          return (valor != null) ? valor.toString() : "null";
        }
      };

    return toString(colecao, projetor);
  }

  public static <T> String toString
  (
    IEnumeravel<T>       colecao,
    IProjetor<T, String> projetor
  )
  {
    if (colecao == null)
      return "[]";

    int total  = colecao.total(),
        indice = 1;

    StringBuilder sb = new StringBuilder();

    sb.append("[");

    for (T item : colecao)
    {
      sb.append( projetor.projetar(item) );

      if (indice++ < total)
        sb.append(", ");
    }

    sb.append("]");

    return sb.toString();
  }
}
