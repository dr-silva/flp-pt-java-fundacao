/*******************************************************************************
 *
 * Arquivo  : ImagemBitmap.java
 * Autor    : Daniel Raimundo da Silva
 * Data     : 2021-08-10 <yyyy-mm-dd>
 * Licença  : Este arquivo está sob licença Non-Comercial Creative Commons
 * Descrição: Esta classe tem o objetivo de desenhar uma imagem bitmap pixel por
 *            pixel.
 *
 *******************************************************************************/
package fractallotus.fundacao.entradas_saidas;

import java.io.File;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.IOException;
import java.awt.Color;
import java.awt.image.BufferedImage;
import javax.imageio.ImageIO;
import fractallotus.fundacao.recursos.Excecoes;

public class ImagemBitmap
{
  // ATRIBUTOS //
  private BufferedImage fBitmap;
  private boolean       fSuperior;
  private int           fAltura,
                        fLargura;

  // CONSTRUTORES //
  private ImagemBitmap(BufferedImage bitmap, boolean origem)
  {
    fBitmap   = bitmap;
    fSuperior = origem;
    fAltura   = bitmap.getHeight();
    fLargura  = bitmap.getWidth ();
  }

  public ImagemBitmap(int altura, int largura)
  {
    if (altura < 0)
      throw Excecoes.argumentoNegativo("altura");
    if (largura < 0)
      throw Excecoes.argumentoNegativo("largura");

    fBitmap   = new BufferedImage(largura, altura, BufferedImage.TYPE_INT_ARGB);
    fSuperior = true;
    fAltura   = altura;
    fLargura  = largura;
  }

  public ImagemBitmap(String arquivo) throws IOException
  {
    this(new File(arquivo));
  }

  public ImagemBitmap(File arquivo) throws IOException
  {
    this(ImageIO.read(arquivo), true);
  }

  public ImagemBitmap(InputStream stream) throws IOException
  {
    this(ImageIO.read(stream), true);
  }

  // public ImagemBitmap(Stream stream)

  // ASSISTENTES //
  private void rotar(boolean esquerda)
  {
    BufferedImage bitmap = new BufferedImage(fAltura, fLargura, BufferedImage.TYPE_INT_ARGB);

    for (int linha = 0; linha < fAltura; linha++)
      for (int coluna = 0; coluna < fLargura; coluna++)
        bitmap.setRGB
        (
          esquerda ? linha : fAltura - linha - 1, // nova coluna
          coluna,                                 // nova linha
          fBitmap.getRGB(coluna, linha)           // cor do ponto original
        );

    int temp = fAltura;
    fAltura  = fLargura;
    fLargura = temp;
    fBitmap  = bitmap;
  }

  // INTERFACE //
  public void definir(int linha, int coluna, float r, float g, float b)
  {
    definir(linha, coluna, 1, r, g, b);
  }

  public void definir(int linha, int coluna, float a, float r, float g, float b)
  {
    if ((a < 0) || (a > 1))
      throw Excecoes.indiceForaLimites("a", 0, 1);
    if ((r < 0) || (r > 1))
      throw Excecoes.indiceForaLimites("r", 0, 1);
    if ((g < 0) || (g > 1))
      throw Excecoes.indiceForaLimites("g", 0, 1);
    if ((b < 0) || (b > 1))
      throw Excecoes.indiceForaLimites("b", 0, 1);

    int A = (int)Math.min(255, a * 256),
        R = (int)Math.min(255, r * 256),
        G = (int)Math.min(255, g * 256),
        B = (int)Math.min(255, b * 256);

    pixel(linha, coluna, new Color(R, G, B, A));
  }

  public void originarDoSuperiorEsquerdo()
  {
    fSuperior = true;
  }

  public void originarDoInferiorEsquerdo()
  {
    fSuperior = false;
  }

  public void negativar()
  {
    for (int linha = 0; linha < fAltura; linha++)
      for (int coluna = 0; coluna < fLargura; coluna++)
      {
        Color cor = new Color(fBitmap.getRGB(coluna, linha));

        int a =       cor.getAlpha(),
            r = 255 - cor.getRed  (),
            g = 255 - cor.getGreen(),
            b = 255 - cor.getBlue ();

        cor = new Color(r, g, b, a);

        fBitmap.setRGB(coluna, linha, cor.getRGB());
      }
  }

  public void espelharHorizontalmente()
  {
    for (int linha = 0; linha < fAltura; linha++)
      for (int coluna = 0; coluna < fLargura / 2; coluna++)
      {
        int temp = fBitmap.getRGB(coluna, linha);

        fBitmap.setRGB(coluna, linha, fBitmap.getRGB(fLargura - coluna - 1, linha));
        fBitmap.setRGB(fLargura - coluna - 1, linha, temp);
      }
  }

  public void espelharVerticalmente()
  {
    for (int linha = 0; linha < fAltura / 2; linha++)
      for (int coluna = 0; coluna < fLargura; coluna++)
      {
        int temp = fBitmap.getRGB(coluna, linha);

        fBitmap.setRGB(coluna, linha, fBitmap.getRGB(coluna, fAltura - linha - 1));
        fBitmap.setRGB(coluna, fAltura - linha - 1, temp);
      }
  }

  public void rotarParaEsquerda()
  {
    rotar(true);
  }

  public void rotarParaDireita()
  {
    rotar(false);
  }

  public ImagemBitmap subimagem
  (
    int linhaInicial, int colunaInicial, int linhaFinal, int colunaFinal
  )
  {
    if ((linhaInicial < 0) || (linhaInicial >= fAltura))
      throw Excecoes.indiceForaLimites("linhaInicial", 0, fAltura - 1);
    if ((colunaInicial < 0) || (colunaInicial >= fLargura))
      throw Excecoes.indiceForaLimites("colunaInicial", 0, fLargura - 1);
    if ((linhaFinal < 0) || (linhaFinal >= fAltura))
      throw Excecoes.indiceForaLimites("linhaFinal", 0, fAltura - 1);
    if ((colunaFinal < 0) || (colunaFinal >= fLargura))
      throw Excecoes.indiceForaLimites("colunaFinal", 0, fLargura - 1);

    int linha   = Math.min(linhaInicial,  linhaFinal),
        coluna  = Math.min(colunaInicial, colunaFinal),
        altura  = Math.abs(linhaFinal  - linhaInicial)  + 1,
        largura = Math.abs(colunaFinal - colunaInicial) + 1;

    BufferedImage bitmap = fBitmap.getSubimage(coluna, linha, largura, altura);

    return new ImagemBitmap(bitmap, fSuperior);
  }

  public void salvar(String arquivo) throws IOException
  {
    salvar(new File(arquivo));
  }

  public void salvar(File arquivo) throws IOException
  {
    String nome     = arquivo.getName(),
           extensao = nome.substring(nome.lastIndexOf('.') + 1).toLowerCase();

    ImageIO.write(fBitmap, extensao, arquivo);
  }

  public void salvar(OutputStream stream, String formato) throws IOException
  {
    ImageIO.write(fBitmap, formato.toLowerCase(), stream);
  }

  // PROPRIEDADES //
  public final int altura()
  {
    return fAltura;
  }

  public final int largura()
  {
    return fLargura;
  }

  public Color pixel(int linha, int coluna)
  {
    if ((linha < 0) || (linha >= fAltura))
      throw Excecoes.indiceForaLimites("linha", 0, fAltura - 1);
    if ((coluna < 0) || (coluna >= fLargura))
      throw Excecoes.indiceForaLimites("coluna", 0, fLargura - 1);

    int rgb = fBitmap.getRGB(coluna, linha);

    return new Color(rgb);
  }

  public void pixel(int linha, int coluna, Color cor)
  {
    if ((linha < 0) || (linha >= fAltura))
      throw Excecoes.indiceForaLimites("linha", 0, fAltura - 1);
    if ((coluna < 0) || (coluna >= fLargura))
      throw Excecoes.indiceForaLimites("coluna", 0, fLargura - 1);

    fBitmap.setRGB(coluna, linha, cor.getRGB());
  }
}
