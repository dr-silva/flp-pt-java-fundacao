/*******************************************************************************
 *
 * Arquivo  : Excecoes.java
 * Autor    : Daniel Raimundo da Silva
 * Data     : 2020-10-02 <yyyy-mm-dd>
 * Licença  : Este arquivo está sob licença Non-Comercial Creative Commons
 * Descrição: Define uma classe de utilidade que cria as exceções necessárias
 *            deste pacote.
 *
 *******************************************************************************/
package fractallotus.fundacao.recursos;

public class Excecoes
{
  // ATRIBUTOS //
  private static final String S_PERIODO_INVALIDO    = "O parâmetro \"%s\" não corresponde a um período",
                              S_ARGUMENTO_NULO      = "O parâmetro \"%s\" não pode ser nulo",
                              S_ARGUMENTO_NEGATIVO  = "O parâmetro \"%s\" deve ser positivo",
                              S_INDICE_FORA_LIMITES = "O parâmetro \"%s\" está fora dos limites: %d e %d",
                              S_CHAVE_DICIONARIO    = "A chave informada não consta no dicionário",

                              S_OP_COLECAO_VAZIA    = "Operação inválida em coleção vazia",
                              S_OP_COLECAO_CHEIA    = "Operação inválida em coleção cheia";

  // MÉTODOS - CONSTRUTORES //
  private Excecoes()
  {
    throw new UnsupportedOperationException();
  }

  // MÉTODOS - INTERFACE //
  public static IllegalArgumentException periodoInvalido(String parametro)
  {
    return new IllegalArgumentException
    (
      String.format(S_PERIODO_INVALIDO, parametro)
    );
  }

  public static IllegalArgumentException argumentoNulo(String parametro)
  {
    return new IllegalArgumentException
    (
      String.format(S_ARGUMENTO_NULO, parametro)
    );
  }

  public static IllegalArgumentException argumentoNegativo(String parametro)
  {
    return new IllegalArgumentException
    (
      String.format(S_ARGUMENTO_NEGATIVO, parametro)
    );
  }

  public static IndexOutOfBoundsException indiceForaLimites(String parametro, int inferior, int superior)
  {
    return new IndexOutOfBoundsException
    (
      String.format(S_INDICE_FORA_LIMITES, parametro, inferior)
    );
  }

  public static IndexOutOfBoundsException chaveDicionario()
  {
    return new IndexOutOfBoundsException(S_CHAVE_DICIONARIO);
  }

  public static IllegalStateException colecaoVazia()
  {
    return new IllegalStateException(S_OP_COLECAO_VAZIA);
  }

  public static IllegalStateException colecaoCheia()
  {
    return new IllegalStateException(S_OP_COLECAO_CHEIA);
  }
}
