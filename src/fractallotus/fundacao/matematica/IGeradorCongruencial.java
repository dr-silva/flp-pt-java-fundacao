/*******************************************************************************
 *
 * Arquivo  : IGeradorCongruencial.java
 * Autor    : Daniel Raimundo da Silva
 * Data     : 2021-07-04 <yyyy-mm-dd>
 * Licença  : Este arquivo está sob licença Non-Comercial Creative Commons
 * Descrição: Define o protocolo dum gerador de números pseudo-aleatórios.
 *
 *******************************************************************************/
package fractallotus.fundacao.matematica;

public interface IGeradorCongruencial
{
  long semente();

  int    uniforme(int limite);
  double uniforme();
}
