/*******************************************************************************
 *
 * Arquivo  : Complexo.java
 * Autor    : Daniel Raimundo da Silva
 * Data     : 2021-09-25 <yyyy-mm-dd>
 * Licença  : Este arquivo está sob licença Non-Comercial Creative Commons
 * Descrição: Define um tipo de dado que representa um número complexo, um corpo
 *            representado por C e escrito na forma a + bi, para a, b \in R e i
 *            a constante imaginária (ou seja, i² = -1).
 *
 *******************************************************************************/
package fractallotus.fundacao.matematica;

import fractallotus.fundacao.cerne.IComparadorIgualdade;
import fractallotus.fundacao.cerne.IRedutor;
import fractallotus.fundacao.cerne.IProjetor;

public class Complexo
{
  // ATRIBUTOS
  private final double fRe,
                       fIm;

  // CONSTANTES
  public static final Complexo ZERO = new Complexo(0, 0);
  public static final Complexo NaN  = new Complexo(Double.NaN, Double.NaN);

  // CONSTRUTORES
  public Complexo(int valor)
  {
    this((double)valor, 0.0);
  }

  public Complexo(double valor)
  {
    this(valor, 0.0);
  }

  public Complexo(double re, double im)
  {
    fRe = re;
    fIm = im;
  }

  public static Complexo formaPolar(double raio, double theta)
  {
    return new Complexo(raio * Math.cos(theta), raio * Math.sin(theta));
  }

  // PROPRIEDADES
  public double   re         () { return fRe;                      }
  public double   im         () { return fIm;                      }
  public double   modulo     () { return Math.hypot(fRe, fIm);     }
  public double   argumento  () { return Math.atan2(fIm, fRe);     }
  public Complexo conjugado  () { return new Complexo(fRe, -fIm);  }
  public boolean  eReal      () { return (fRe != 0) && (fIm == 0); }
  public boolean  eImaginario() { return (fRe == 0) && (fIm != 0); }

  // MÉTODOS
  public boolean equals(Object obj)
  {
    return (obj instanceof Complexo) && equals(this, (Complexo)obj);
  }

  public int hashCode()
  {
    return (new Double(fRe)).hashCode() ^
           (new Double(fIm)).hashCode();
  }

  public String toString()
  {
    String re  = Double.toString(fRe),
           im  = Double.toString(Math.abs(fIm)),
           str = null;

    if (eZero(this))
      str = "0";
    else if (eReal())
      str = re;
    else if (eImaginario())
      str = Double.toString(fIm) + "i";
    else if (fIm > 0)
      str = re + " + " + im + "i";
    else
      str = re + " - " + im + "i";

    return str;
  }

  // OPERADORES - COMPARAÇÃO
  public static boolean equals(Complexo esquerda, Complexo direita)
  {
    return (esquerda.fRe == direita.fRe) &&
           (esquerda.fIm == direita.fIm);
  }

  public static boolean notEquals(Complexo esquerda, Complexo direita)
  {
    return !equals(esquerda, direita);
  }

  // OPERADORES - UNÁRIOS
  public static Complexo positive(Complexo valor)
  {
    return valor;
  }

  public static Complexo negative(Complexo valor)
  {
    return new Complexo(-valor.fRe, -valor.fIm);
  }

  // OPERADORES - ADIÇÃO
  public static Complexo add(int esquerda, Complexo direita)
  {
    return add(direita,  (double)esquerda);
  }

  public static Complexo add(double esquerda, Complexo direita)
  {
    return add(direita,  esquerda);
  }

  public static Complexo add(Complexo esquerda, int direita)
  {
    return add(esquerda, (double)direita);
  }

  public static Complexo add(Complexo esquerda, double direita)
  {
    return new Complexo(esquerda.fRe + direita, esquerda.fIm);
  }

  public static Complexo add(Complexo esquerda, Complexo direita)
  {
    return new Complexo(esquerda.fRe + direita.fRe, esquerda.fIm + direita.fIm);
  }

  // OPERADORES - SUBTRAÇÃO
  public static Complexo subtract(int esquerda, Complexo direita)
  {
    return subtract((double)esquerda, direita);
  }

  public static Complexo subtract(double esquerda, Complexo direita)
  {
    return add(esquerda, negative(direita));
  }

  public static Complexo subtract(Complexo esquerda, int direita)
  {
    return subtract(esquerda, (double)direita);
  }

  public static Complexo subtract(Complexo esquerda, double direita)
  {
    return add(esquerda, -direita);
  }

  public static Complexo subtract(Complexo esquerda, Complexo direita)
  {
    return new Complexo(esquerda.fRe - direita.fRe, esquerda.fIm - direita.fIm);
  }

  // OPERADORES - MULTIPLICAÇÃO
  public static Complexo multiply(int esquerda, Complexo direita)
  {
    return multiply(direita, (double)esquerda);
  }

  public static Complexo multiply(double esquerda, Complexo direita)
  {
    return multiply(direita, esquerda);
  }

  public static Complexo multiply(Complexo esquerda, int direita)
  {
    return multiply(esquerda, (double)direita);
  }

  public static Complexo multiply(Complexo esquerda, double direita)
  {
    return new Complexo(esquerda.fRe * direita, esquerda.fIm * direita);
  }

  public static Complexo multiply(Complexo esquerda, Complexo direita)
  {
    return new Complexo
    (
      esquerda.fRe * direita.fRe - esquerda.fIm * direita .fIm,
      esquerda.fRe * direita.fIm + direita .fRe * esquerda.fIm
    );
  }

  // OPERADORES - DIVISÃO
  public static Complexo divide(int esquerda, Complexo direita)
  {
    return divide((double)esquerda, direita);
  }

  public static Complexo divide(double esquerda, Complexo direita)
  {
    return divide(new Complexo(esquerda), direita);
  }

  public static Complexo divide(Complexo esquerda, int direita)
  {
    return divide(esquerda, (double)direita);
  }

  public static Complexo divide(Complexo esquerda, double direita)
  {
    return (direita == 0) ? NaN
                          : new Complexo(esquerda.fRe / direita, esquerda.fIm / direita);
  }

  public static Complexo divide(Complexo esquerda, Complexo direita)
  {
    if (eZero(direita))
      return NaN;

    double raio  = esquerda.modulo()    / direita.modulo(),
           theta = esquerda.argumento() - direita.argumento();

    return formaPolar(raio, theta);
  }

  // FUNÇÕES
  public static boolean eZero(Complexo valor)
  {
    return (valor.fRe == 0) && (valor.fIm == 0);
  }

  public static boolean eZero(Complexo valor, double epsilon)
  {
    return valor.modulo() < epsilon;
  }

  public static boolean eNaN(Complexo valor)
  {
    return Double.isNaN     (valor.fRe) ||
           Double.isNaN     (valor.fIm) ||
           Double.isInfinite(valor.fRe) ||
           Double.isInfinite(valor.fIm);
  }

  public static double abs(Complexo valor)
  {
    return valor.modulo();
  }

  public static double arg(Complexo valor)
  {
    return valor.argumento();
  }

  public static double dist(Complexo esquerda, Complexo direita)
  {
    return subtract(esquerda, direita).modulo();
  }

  public static Complexo conj(Complexo valor)
  {
    return valor.conjugado();
  }

  public static Complexo exp(Complexo expoente)
  {
    return formaPolar(Math.exp(expoente.fRe), expoente.fIm);
  }

  public static Complexo exp(Complexo base, Complexo expoente)
  {
    return exp( multiply(expoente, ln(base)) );
  }

  public static Complexo ln(Complexo valor)
  {
    return new Complexo(Math.log(valor.modulo()), valor.argumento());
  }

  public static Complexo log(Complexo base, Complexo valor)
  {
    return divide(ln(valor), ln(base));
  }

  public static Complexo sen(Complexo valor)
  {
    return new Complexo
    (
      Math.sin(valor.fRe) * Math.cosh(valor.fIm),
      Math.cos(valor.fRe) * Math.sinh(valor.fIm)
    );
  }

  public static Complexo cos(Complexo valor)
  {
    return new Complexo
    (
      + Math.cos(valor.fRe) * Math.cosh(valor.fIm),
      - Math.sin(valor.fRe) * Math.sinh(valor.fIm)
    );
  }

  public static Complexo tan(Complexo valor)
  {
    return divide(sen(valor), cos(valor));
  }

  public static Complexo csc(Complexo valor)
  {
    return divide(1, sen(valor));
  }

  public static Complexo sec(Complexo valor)
  {
    return divide(1, cos(valor));
  }

  public static Complexo cot(Complexo valor)
  {
    return divide(cos(valor), sen(valor));
  }

  public static Complexo senh(Complexo valor)
  {
    return multiply(0.5, subtract(exp(valor), exp(negative(valor))));
  }

  public static Complexo cosh(Complexo valor)
  {
    return multiply(0.5, add(exp(valor), exp(negative(valor))));
  }

  public static Complexo tanh(Complexo valor)
  {
    return divide(senh(valor), cosh(valor));
  }

  public static Complexo csch(Complexo valor)
  {
    return divide(1, senh(valor));
  }

  public static Complexo sech(Complexo valor)
  {
    return divide(1, cosh(valor));
  }

  public static Complexo coth(Complexo valor)
  {
    return divide(cosh(valor), senh(valor));
  }

  // PROTOCOLOS DE FUNÇÕES
  public static IComparadorIgualdade<Complexo> comparadorIgualdade()
  {
    return comparadorIgualdade(Matematica.EPSILON);
  }

  public static IComparadorIgualdade<Complexo> comparadorIgualdade(double epsilon)
  {
    return new IComparadorIgualdade<Complexo>()
    {
      private final double fEpsilon = (epsilon <= 0) ? Matematica.EPSILON : epsilon;

      public boolean eIgual(Complexo esquerda, Complexo direita)
      {
        return dist(esquerda, direita) < fEpsilon;
      }

      public int obterHash(Complexo valor)
      {
        return (valor != null) ? valor.hashCode() : 42;
      }
    };
  }

  public static IRedutor<Complexo, Complexo> redutorSoma()
  {
    return new IRedutor<Complexo, Complexo>()
    {
      public Complexo reduzir(Complexo valor, Complexo item)
      {
        return add(valor, item);
      }
    };
  }

  public static IRedutor<Complexo, Complexo> redutorMultiplicacao()
  {
    return new IRedutor<Complexo, Complexo>()
    {
      public Complexo reduzir(Complexo valor, Complexo item)
      {
        return multiply(valor, item);
      }
    };
  }

  public static IProjetor<Complexo, String> projetorString()
  {
    return new IProjetor<Complexo, String>()
    {
      public String projetar(Complexo valor)
      {
        return (valor != null) ? valor.toString() : null;
      }
    };
  }
}
