/*******************************************************************************
 *
 * Arquivo  : ComparadorIgualdadeFloat.java
 * Autor    : Daniel Raimundo da Silva
 * Data     : 2021-07-09 <yyyy-mm-dd>
 * Licença  : Este arquivo está sob licença Non-Comercial Creative Commons
 * Descrição: Define uma implementação do comparador de igualdade para ser usada
 *            em contextos nos quais x = y se, e somente se, |x - y| < epsilon.
 *
 *******************************************************************************/
package fractallotus.fundacao.matematica;

import fractallotus.fundacao.cerne.IComparadorIgualdade;

class ComparadorIgualdadeFloat implements IComparadorIgualdade<Float>
{
  private float epsilon;

  public ComparadorIgualdadeFloat(float epsilon)
  {
    this.epsilon = epsilon;
  }

  public boolean eIgual(Float esquerda, Float direita)
  {
    return Math.abs(direita - esquerda) < epsilon;
  }

  public int obterHash(Float obj)
  {
    return Float.hashCode(obj);
  }
}
