/*******************************************************************************
 *
 * Arquivo  : TipoGeradorCongruencial.java
 * Autor    : Daniel Raimundo da Silva
 * Data     : 2021-08-22 <yyyy-mm-dd>
 * Licença  : Este arquivo está sob licença Non-Comercial Creative Commons
 * Descrição: Define uma enumeração para ser os tipos possíveis de geradores
 *            congruenciais.
 *
 *******************************************************************************/
package fractallotus.fundacao.matematica;

public enum TipoGeradorCongruencial
{
  Linear   (0x01),
  NaoLinear(0x02);

  public final int valor;

  TipoGeradorCongruencial(int valor)
  {
    this.valor = valor;
  }
}
