/*******************************************************************************
 *
 * Arquivo  : ComparadorFloat.java
 * Autor    : Daniel Raimundo da Silva
 * Data     : 2021-07-09 <yyyy-mm-dd>
 * Licença  : Este arquivo está sob licença Non-Comercial Creative Commons
 * Descrição: Define uma implementação do comparador para ser usada em contextos
 *            nos quais:
 *              - x < y se, e somente se,  x - y <= -epsilon;
 *              - x > y se, e somente se,  x - y >= +epsilon;
 *              - x = y se, e somente se, |x - y| < +epsilon;
 *
 *******************************************************************************/
package fractallotus.fundacao.matematica;

import fractallotus.fundacao.cerne.IComparador;

class ComparadorFloat implements IComparador<Float>
{
  private float epsilon;

  public ComparadorFloat(float epsilon)
  {
    this.epsilon = epsilon;
  }

  public int compare(Float esquerda, Float direita)
  {
    int   result;
    float valor = esquerda - direita;

         if (valor <= -epsilon) result = -1;
    else if (valor >= +epsilon) result = +1;
    else                        result =  0;

    return result;
  }
}
