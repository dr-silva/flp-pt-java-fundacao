/*******************************************************************************
 *
 * Arquivo  : FabricaGeradorCongruencial.java
 * Autor    : Daniel Raimundo da Silva
 * Data     : 2021-08-22 <yyyy-mm-dd>
 * Licença  : Este arquivo está sob licença Non-Comercial Creative Commons
 * Descrição: Classe de suporte para gerar instâncias do IGeradorCongruencial
 *            de acordo com os registros feitos pelos pacotes da Fractal Lotus.
 *
 *******************************************************************************/
package fractallotus.fundacao.matematica;

import java.util.ServiceLoader;
import java.util.ServiceConfigurationError;
import fractallotus.fundacao.cerne.IListavel;
import fractallotus.fundacao.colecoes.ListaEncadeada;
import fractallotus.fundacao.matematica.Matematica;
import fractallotus.fundacao.matematica.spi.IServicoGeradorCongruencial;

public class FabricaGeradorCongruencial
{
  // ASSISTENTES //
  private static synchronized IListavel<IServicoGeradorCongruencial> listarServicos()
  {
    IListavel<IServicoGeradorCongruencial>     result = null;
    ServiceLoader<IServicoGeradorCongruencial> loader = ServiceLoader.load
    (
      IServicoGeradorCongruencial.class
    );

    try
    {
      result = new ListaEncadeada<IServicoGeradorCongruencial>();

      for (IServicoGeradorCongruencial servico : loader)
        result.adicionar(servico);
    }
    catch (ServiceConfigurationError error)
    {
      result = null;
      error.printStackTrace();
    }

    return result;
  }

  private static synchronized IListavel<IServicoGeradorCongruencial> listarServicos
  (
    TipoGeradorCongruencial tipo
  )
  {
    if (tipo == null)
      return null;

    IListavel<IServicoGeradorCongruencial>     result = null;
    ServiceLoader<IServicoGeradorCongruencial> loader = ServiceLoader.load
    (
      IServicoGeradorCongruencial.class
    );

    try
    {
      result = new ListaEncadeada<IServicoGeradorCongruencial>();

      for (IServicoGeradorCongruencial servico : loader)
        if (servico.validar(tipo))
          result.adicionar(servico);
    }
    catch (ServiceConfigurationError error)
    {
      result = null;
      error.printStackTrace();
    }

    return result;
  }

  private static synchronized IServicoGeradorCongruencial obterServico
  (
    String nome, TipoGeradorCongruencial tipo
  )
  {
    if ((tipo == null) || (nome == null) || nome.equals(""))
      return null;

    IServicoGeradorCongruencial                result = null;
    ServiceLoader<IServicoGeradorCongruencial> loader = ServiceLoader.load
    (
      IServicoGeradorCongruencial.class
    );

    try
    {
      for (IServicoGeradorCongruencial servico : loader)
        if (servico.validar(nome, tipo))
        {
          result = servico;
          break;
        }
    }
    catch (ServiceConfigurationError error)
    {
      result = null;
      error.printStackTrace();
    }

    return result;
  }

  private static IGeradorCongruencial criar
  (
    IListavel<IServicoGeradorCongruencial> lista, long semente
  )
  {
    IServicoGeradorCongruencial servico = null;

    if (lista.total() > 0)
      servico = lista.item( Matematica.uniforme(lista.total()) );

    return (servico != null) ? servico.criar(semente) : null;
  }

  // INTERFACE //
  public static IGeradorCongruencial criar()
  {
    return criar(System.currentTimeMillis());
  }

  public static IGeradorCongruencial criar(long semente)
  {
    IListavel<IServicoGeradorCongruencial> lista = listarServicos();

    return (lista != null) ? criar(lista, semente) : null;
  }

  public static IGeradorCongruencial criar(TipoGeradorCongruencial tipo)
  {
    return criar(tipo, System.currentTimeMillis());
  }

  public static IGeradorCongruencial criar
  (
    TipoGeradorCongruencial tipo, long semente
  )
  {
    IListavel<IServicoGeradorCongruencial> lista = listarServicos(tipo);

    return (lista != null) ? criar(lista, semente) : null;
  }

  public static IGeradorCongruencial criar
  (
    String nome, TipoGeradorCongruencial tipo
  )
  {
    return criar(nome, tipo, System.currentTimeMillis());
  }

  public static IGeradorCongruencial criar
  (
    String nome, TipoGeradorCongruencial tipo, long semente
  )
  {
    IServicoGeradorCongruencial servico = obterServico(nome, tipo);

    return (servico != null) ? servico.criar(semente) : null;
  }
}