/*******************************************************************************
 *
 * Arquivo  : ComparadorIgualdadeDouble.java
 * Autor    : Daniel Raimundo da Silva
 * Data     : 2021-07-04 <yyyy-mm-dd>
 * Licença  : Este arquivo está sob licença Non-Comercial Creative Commons
 * Descrição: Define uma implementação do comparador de igualdade para ser usada
 *            em contextos nos quais x = y se, e somente se, |x - y| < epsilon.
 *
 *******************************************************************************/
package fractallotus.fundacao.matematica;

import fractallotus.fundacao.cerne.IComparadorIgualdade;

class ComparadorIgualdadeDouble implements IComparadorIgualdade<Double>
{
  private double epsilon;

  public ComparadorIgualdadeDouble(double epsilon)
  {
    this.epsilon = epsilon;
  }

  public boolean eIgual(Double esquerda, Double direita)
  {
    return Math.abs(direita - esquerda) < epsilon;
  }

  public int obterHash(Double obj)
  {
    return Double.hashCode(obj);
  }
}
