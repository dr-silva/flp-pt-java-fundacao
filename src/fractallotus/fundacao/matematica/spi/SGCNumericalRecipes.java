/*******************************************************************************
 *
 * Arquivo  : SGCNumericalRecipes.java
 * Autor    : Daniel Raimundo da Silva
 * Data     : 2021-08-28 <yyyy-mm-dd>
 * Licença  : Este arquivo está sob licença Non-Comercial Creative Commons
 * Descrição: Define o gerador congruencial linear com as especificações do
 *            Numerical Recipes, assim como o serviço responsável pela sua ins-
 *            tanciação.
 *
 *******************************************************************************/
package fractallotus.fundacao.matematica.spi;

import fractallotus.fundacao.matematica.IGeradorCongruencial;
import fractallotus.fundacao.matematica.TipoGeradorCongruencial;
import fractallotus.fundacao.recursos.Excecoes;

public class SGCNumericalRecipes implements IServicoGeradorCongruencial
{
  public boolean validar(TipoGeradorCongruencial tipo)
  {
    return tipo == TipoGeradorCongruencial.Linear;
  }

  public boolean validar(String nome, TipoGeradorCongruencial tipo)
  {
    return nome.equals("numerical-recipes") && validar(tipo);
  }

  public IGeradorCongruencial criar(long semente)
  {
    return new GCLNumericalRecipes(semente);
  }

  // SUBCLASSE - GERADOR CONGRUENCIAL //
  private class GCLNumericalRecipes implements IGeradorCongruencial
  {
    // ATRIBUTOS
    private long fValor,
                 fSemente;

    // CONSTRUTOR
    public GCLNumericalRecipes(long semente)
    {
      fValor = fSemente = semente;
    }

    // ASSISTENTE
    private long proximo()
    {
      fValor = (fValor * 0x19660D + 0x3C6EF35F) & 0xFFFFFFFFL; // mod 2^32
      return fValor;
    }

    // INTERFACE
    public long semente()
    {
      return fSemente;
    }

    public int uniforme(int limite)
    {
      if (limite <= 0)
        throw Excecoes.argumentoNegativo("limite");

      return (int)(proximo() % (long)limite);
    }

    public double uniforme()
    {
      return proximo() * (1.0 / 0x100000000L); // 2^-32
    }
  }
}