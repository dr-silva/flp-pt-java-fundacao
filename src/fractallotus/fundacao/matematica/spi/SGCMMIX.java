/*******************************************************************************
 *
 * Arquivo  : SGCMMIX.java
 * Autor    : Daniel Raimundo da Silva
 * Data     : 2021-08-28 <yyyy-mm-dd>
 * Licença  : Este arquivo está sob licença Non-Comercial Creative Commons
 * Descrição: Define o gerador congruencial linear com as especificações do
 *            MMIX desenhado por Donald Knuth. Assim como o serviço responsável
 *            pela sua instanciação.
 *
 *******************************************************************************/
package fractallotus.fundacao.matematica.spi;

import fractallotus.fundacao.matematica.IGeradorCongruencial;
import fractallotus.fundacao.matematica.TipoGeradorCongruencial;
import fractallotus.fundacao.recursos.Excecoes;

public class SGCMMIX implements IServicoGeradorCongruencial
{
  public boolean validar(TipoGeradorCongruencial tipo)
  {
    return tipo == TipoGeradorCongruencial.Linear;
  }

  public boolean validar(String nome, TipoGeradorCongruencial tipo)
  {
    return nome.equals("mmix") && validar(tipo);
  }

  public IGeradorCongruencial criar(long semente)
  {
    return new GCLMMIX(semente);
  }

  // SUBCLASSE - GERADOR CONGRUENCIAL //
  private class GCLMMIX implements IGeradorCongruencial
  {
    // ATRIBUTOS
    private long fValor,
                 fSemente;

    // CONSTRUTOR
    public GCLMMIX(long semente)
    {
      fValor = fSemente = semente;
    }

    // ASSISTENTE
    private void proximo()
    {
      fValor = fValor * 0x5851F42D4C957F2DL + 0x14057B7EF767814FL;
    }

    private long parcela1()
    {
      return (fValor >= 0) ? fValor : Long.MAX_VALUE;
    }

    private long parcela2()
    {
      return (fValor >= 0) ? 0 : Long.MAX_VALUE + fValor + 2;
    }

    // INTERFACE
    public long semente()
    {
      return fSemente;
    }

    public int uniforme(int limite)
    {
      if (limite <= 0)
        throw Excecoes.argumentoNegativo("limite");

      proximo();

      long m = limite,
           a = parcela1() % m,
           b = parcela2() % m;

      return (int)((a + b) % m);
    }

    public double uniforme()
    {
      proximo();

      double d = 1.0 / 0x100000000L, // 2^-32
             a = parcela1(),
             b = parcela2();

      return (a * d * d) + (b * d * d);
    }
  }
}