/*******************************************************************************
 *
 * Arquivo  : SGCPosix.java
 * Autor    : Daniel Raimundo da Silva
 * Data     : 2021-08-28 <yyyy-mm-dd>
 * Licença  : Este arquivo está sob licença Non-Comercial Creative Commons
 * Descrição: Define o gerador congruencial linear com as especificações do
 *            POSIX (Portable Operating System Interface), uma família de
 *            padrões especificados pela IEEE Computer Society. Assim como o
 *            serviço responsável pela sua instanciação.
 *
 *******************************************************************************/
package fractallotus.fundacao.matematica.spi;

import fractallotus.fundacao.matematica.IGeradorCongruencial;
import fractallotus.fundacao.matematica.TipoGeradorCongruencial;
import fractallotus.fundacao.recursos.Excecoes;

public class SGCPosix implements IServicoGeradorCongruencial
{
  public boolean validar(TipoGeradorCongruencial tipo)
  {
    return tipo == TipoGeradorCongruencial.Linear;
  }

  public boolean validar(String nome, TipoGeradorCongruencial tipo)
  {
    return nome.equals("posix") && validar(tipo);
  }

  public IGeradorCongruencial criar(long semente)
  {
    return new GCLPosix(semente);
  }

  // SUBCLASSE - GERADOR CONGRUENCIAL //
  private class GCLPosix implements IGeradorCongruencial
  {
    // ATRIBUTOS
    private long fValor,
                 fSemente;

    // CONSTRUTOR
    public GCLPosix(long semente)
    {
      fValor = fSemente = semente;
    }

    // ASSISTENTE
    private long proximo()
    {
      fValor = (fValor * 0x5DEECE66DL + 11) & 0xFFFFFFFFFFFFL;
      return (fValor >>> 16);
    }

    // INTERFACE
    public long semente()
    {
      return fSemente;
    }

    public int uniforme(int limite)
    {
      if (limite <= 0)
        throw Excecoes.argumentoNegativo("limite");

      return (int)(proximo() % (long)limite);
    }

    public double uniforme()
    {
      return proximo() * (1.0 / 0x100000000L); // 2^-32
    }
  }
}