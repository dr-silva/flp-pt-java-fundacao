/*******************************************************************************
 *
 * Arquivo  : SGCRtlUniform.java
 * Autor    : Daniel Raimundo da Silva
 * Data     : 2021-08-28 <yyyy-mm-dd>
 * Licença  : Este arquivo está sob licença Non-Comercial Creative Commons
 * Descrição: Define o gerador congruencial linear com as especificações do
 *            RtlUniform da Native API usada pelo Windows NT, assim como o
 *            serviço responsável pela sua instanciação.
 *
 *******************************************************************************/
package fractallotus.fundacao.matematica.spi;

import fractallotus.fundacao.matematica.IGeradorCongruencial;
import fractallotus.fundacao.matematica.TipoGeradorCongruencial;
import fractallotus.fundacao.recursos.Excecoes;

public class SGCRtlUniform implements IServicoGeradorCongruencial
{
  public boolean validar(TipoGeradorCongruencial tipo)
  {
    return tipo == TipoGeradorCongruencial.Linear;
  }

  public boolean validar(String nome, TipoGeradorCongruencial tipo)
  {
    return nome.equals("rtl-uniform") && validar(tipo);
  }

  public IGeradorCongruencial criar(long semente)
  {
    return new GCLRtlUniform(semente);
  }

  // SUBCLASSE - GERADOR CONGRUENCIAL //
  private class GCLRtlUniform implements IGeradorCongruencial
  {
    // ATRIBUTOS
    private int  fValor;
    private long fSemente;

    // CONSTRUTOR
    public GCLRtlUniform(long semente)
    {
      fValor   = (int)semente;
      fSemente = semente;
    }

    // ASSISTENTE
    private int proximo()
    {
      fValor = (fValor * 0x7FFFFFED + 0x7FFFFFC3) & 0x7FFFFFFF;
      return fValor;
    }

    // INTERFACE
    public long semente()
    {
      return fSemente;
    }

    public int uniforme(int limite)
    {
      if (limite <= 0)
        throw Excecoes.argumentoNegativo("limite");

      return proximo() % limite;
    }

    public double uniforme()
    {
      return proximo() * (1.0 / 0x80000000L); // 2^-31
    }
  }
}