/*******************************************************************************
 *
 * Arquivo  : SGCBorlandC.java
 * Autor    : Daniel Raimundo da Silva
 * Data     : 2021-08-28 <yyyy-mm-dd>
 * Licença  : Este arquivo está sob licença Non-Comercial Creative Commons
 * Descrição: Define o gerador congruencial linear com as especificações do
 *            Borland C, assim como o serviço responsável pela sua instanciação.
 *
 *******************************************************************************/
package fractallotus.fundacao.matematica.spi;

import fractallotus.fundacao.matematica.IGeradorCongruencial;
import fractallotus.fundacao.matematica.TipoGeradorCongruencial;
import fractallotus.fundacao.recursos.Excecoes;

public class SGCBorlandC implements IServicoGeradorCongruencial
{
  public boolean validar(TipoGeradorCongruencial tipo)
  {
    return tipo == TipoGeradorCongruencial.Linear;
  }

  public boolean validar(String nome, TipoGeradorCongruencial tipo)
  {
    return nome.equals("borland-c") && validar(tipo);
  }

  public IGeradorCongruencial criar(long semente)
  {
    return new GCLBorlandC(semente);
  }

  // SUBCLASSE - GERADOR CONGRUENCIAL //
  private class GCLBorlandC implements IGeradorCongruencial
  {
    // ATRIBUTOS
    private int  fValor;
    private long fSemente;

    // CONSTRUTOR
    public GCLBorlandC(long semente)
    {
      fValor   = (int)semente;
      fSemente = semente;
    }

    // ASSISTENTE
    private int proximo()
    {
      fValor = (fValor * 0x015A4E35 + 1) & 0x7FFFFFFF; // mod 2^30
      return fValor;
    }

    // INTERFACE
    public long semente()
    {
      return fSemente;
    }

    public int uniforme(int limite)
    {
      if (limite <= 0)
        throw Excecoes.argumentoNegativo("limite");

      return proximo() % limite;
    }

    public double uniforme()
    {
      return proximo() * (1.0 / 0x80000000L); // 2^-31
    }
  }
}