/*******************************************************************************
 *
 * Arquivo  : IServicoGeradorCongruencial.java
 * Autor    : Daniel Raimundo da Silva
 * Data     : 2021-08-22 <yyyy-mm-dd>
 * Licença  : Este arquivo está sob licença Non-Comercial Creative Commons
 * Descrição: Define o protocolo do serviço para o gerador de números pseudo-
 *            -aleatórios.
 *
 *******************************************************************************/
package fractallotus.fundacao.matematica.spi;

import fractallotus.fundacao.matematica.IGeradorCongruencial;
import fractallotus.fundacao.matematica.TipoGeradorCongruencial;

public interface IServicoGeradorCongruencial
{
  boolean validar(TipoGeradorCongruencial tipo);
  boolean validar(String nome, TipoGeradorCongruencial tipo);

  IGeradorCongruencial criar(long semente);
}