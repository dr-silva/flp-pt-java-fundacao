/*******************************************************************************
 *
 * Arquivo  : SGCAppleCarbon.java
 * Autor    : Daniel Raimundo da Silva
 * Data     : 2021-08-28 <yyyy-mm-dd>
 * Licença  : Este arquivo está sob licença Non-Comercial Creative Commons
 * Descrição: Define o gerador congruencial linear com as especificações do
 *            Apple Carbon desenvolvido pela Apple para o Mac OS, assim como o
 *            serviço responsável pela sua instanciação.
 *
 *******************************************************************************/
package fractallotus.fundacao.matematica.spi;

import fractallotus.fundacao.matematica.IGeradorCongruencial;
import fractallotus.fundacao.matematica.TipoGeradorCongruencial;
import fractallotus.fundacao.recursos.Excecoes;

public class SGCAppleCarbon implements IServicoGeradorCongruencial
{
  public boolean validar(TipoGeradorCongruencial tipo)
  {
    return tipo == TipoGeradorCongruencial.Linear;
  }

  public boolean validar(String nome, TipoGeradorCongruencial tipo)
  {
    return nome.equals("apple-carbon") && validar(tipo);
  }

  public IGeradorCongruencial criar(long semente)
  {
    return new GCLAppleCarbon(semente);
  }

  // SUBCLASSE - GERADOR CONGRUENCIAL //
  private class GCLAppleCarbon implements IGeradorCongruencial
  {
    // ATRIBUTOS
    private int  fValor;
    private long fSemente;

    // CONSTRUTOR
    public GCLAppleCarbon(long semente)
    {
      fValor   = (int)semente;
      fSemente = semente;
    }

    // ASSISTENTE
    private int proximo()
    {
      fValor = (fValor * 0x41A7) & 0x7FFFFFFF;
      return fValor;
    }

    // INTERFACE
    public long semente()
    {
      return fSemente;
    }

    public int uniforme(int limite)
    {
      if (limite <= 0)
        throw Excecoes.argumentoNegativo("limite");

      return proximo() % limite;
    }

    public double uniforme()
    {
      return proximo() * (1.0 / 0x80000000L); // 2^-31
    }
  }
}