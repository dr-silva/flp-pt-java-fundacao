/*******************************************************************************
 *
 * Arquivo  : SGCBorlandPascal.java
 * Autor    : Daniel Raimundo da Silva
 * Data     : 2021-08-28 <yyyy-mm-dd>
 * Licença  : Este arquivo está sob licença Non-Comercial Creative Commons
 * Descrição: Define o gerador congruencial linear com as especificações do
 *            Borland Pascal, uma biblioteca padrão para o C. Assim como o ser-
 *            viço responsável pela sua instanciação.
 *
 *******************************************************************************/
package fractallotus.fundacao.matematica.spi;

import fractallotus.fundacao.matematica.IGeradorCongruencial;
import fractallotus.fundacao.matematica.TipoGeradorCongruencial;
import fractallotus.fundacao.recursos.Excecoes;

public class SGCBorlandPascal implements IServicoGeradorCongruencial
{
  public boolean validar(TipoGeradorCongruencial tipo)
  {
    return tipo == TipoGeradorCongruencial.Linear;
  }

  public boolean validar(String nome, TipoGeradorCongruencial tipo)
  {
    return nome.equals("borland-pascal") && validar(tipo);
  }

  public IGeradorCongruencial criar(long semente)
  {
    return new GCLBorlandPascal(semente);
  }

  // SUBCLASSE - GERADOR CONGRUENCIAL //
  private class GCLBorlandPascal implements IGeradorCongruencial
  {
    // ATRIBUTOS
    private long fValor,
                 fSemente;

    // CONSTRUTOR
    public GCLBorlandPascal(long semente)
    {
      fValor = fSemente = semente;
    }

    // ASSISTENTE
    private long proximo()
    {
      fValor = (fValor * 0x08088405 + 1) & 0xFFFFFFFFL;
      return fValor;
    }

    // INTERFACE
    public long semente()
    {
      return fSemente;
    }

    public int uniforme(int limite)
    {
      if (limite <= 0)
        throw Excecoes.argumentoNegativo("limite");

      long result = (proximo() * limite);
      return (int)(result >>> 32);
    }

    public double uniforme()
    {
      return proximo() * (1.0 / 0x100000000L); // 2^-32
    }
  }
}