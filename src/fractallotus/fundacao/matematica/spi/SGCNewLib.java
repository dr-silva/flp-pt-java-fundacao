/*******************************************************************************
 *
 * Arquivo  : SGCNewLib.java
 * Autor    : Daniel Raimundo da Silva
 * Data     : 2021-08-28 <yyyy-mm-dd>
 * Licença  : Este arquivo está sob licença Non-Comercial Creative Commons
 * Descrição: Define o gerador congruencial linear com as especificações do
 *            New Lib, uma biblioteca padrão para o C. Assim como o serviço
 *            responsável pela sua instanciação.
 *
 *******************************************************************************/
package fractallotus.fundacao.matematica.spi;

import fractallotus.fundacao.matematica.IGeradorCongruencial;
import fractallotus.fundacao.matematica.TipoGeradorCongruencial;
import fractallotus.fundacao.recursos.Excecoes;

public class SGCNewLib implements IServicoGeradorCongruencial
{
  public boolean validar(TipoGeradorCongruencial tipo)
  {
    return tipo == TipoGeradorCongruencial.Linear;
  }

  public boolean validar(String nome, TipoGeradorCongruencial tipo)
  {
    return nome.equals("new-lib") && validar(tipo);
  }

  public IGeradorCongruencial criar(long semente)
  {
    return new GCLNewLib(semente);
  }

  // SUBCLASSE - GERADOR CONGRUENCIAL //
  private class GCLNewLib implements IGeradorCongruencial
  {
    // ATRIBUTOS
    private long fValor,
                 fSemente;

    // CONSTRUTOR
    public GCLNewLib(long semente)
    {
      fValor = fSemente = semente;
    }

    // ASSISTENTE
    private long proximo()
    {
      fValor = fValor * 0x5851F42D4C957F2DL + 1;
      return (fValor >>> 32);
    }

    // INTERFACE
    public long semente()
    {
      return fSemente;
    }

    public int uniforme(int limite)
    {
      if (limite <= 0)
        throw Excecoes.argumentoNegativo("limite");

      return (int)(proximo() % (long)limite);
    }

    public double uniforme()
    {
      return proximo() * (1.0 / 0x100000000L); // 2^-32
    }
  }
}