/*******************************************************************************
 *
 * Arquivo  : SGCOpenVms.java
 * Autor    : Daniel Raimundo da Silva
 * Data     : 2021-08-28 <yyyy-mm-dd>
 * Licença  : Este arquivo está sob licença Non-Comercial Creative Commons
 * Descrição: Define o gerador congruencial linear com as especificações do
 *            Open VMS, um sistema operacional. Assim como o serviço responsável
 *            pela sua instanciação.
 *
 *******************************************************************************/
package fractallotus.fundacao.matematica.spi;

import fractallotus.fundacao.matematica.IGeradorCongruencial;
import fractallotus.fundacao.matematica.TipoGeradorCongruencial;
import fractallotus.fundacao.recursos.Excecoes;

public class SGCOpenVms implements IServicoGeradorCongruencial
{
  public boolean validar(TipoGeradorCongruencial tipo)
  {
    return tipo == TipoGeradorCongruencial.Linear;
  }

  public boolean validar(String nome, TipoGeradorCongruencial tipo)
  {
    return nome.equals("open-vms") && validar(tipo);
  }

  public IGeradorCongruencial criar(long semente)
  {
    return new GCLOpenVms(semente);
  }

  // SUBCLASSE - GERADOR CONGRUENCIAL //
  private class GCLOpenVms implements IGeradorCongruencial
  {
    // ATRIBUTOS
    private long fValor,
                 fSemente;

    // CONSTRUTOR
    public GCLOpenVms(long semente)
    {
      fValor = fSemente = semente;
    }

    // ASSISTENTE
    private long proximo()
    {
      fValor = (fValor * 0x10DCD + 1) & 0xFFFFFFFFL;
      return fValor;
    }

    // INTERFACE
    public long semente()
    {
      return fSemente;
    }

    public int uniforme(int limite)
    {
      if (limite <= 0)
        throw Excecoes.argumentoNegativo("limite");

      return (int)(proximo() % (long)limite);
    }

    public double uniforme()
    {
      return proximo() * (1.0 / 0x100000000L); // 2^-31
    }
  }
}