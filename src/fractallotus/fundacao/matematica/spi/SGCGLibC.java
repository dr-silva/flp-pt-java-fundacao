/*******************************************************************************
 *
 * Arquivo  : SGCGLibC.java
 * Autor    : Daniel Raimundo da Silva
 * Data     : 2021-08-28 <yyyy-mm-dd>
 * Licença  : Este arquivo está sob licença Non-Comercial Creative Commons
 * Descrição: Define o gerador congruencial linear com as especificações do
 *            GNU C Library, assim como o serviço responsável pela sua instanci-
 *            ação.
 *
 *******************************************************************************/
package fractallotus.fundacao.matematica.spi;

import fractallotus.fundacao.matematica.IGeradorCongruencial;
import fractallotus.fundacao.matematica.TipoGeradorCongruencial;
import fractallotus.fundacao.recursos.Excecoes;

public class SGCGLibC implements IServicoGeradorCongruencial
{
  public boolean validar(TipoGeradorCongruencial tipo)
  {
    return tipo == TipoGeradorCongruencial.Linear;
  }

  public boolean validar(String nome, TipoGeradorCongruencial tipo)
  {
    return nome.equals("glibc") && validar(tipo);
  }

  public IGeradorCongruencial criar(long semente)
  {
    return new GCLGLibC(semente);
  }

  // SUBCLASSE - GERADOR CONGRUENCIAL //
  private class GCLGLibC implements IGeradorCongruencial
  {
    // ATRIBUTOS
    private int  fValor;
    private long fSemente;

    // CONSTRUTOR
    public GCLGLibC(long semente)
    {
      fValor   = (int)semente;
      fSemente = semente;
    }

    // ASSISTENTE
    private int proximo()
    {
      fValor = (fValor * 0x41C64E6D + 0x3039) & 0x7FFFFFFF;
      return fValor;
    }

    // INTERFACE
    public long semente()
    {
      return fSemente;
    }

    public int uniforme(int limite)
    {
      if (limite <= 0)
        throw Excecoes.argumentoNegativo("limite");

      return proximo() % limite;
    }

    public double uniforme()
    {
      return proximo() * (1.0 / 0x80000000L); // 2^-31
    }
  }
}