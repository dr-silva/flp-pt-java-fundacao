/*******************************************************************************
 *
 * Arquivo  : SGCCpp11.java
 * Autor    : Daniel Raimundo da Silva
 * Data     : 2021-08-28 <yyyy-mm-dd>
 * Licença  : Este arquivo está sob licença Non-Comercial Creative Commons
 * Descrição: Define o gerador congruencial linear com as especificações do
 *            C++ aprovadas pela International Organization for Standardization
 *            em 12 de Agosto de 2011. Assim como o serviço responsável pela sua
 *            instanciação.
 *
 *******************************************************************************/
package fractallotus.fundacao.matematica.spi;

import fractallotus.fundacao.matematica.IGeradorCongruencial;
import fractallotus.fundacao.matematica.TipoGeradorCongruencial;
import fractallotus.fundacao.recursos.Excecoes;

public class SGCCpp11 implements IServicoGeradorCongruencial
{
  public boolean validar(TipoGeradorCongruencial tipo)
  {
    return tipo == TipoGeradorCongruencial.Linear;
  }

  public boolean validar(String nome, TipoGeradorCongruencial tipo)
  {
    return nome.equals("cpp11") && validar(tipo);
  }

  public IGeradorCongruencial criar(long semente)
  {
    return new GCLCpp11(semente);
  }

  // SUBCLASSE - GERADOR CONGRUENCIAL //
  private class GCLCpp11 implements IGeradorCongruencial
  {
    // ATRIBUTOS
    private int  fValor;
    private long fSemente;

    // CONSTRUTOR
    public GCLCpp11(long semente)
    {
      fValor   = (int)semente;
      fSemente = semente;
    }

    // ASSISTENTE
    private int proximo()
    {
      fValor = (fValor * 0xBC8F) & 0x7FFFFFFF;
      return fValor;
    }

    // INTERFACE
    public long semente()
    {
      return fSemente;
    }

    public int uniforme(int limite)
    {
      if (limite <= 0)
        throw Excecoes.argumentoNegativo("limite");

      return proximo() % limite;
    }

    public double uniforme()
    {
      return proximo() * (1.0 / 0x80000000L); // 2^-31
    }
  }
}