/*******************************************************************************
 *
 * Arquivo  : ComparadorDouble.java
 * Autor    : Daniel Raimundo da Silva
 * Data     : 2021-07-09 <yyyy-mm-dd>
 * Licença  : Este arquivo está sob licença Non-Comercial Creative Commons
 * Descrição: Define uma implementação do comparador para ser usada em contextos
 *            nos quais:
 *              - x < y se, e somente se,  x - y <= -epsilon;
 *              - x > y se, e somente se,  x - y >= +epsilon;
 *              - x = y se, e somente se, |x - y| < +epsilon;
 *
 *******************************************************************************/
package fractallotus.fundacao.matematica;

import fractallotus.fundacao.cerne.IComparador;

class ComparadorDouble implements IComparador<Double>
{
  private double epsilon;

  public ComparadorDouble(double epsilon)
  {
    this.epsilon = epsilon;
  }

  public int compare(Double esquerda, Double direita)
  {
    int    result;
    double valor = esquerda - direita;

         if (valor <= -epsilon) result = -1;
    else if (valor >= +epsilon) result = +1;
    else                        result =  0;

    return result;
  }
}
