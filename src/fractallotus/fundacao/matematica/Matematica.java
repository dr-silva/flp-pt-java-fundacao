/*******************************************************************************
 *
 * Arquivo  : Matematica.java
 * Autor    : Daniel Raimundo da Silva
 * Data     : 2021-07-09 <yyyy-mm-dd>
 * Licença  : Este arquivo está sob licença Non-Comercial Creative Commons
 * Descrição: Define uma classe de utilidades para operar alguns objetos defini-
 *            dos neste pacote.
 *
 *******************************************************************************/
package fractallotus.fundacao.matematica;

import fractallotus.fundacao.cerne.IComparador;
import fractallotus.fundacao.cerne.IComparadorIgualdade;
import fractallotus.fundacao.recursos.Excecoes;

public class Matematica
{
  // CONSTANTES //
  public static final double EPSILON = 1E-7;

  // ATRIBUTOS //
  private static long fSemente = System.currentTimeMillis(),
                      fValor   = fSemente;

  // PROPRIEDADES //
  public static long semente()
  {
    return fSemente;
  }

  // CONSTRUTOR //
  private Matematica()
  {
    throw new UnsupportedOperationException();
  }

  // ASSISTENTES //
  private static synchronized long proximo()
  {
    fValor = (fValor * 0x19660D + 0x3C6EF35F) & 0xFFFFFFFFL; // mod 2^32
    return fValor;
  }

  // INTERFACE - GERADOR CONGRUENCIAL //
  public static int uniforme(int limite) throws IllegalArgumentException
  {
    if (limite <= 0)
      throw Excecoes.argumentoNegativo("limite");

    return (int)(proximo() % (long)limite);
  }

  public static double uniforme()
  {
    return proximo() * (1.0 / 0x100000000L);
  }

  // INTERFACE - COMPARADOR IGUALDADE //
  public static IComparadorIgualdade<Float> criarComparadorIgualdadeFloat()
  {
    return criarComparadorIgualdadeFloat((float)EPSILON);
  }

  public static IComparadorIgualdade<Float> criarComparadorIgualdadeFloat(float epsilon)
  {
    if (epsilon <= 0)
      epsilon = (float)EPSILON;

    return new ComparadorIgualdadeFloat(epsilon);
  }

  public static IComparadorIgualdade<Double> criarComparadorIgualdadeDouble()
  {
    return criarComparadorIgualdadeDouble(EPSILON);
  }

  public static IComparadorIgualdade<Double> criarComparadorIgualdadeDouble(double epsilon)
  {
    if (epsilon <= 0)
      epsilon = EPSILON;

    return new ComparadorIgualdadeDouble(epsilon);
  }

  // INTERFACE - COMPARADOR //
  public static IComparador<Float> criarComparadorFloat()
  {
    return criarComparadorFloat((float)EPSILON);
  }

  public static IComparador<Float> criarComparadorFloat(float epsilon)
  {
    if (epsilon <= 0)
      epsilon = (float)EPSILON;

    return new ComparadorFloat(epsilon);
  }

  public static IComparador<Double> criarComparadorDouble()
  {
    return criarComparadorDouble(EPSILON);
  }

  public static IComparador<Double> criarComparadorDouble(double epsilon)
  {
    if (epsilon <= 0)
      epsilon = EPSILON;

    return new ComparadorDouble(epsilon);
  }
}
