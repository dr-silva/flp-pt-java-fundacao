/*******************************************************************************
 *
 * Arquivo  : OrdenacaoHeap.java
 * Autor    : Daniel Raimundo da Silva
 * Data     : 2020-08-17 <yyyy-mm-dd>
 * Licença  : Este arquivo está sob licença Non-Comercial Creative Commons
 * Descrição: Classe de ordenação que implementa o método heap.
 *
 *******************************************************************************/
package fractallotus.fundacao.ordenacoes;

import fractallotus.fundacao.cerne.IComparador;

public abstract class OrdenacaoHeap<T> extends OrdenacaoAbstrata<T>
{
  // CONSTRUTORES //
  public OrdenacaoHeap(IComparador<T> comparador)
  {
    super(comparador);
  }

  // ASSISTENTES //
  private void amontoar(int i)
  {
    int esquerda = 2 * (i + 1) - 1,
        direita  = 2 * (i + 1);
    int prioritario;

    if ((esquerda < fTotal) && eOrdenavel(esquerda, i))
      prioritario = esquerda;
    else
      prioritario = i;

    if ((direita < fTotal) && eOrdenavel(direita, prioritario))
      prioritario = direita;

    if (prioritario != i)
    {
      trocar(i, prioritario);
      amontoar(prioritario);
    }
  }

  protected abstract boolean eOrdenavel(int i, int j);

  // ORDENAÇÃO //
  protected void fazerOrdenacao()
  {
    // criar heap
    for (int i = fTotal / 2; i >= 0; i--)
      amontoar(i);

    // ordenar array
    for (int i = fTotal - 1; i > 0; i--)
    {
      trocar(0, i);
      fTotal--;
      amontoar(0);
    }
  }
}
