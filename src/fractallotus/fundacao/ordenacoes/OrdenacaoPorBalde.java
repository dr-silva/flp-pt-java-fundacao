/*******************************************************************************
 *
 * Arquivo  : OrdenacaoPorBalde.java
 * Autor    : Daniel Raimundo da Silva
 * Data     : 2020-08-17 <yyyy-mm-dd>
 * Licença  : Este arquivo está sob licença Non-Comercial Creative Commons
 * Descrição: Classe de ordenação que implementa o método por balde.
 *
 *******************************************************************************/
package fractallotus.fundacao.ordenacoes;

import fractallotus.fundacao.cerne.IOrdenacao;
import fractallotus.fundacao.cerne.IProjetor;
import fractallotus.fundacao.recursos.Excecoes;

public abstract class OrdenacaoPorBalde<T> implements IOrdenacao<T>
{
  private class Item
  {
    public T      valor;
    public double projecao;
    public Item   ulterior,
                  anterior;
  }

  // ATRIBUTOS //
  private int      fTotal;
  private T[]      fItens;
  private Object[] fBaldes;

  private IProjetor<T, Double> fProjetor;

  // CONSTRUTORES //
  public OrdenacaoPorBalde(IProjetor<T, Double> projetor)
  {
    if (projetor == null)
      throw Excecoes.argumentoNulo("projetor");

    fProjetor = projetor;
  }

  // ASSISTENTES //
  private void criarItem(T valor)
  {
    Item item = new Item();

    item.valor    = valor;
    item.projecao = fProjetor.projetar(valor);
    item.ulterior = null;
    item.anterior = null;

    int  indice = (int)(fTotal * item.projecao);
    @SuppressWarnings("unchecked")
    Item lista  = (Item)fBaldes[indice];

    item.ulterior = lista;

    if (lista != null)
      lista.anterior = item;

    fBaldes[indice] = item;
  }

  private void ordenar(int indice)
  {
    @SuppressWarnings("unchecked")
    Item primeiro = (Item)fBaldes[indice];

    if (primeiro == null)
      return;

    Item fixo = primeiro.ulterior;
    while (fixo != null)
    {
      Item movel = fixo.anterior;

      while ((movel != null) && (movel.projecao > movel.ulterior.projecao))
      {
        movel = trocar(movel, movel.ulterior);
        movel = movel.anterior;
      }

      fixo = fixo.ulterior;
    }

    while (primeiro.anterior != null)
      primeiro = primeiro.anterior;

    fBaldes[indice] = primeiro;
  }

  private int transferir(int indice, int balde)
  {
    @SuppressWarnings("unchecked")
    Item item = (Item)fBaldes[balde];

    while (item != null)
    {
      fItens[indexar(fTotal, indice++)] = item.valor;

      item = item.ulterior;
    }

    return indice;
  }

  private Item trocar(Item esquerda, Item direita)
  {
    Item extremaEsquerda = esquerda.anterior,
         extremaDireita  = direita .ulterior;

    if (extremaEsquerda != null)
      extremaEsquerda.ulterior = direita;
    if (extremaDireita != null)
      extremaDireita.anterior = esquerda;

    direita .ulterior = esquerda;
    esquerda.ulterior = extremaDireita;
    esquerda.anterior = direita;
    direita .anterior = extremaEsquerda;

    return direita;
  }

  protected abstract int indexar(int total, int indice);

  // ORDENAÇÃO //
  public final void ordenar(T[] itens)
  {
    if ((itens == null) || (itens.length == 0))
      return;

    fItens  = itens;
    fTotal  = itens.length;
    fBaldes = new Object[fTotal];

    for (int i = 0; i < fTotal; i++)
      criarItem(fItens[i]);

    for (int i = 0; i < fTotal; i++)
      ordenar(i);

    int indice = 0;
    for (int i = 0; i < fTotal; i++)
      indice = transferir(indice, i);
  }
}