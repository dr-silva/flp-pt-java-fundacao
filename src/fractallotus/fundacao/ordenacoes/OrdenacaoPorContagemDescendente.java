/*******************************************************************************
 *
 * Arquivo  : OrdenacaoPorContagemDescendente.java
 * Autor    : Daniel Raimundo da Silva
 * Data     : 2020-08-17 <yyyy-mm-dd>
 * Licença  : Este arquivo está sob licença Non-Comercial Creative Commons
 * Descrição: Classe de ordenação que implementa o método por contagem decres-
 *            cente.
 *
 *******************************************************************************/
package fractallotus.fundacao.ordenacoes;

import java.lang.Class;
import fractallotus.fundacao.cerne.IOrdenacao;
import fractallotus.fundacao.cerne.IProjetor;

public class OrdenacaoPorContagemDescendente<T> extends OrdenacaoPorContagem<T>
{
  // CONSTRUTORES //
  public OrdenacaoPorContagemDescendente(IProjetor<T, Integer> projetor)
  {
    super(projetor);
  }

  // ASSISTENTES //
  protected int indexarSaida(int i)
  {
    return total() - (i + 1);
  }
}