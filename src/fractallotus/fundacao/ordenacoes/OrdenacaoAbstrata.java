/*******************************************************************************
 *
 * Arquivo  : OrdenacaoAbstrata.java
 * Autor    : Daniel Raimundo da Silva
 * Data     : 2020-08-17 <yyyy-mm-dd>
 * Licença  : Este arquivo está sob licença Non-Comercial Creative Commons
 * Descrição: Define uma classe base para as classes de ordenações.
 *
 *******************************************************************************/
package fractallotus.fundacao.ordenacoes;

import fractallotus.fundacao.cerne.IOrdenacao;
import fractallotus.fundacao.cerne.IComparador;
import fractallotus.fundacao.recursos.Excecoes;

public abstract class OrdenacaoAbstrata<T> implements IOrdenacao<T>
{
  // ATRIBUTOS //
  private   T[]            fItens;
  protected int            fTotal;
  protected IComparador<T> fComparador;

  // PROPRIEDADES //
  protected T item(int indice)
  {
    return fItens[indice];
  }

  // MÉTODOS //
  public OrdenacaoAbstrata(IComparador<T> comparador)
  {
    if (comparador == null)
      throw Excecoes.argumentoNulo("comparador");

    fComparador = comparador;
  }

  protected void trocar(int i, int j)
  {
    if (i != j)
    {
      T temp    = fItens[i];
      fItens[i] = fItens[j];
      fItens[j] = temp;
    }
  }

  protected abstract void fazerOrdenacao();

  public final void ordenar(T[] itens)
  {
    if ((itens == null) || (itens.length == 0))
      return;

    fItens = itens;
    fTotal = itens.length;

    fazerOrdenacao();
  }
}