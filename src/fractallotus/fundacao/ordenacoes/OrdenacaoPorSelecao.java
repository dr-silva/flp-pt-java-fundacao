/*******************************************************************************
 *
 * Arquivo  : OrdenacaoPorSelecao.java
 * Autor    : Daniel Raimundo da Silva
 * Data     : 2020-08-17 <yyyy-mm-dd>
 * Licença  : Este arquivo está sob licença Non-Comercial Creative Commons
 * Descrição: Classe de ordenação que implementa o método por selecao.
 *
 *******************************************************************************/
package fractallotus.fundacao.ordenacoes;

import fractallotus.fundacao.cerne.IComparador;

public abstract class OrdenacaoPorSelecao<T> extends OrdenacaoAbstrata<T>
{
  // CONSTRUTORES //
  public OrdenacaoPorSelecao(IComparador<T> comparador)
  {
    super(comparador);
  }

  // ASSISTENTES //
  protected abstract boolean eOrdenavel(int i, int j);

  // ORDENAÇÃO //
  protected void fazerOrdenacao()
  {
    for (int i = 0; i < fTotal; i++)
    {
      int minimo = i;
      for (int j = i + 1; j < fTotal; j++)
        if (eOrdenavel(minimo, j))
          minimo = j;

      trocar(i, minimo);
    }
  }
}
