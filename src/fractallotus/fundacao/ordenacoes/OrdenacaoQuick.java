/*******************************************************************************
 *
 * Arquivo  : OrdenacaoQuick.java
 * Autor    : Daniel Raimundo da Silva
 * Data     : 2020-08-17 <yyyy-mm-dd>
 * Licença  : Este arquivo está sob licença Non-Comercial Creative Commons
 * Descrição: Classe de ordenação que implementa o método quick.
 *
 *******************************************************************************/
package fractallotus.fundacao.ordenacoes;

import fractallotus.fundacao.cerne.IComparador;

public abstract class OrdenacaoQuick<T> extends OrdenacaoAbstrata<T>
{
  // CONSTRUTORES //
  public OrdenacaoQuick(IComparador<T> comparador)
  {
    super(comparador);
  }

  // ASSISTENTES //
  private int particionar(int minimo, int maximo)
  {
    int i = minimo + (int)(Math.random() * (maximo - minimo));

    trocar(i, maximo);

    i = minimo;

    for (int j = minimo; j < maximo; j++)
      if (eOrdenavel(j, maximo))
        trocar(i++, j);

    trocar(i, maximo);

    return i;
  }

  private void ordenar(int minimo, int maximo)
  {
    if (minimo < maximo)
    {
      int ponto = particionar(minimo, maximo);

      ordenar(minimo, ponto - 1);
      ordenar(ponto + 1, maximo);
    }
  }

  protected abstract boolean eOrdenavel(int i, int j);

  // ORDENAÇÃO //
  protected void fazerOrdenacao()
  {
    ordenar(0, fTotal - 1);
  }
}
