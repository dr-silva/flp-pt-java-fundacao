/*******************************************************************************
 *
 * Arquivo  : OrdenacaoQuick3Way.java
 * Autor    : Daniel Raimundo da Silva
 * Data     : 2020-08-17 <yyyy-mm-dd>
 * Licença  : Este arquivo está sob licença Non-Comercial Creative Commons
 * Descrição: Classe de ordenação que implementa o método quick 3-way.
 *
 *******************************************************************************/
package fractallotus.fundacao.ordenacoes;

import fractallotus.fundacao.cerne.IComparador;

public abstract class OrdenacaoQuick3Way<T> extends OrdenacaoAbstrata<T>
{
  // CONSTRUTORES //
  public OrdenacaoQuick3Way(IComparador<T> comparador)
  {
    super(comparador);
  }

  // ASSISTENTES //
  private void ordenar(int minimo, int maximo)
  {
    if (minimo >= maximo)
      return;

    int menor = minimo,
        maior = maximo,
        i     = minimo + 1;
    T   valor = item(minimo);

    while (i <= maior)
    {
      int comparacao = comparar(item(i), valor);

      if (comparacao < 0)
        trocar(menor++, i++);
      else if (comparacao > 0)
        trocar(i, maior--);
      else
        i++;
    }

    ordenar(minimo, menor - 1);
    ordenar(maior + 1, maximo);
  }

  protected abstract int comparar(T esquerda, T direita);

  // ORDENAÇÃO //
  protected void fazerOrdenacao()
  {
    ordenar(0, fTotal - 1);
  }
}
