/*******************************************************************************
 *
 * Arquivo  : OrdenacaoShell.java
 * Autor    : Daniel Raimundo da Silva
 * Data     : 2020-08-17 <yyyy-mm-dd>
 * Licença  : Este arquivo está sob licença Non-Comercial Creative Commons
 * Descrição: Classe de ordenação que implementa o método de Shell.
 *
 *******************************************************************************/
package fractallotus.fundacao.ordenacoes;

import fractallotus.fundacao.cerne.IComparador;

public abstract class OrdenacaoShell<T> extends OrdenacaoAbstrata<T>
{
  // CONSTRUTORES //
  public OrdenacaoShell(IComparador<T> comparador)
  {
    super(comparador);
  }

  // ASSISTENTES //
  protected abstract boolean eOrdenavel(int i, int j);

  // ORDENAÇÃO //
  protected void fazerOrdenacao()
  {
    int lacuna = 1;
    while (lacuna < fTotal / 3)
      lacuna = 3 * lacuna + 1;

    while (lacuna >= 1)
    {
      for (int i = lacuna; i < fTotal; i++)
      {
        int j = i;

        while ((j >= lacuna) && eOrdenavel(j - lacuna, j))
        {
          trocar(j - lacuna, j);
          j -= lacuna;
        }
      }

      lacuna /= 3;
    }
  }
}
