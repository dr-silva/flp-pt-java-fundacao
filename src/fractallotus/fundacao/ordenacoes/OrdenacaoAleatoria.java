/*******************************************************************************
 *
 * Arquivo  : OrdenacaoAleatoria.java
 * Autor    : Daniel Raimundo da Silva
 * Data     : 2020-08-18 <yyyy-mm-dd>
 * Licença  : Este arquivo está sob licença Non-Comercial Creative Commons
 * Descrição: Classe de ordenação que implementa o método para embaralhar os
 *            valores do array recebido.
 *
 *******************************************************************************/
package fractallotus.fundacao.ordenacoes;

import fractallotus.fundacao.cerne.IProjetor;
import fractallotus.fundacao.matematica.IGeradorCongruencial;
import fractallotus.fundacao.matematica.TipoGeradorCongruencial;
import fractallotus.fundacao.matematica.FabricaGeradorCongruencial;

public class OrdenacaoAleatoria<T> extends OrdenacaoPorBaldeAscendente<T>
{
  // CONSTRUTORES //
  public OrdenacaoAleatoria()
  {
    super
    (
      new IProjetor<T, Double>()
      {
        private final IGeradorCongruencial gerador = FabricaGeradorCongruencial.criar
        (
          TipoGeradorCongruencial.Linear
        );

        public Double projetar(T valor)
        {
          return new Double(gerador.uniforme());
        }
      }
    );
  }
}