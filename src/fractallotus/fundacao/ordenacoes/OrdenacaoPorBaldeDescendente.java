/*******************************************************************************
 *
 * Arquivo  : OrdenacaoPorBaldeDescendente.java
 * Autor    : Daniel Raimundo da Silva
 * Data     : 2020-08-17 <yyyy-mm-dd>
 * Licença  : Este arquivo está sob licença Non-Comercial Creative Commons
 * Descrição: Classe de ordenação que implementa o método por balde crescen-
 *            temente.
 *
 *******************************************************************************/
package fractallotus.fundacao.ordenacoes;

import fractallotus.fundacao.cerne.IProjetor;

public class OrdenacaoPorBaldeDescendente<T> extends OrdenacaoPorBalde<T>
{
  // CONSTRUTORES //
  public OrdenacaoPorBaldeDescendente(IProjetor<T, Double> projetor)
  {
    super(projetor);
  }

  // ASSISTENTES //
  protected int indexar(int total, int indice)
  {
    return total - (indice + 1);
  }
}