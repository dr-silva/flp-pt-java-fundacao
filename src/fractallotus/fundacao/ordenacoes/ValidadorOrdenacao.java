/*******************************************************************************
 *
 * Arquivo  : ValidadorOrdenacao.java
 * Autor    : Daniel Raimundo da Silva
 * Data     : 2020-08-18 <yyyy-mm-dd>
 * Licença  : Este arquivo está sob licença Non-Comercial Creative Commons
 * Descrição: Classe que implementa uma validação a um array para averiguar se
 *            ele está, de fato, ordenado.
 *
 *******************************************************************************/
package fractallotus.fundacao.ordenacoes;

import fractallotus.fundacao.cerne.IComparador;
import fractallotus.fundacao.cerne.IValidadorOrdenacao;
import fractallotus.fundacao.recursos.Excecoes;

public abstract class ValidadorOrdenacao<T> implements IValidadorOrdenacao<T>
{
  // ATRIBUTOS //
  protected IComparador<T> fComparador;

  // CONSTRUTORES //
  public ValidadorOrdenacao(IComparador<T> comparador)
  {
    if (comparador == null)
      throw Excecoes.argumentoNulo("comparador");

    fComparador = comparador;
  }

  // ASSISTENTES //
  protected abstract boolean estaOrdenado(T esquerda, T direita);

  // VALIDAÇÃO //
  public final boolean validar(T[] itens)
  {
    if ((itens == null) || (itens.length == 0))
      return true;

    int total = itens.length,
        i     = 1;

    while ((i < total) && estaOrdenado(itens[i - 1], itens[i]))
      i++;

    return i == total;
  }
}
