/*******************************************************************************
 *
 * Arquivo  : OrdenacaoPorInsercao.java
 * Autor    : Daniel Raimundo da Silva
 * Data     : 2020-08-17 <yyyy-mm-dd>
 * Licença  : Este arquivo está sob licença Non-Comercial Creative Commons
 * Descrição: Classe de ordenação que implementa o método por inserção.
 *
 *******************************************************************************/
package fractallotus.fundacao.ordenacoes;

import fractallotus.fundacao.cerne.IComparador;

public abstract class OrdenacaoPorInsercao<T> extends OrdenacaoAbstrata<T>
{
  // CONSTRUTORES //
  public OrdenacaoPorInsercao(IComparador<T> comparador)
  {
    super(comparador);
  }

  // ASSISTENTES //
  protected abstract boolean eOrdenavel(int i, int j);

  // ORDENAÇÃO //
  protected void fazerOrdenacao()
  {
    for (int j = 1; j < fTotal; j++)
    {
      int i = j - 1;
      while ((i >= 0) && eOrdenavel(i, i + 1))
      {
        trocar(i, i + 1);
        i--;
      }
    }
  }
}
