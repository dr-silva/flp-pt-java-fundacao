/*******************************************************************************
 *
 * Arquivo  : OrdenacaoShellDescendente.java
 * Autor    : Daniel Raimundo da Silva
 * Data     : 2020-08-17 <yyyy-mm-dd>
 * Licença  : Este arquivo está sob licença Non-Comercial Creative Commons
 * Descrição: Classe de ordenação que implementa o método de Shell decrescente-
 *            mente.
 *
 *******************************************************************************/
package fractallotus.fundacao.ordenacoes;

import fractallotus.fundacao.cerne.IComparador;

public class OrdenacaoShellDescendente<T> extends OrdenacaoShell<T>
{
  // CONSTRUTORES //
  public OrdenacaoShellDescendente(IComparador<T> comparador)
  {
    super(comparador);
  }

  // ASSISTENTES //
  protected boolean eOrdenavel(int i, int j)
  {
    return (fComparador.compare(item(i), item(j)) < 0);
  }
}
