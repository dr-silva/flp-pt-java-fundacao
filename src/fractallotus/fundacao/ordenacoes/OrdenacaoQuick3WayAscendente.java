/*******************************************************************************
 *
 * Arquivo  : OrdenacaoQuick3WayAscendente.java
 * Autor    : Daniel Raimundo da Silva
 * Data     : 2020-08-17 <yyyy-mm-dd>
 * Licença  : Este arquivo está sob licença Non-Comercial Creative Commons
 * Descrição: Classe de ordenação que implementa o método quick 3-way crescente.
 *
 *******************************************************************************/
package fractallotus.fundacao.ordenacoes;

import fractallotus.fundacao.cerne.IComparador;

public class OrdenacaoQuick3WayAscendente<T> extends OrdenacaoQuick3Way<T>
{
  // CONSTRUTORES //
  public OrdenacaoQuick3WayAscendente(IComparador<T> comparador)
  {
    super(comparador);
  }

  // ASSISTENTES //
  protected int comparar(T esquerda, T direita)
  {
    return fComparador.compare(esquerda, direita);
  }
}
