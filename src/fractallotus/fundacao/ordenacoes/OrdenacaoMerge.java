/*******************************************************************************
 *
 * Arquivo  : OrdenacaoMerge.java
 * Autor    : Daniel Raimundo da Silva
 * Data     : 2020-08-17 <yyyy-mm-dd>
 * Licença  : Este arquivo está sob licença Non-Comercial Creative Commons
 * Descrição: Classe de ordenação que implementa o método merge.
 *
 *******************************************************************************/
package fractallotus.fundacao.ordenacoes;

import fractallotus.fundacao.cerne.IOrdenacao;
import fractallotus.fundacao.cerne.IComparador;
import fractallotus.fundacao.recursos.Excecoes;

public abstract class OrdenacaoMerge<T> implements IOrdenacao<T>
{
  // ATRIBUTOS //
  private Object[] fItens,
                   fEsquerda,
                   fDireita;

  protected IComparador<T> fComparador;

  // CONSTRUTORES //
  public OrdenacaoMerge(IComparador<T> comparador)
  {
    if (comparador == null)
      throw Excecoes.argumentoNulo("comparador");

    fComparador = comparador;
  }

  // ASSISTENTES //
  private T obterValor(Object[] vetor, int indice)
  {
    @SuppressWarnings("unchecked")
    T valor = (T)vetor[indice];

    return valor;
  }

  private int criarEsquerda(int minimo, int maximo)
  {
    int total = maximo - minimo + 1;
    fEsquerda = new Object[total];

    for (int i = 0; i < total; i++)
      fEsquerda[i] = fItens[minimo + i];

    return total;
  }

  private int criarDireita(int minimo, int maximo)
  {
    int total = maximo - minimo;
    fDireita  = new Object[total];

    for (int i = 0; i < total; i++)
      fDireita[i] = fItens[minimo + i + 1];

    return total;
  }

  private int definirEsquerda(int indice, int esquerda)
  {
    fItens[indice] = fEsquerda[esquerda];

    return esquerda + 1;
  }

  private int definirDireita(int indice, int direita)
  {
    fItens[indice] = fDireita[direita];

    return direita + 1;
  }

  private void fundir(int minimo, int mediana, int maximo)
  {
    int totalEsquerda = criarEsquerda(minimo, mediana),
        totalDireita  = criarDireita (mediana, maximo);

    int esquerda = 0,
        direita  = 0;

    for (int i = minimo; i <= maximo; i++)
      if (esquerda >= totalEsquerda)
        direita = definirDireita(i, direita);
      else if (direita >= totalDireita)
        esquerda = definirEsquerda(i, esquerda);
      // else if (eOrdenavel((T)fEsquerda[esquerda], (T)fDireita[direita]))
      else if (eOrdenavel(esquerda, direita))
        esquerda = definirEsquerda(i, esquerda);
      else
        direita = definirDireita(i, direita);
  }

  private void ordenar(int minimo, int maximo)
  {
    if (minimo < maximo)
    {
      int mediana = (minimo + maximo) / 2;

      ordenar(minimo, mediana);
      ordenar(mediana + 1, maximo);

      fundir(minimo, mediana, maximo);
    }
  }

  private boolean eOrdenavel(int esquerda, int direita)
  {
    T objE = obterValor(fEsquerda, esquerda);
    T objD = obterValor(fDireita,  direita);

    return eOrdenavel(objE, objD);
  }

  protected abstract boolean eOrdenavel(T esquerda, T direita);

  // ORDENAÇÃO //
  public final void ordenar(T[] itens)
  {
    if ((itens == null) || (itens.length == 0))
      return;

    fItens = itens;

    ordenar(0, itens.length - 1);
  }
}