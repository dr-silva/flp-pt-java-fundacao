/*******************************************************************************
 *
 * Arquivo  : OrdenacaoPorContagem.java
 * Autor    : Daniel Raimundo da Silva
 * Data     : 2020-08-17 <yyyy-mm-dd>
 * Licença  : Este arquivo está sob licença Non-Comercial Creative Commons
 * Descrição: Classe de ordenação que implementa o método por contagem.
 *
 *******************************************************************************/
package fractallotus.fundacao.ordenacoes;

import fractallotus.fundacao.cerne.IOrdenacao;
import fractallotus.fundacao.cerne.IProjetor;
import fractallotus.fundacao.recursos.Excecoes;

public abstract class OrdenacaoPorContagem<T> implements IOrdenacao<T>
{
  // ATRIBUTOS //
  private int fTotal,
              fMinimo,
              fMaximo;

  private Object[] fItens;
  private IProjetor<T, Integer> fProjetor;

  // PROPRIEDADES //
  protected int total () { return fTotal;  }
  public    int minimo() { return fMinimo; }
  public    int maximo() { return fMaximo; }

  public    void minimo(int valor) { fMinimo = valor; }
  public    void maximo(int valor) { fMaximo = valor; }

  // CONSTRUTORES //
  public OrdenacaoPorContagem(IProjetor<T, Integer> projetor)
  {
    if (projetor == null)
      throw Excecoes.argumentoNulo("projetor");

    fProjetor = projetor;
    fMinimo   = Integer.MAX_VALUE;
    fMaximo   = Integer.MIN_VALUE;
  }

  // ASSISTENTES //
  private Integer projetar(int indice)
  {
    @SuppressWarnings("unchecked")
    T valor = (T)fItens[indice];

    return fProjetor.projetar(valor);
  }

  private int obterMinimo()
  {
    int minimo = Integer.MAX_VALUE,
        projecao;

    for (int i = 0; i < fTotal; i++)
    {
      projecao = projetar(i);

      if (projecao < minimo)
        minimo = projecao;
    }

    return minimo;
  }

  private int obterMaximo()
  {
    int maximo = Integer.MIN_VALUE,
        projecao;

    for (int i = 0; i < fTotal; i++)
    {
      projecao = projetar(i);

      if (projecao > maximo)
        maximo = projecao;
    }

    return maximo;
  }

  private boolean preparar()
  {
    if (fMinimo >= fMaximo)
    {
      fMinimo = obterMinimo();
      fMaximo = obterMaximo();
    }

    return (fMaximo - fMinimo > 1);
  }

  private int indexar(int i)
  {
    return projetar(i) - fMinimo;
  }

  protected abstract int indexarSaida(int i);

  // ORDENAÇÃO //
  public final void ordenar(T[] itens)
  {
    if ((itens == null) || (itens.length == 0))
      return;

    fItens = itens;
    fTotal = itens.length;

    if (!preparar())
      return;

    int      diferenca = fMaximo - fMinimo;
    int[]    contagem  = new int[diferenca + 1];
    Object[] saida     = new Object[fTotal];

    for (int i = 0; i <= diferenca; i++)
      contagem[i] = 0;

    for (int i = 0; i < fTotal; i++)
    {
      int indice = indexar(i);

      contagem[indice] = contagem[indice] + 1;
    }

    for (int i = 1; i <= diferenca; i++)
      contagem[i] += contagem[i - 1];

    for (int i = fTotal - 1; i >= 0; i--)
    {
      int indice = indexar(i);

      saida[ contagem[indice] - 1] = fItens[i];

      contagem[indice] = contagem[indice] - 1;
    }

    for (int i = 0; i < fTotal; i++)
      fItens[i] = saida[ indexarSaida(i) ];
  }
}