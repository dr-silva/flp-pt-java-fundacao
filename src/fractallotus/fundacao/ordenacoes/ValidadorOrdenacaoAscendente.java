/*******************************************************************************
 *
 * Arquivo  : ValidadorOrdenacaoAscendente.java
 * Autor    : Daniel Raimundo da Silva
 * Data     : 2020-08-18 <yyyy-mm-dd>
 * Licença  : Este arquivo está sob licença Non-Comercial Creative Commons
 * Descrição: Classe que implementa uma validação a um array para averiguar se
 *            ele está, de fato, ordenado crescentemente.
 *
 *******************************************************************************/
package fractallotus.fundacao.ordenacoes;

import fractallotus.fundacao.cerne.IComparador;

public class ValidadorOrdenacaoAscendente<T> extends ValidadorOrdenacao<T>
{
  // CONSTRUTORES //
  public ValidadorOrdenacaoAscendente(IComparador<T> comparador)
  {
    super(comparador);
  }

  // ASSISTENTES //
  protected boolean estaOrdenado(T esquerda, T direita)
  {
    return (fComparador.compare(esquerda, direita) <= 0);
  }
}
