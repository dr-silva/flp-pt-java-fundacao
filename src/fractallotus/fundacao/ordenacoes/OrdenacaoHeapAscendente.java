/*******************************************************************************
 *
 * Arquivo  : OrdenacaoHeapAscendente.java
 * Autor    : Daniel Raimundo da Silva
 * Data     : 2020-08-17 <yyyy-mm-dd>
 * Licença  : Este arquivo está sob licença Non-Comercial Creative Commons
 * Descrição: Classe de ordenação que implementa o método heap crescentemente.
 *
 *******************************************************************************/
package fractallotus.fundacao.ordenacoes;

import fractallotus.fundacao.cerne.IComparador;

public class OrdenacaoHeapAscendente<T> extends OrdenacaoHeap<T>
{
  // CONSTRUTORES //
  public OrdenacaoHeapAscendente(IComparador<T> comparador)
  {
    super(comparador);
  }

  // ASSISTENTES //
  protected boolean eOrdenavel(int i, int j)
  {
    return (fComparador.compare(item(i), item(j)) > 0);
  }
}
