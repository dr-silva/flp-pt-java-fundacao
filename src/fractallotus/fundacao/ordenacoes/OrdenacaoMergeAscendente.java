/*******************************************************************************
 *
 * Arquivo  : OrdenacaoMergeAscendente.java
 * Autor    : Daniel Raimundo da Silva
 * Data     : 2020-08-17 <yyyy-mm-dd>
 * Licença  : Este arquivo está sob licença Non-Comercial Creative Commons
 * Descrição: Classe de ordenação que implementa o método merge crescentemente.
 *
 *******************************************************************************/
package fractallotus.fundacao.ordenacoes;

import java.lang.Class;
import fractallotus.fundacao.cerne.IOrdenacao;
import fractallotus.fundacao.cerne.IComparador;

public class OrdenacaoMergeAscendente<T> extends OrdenacaoMerge<T>
{
  // CONSTRUTORES //
  public OrdenacaoMergeAscendente(IComparador<T> comparador)
  {
    super(comparador);
  }

  // ASSISTENTES //
  protected boolean eOrdenavel(T esquerda, T direita)
  {
    return (fComparador.compare(esquerda, direita) <= 0);
  }
}