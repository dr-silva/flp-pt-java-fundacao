/*******************************************************************************
 *
 * Arquivo  : OrdenacaoPorBaldeAscendente.java
 * Autor    : Daniel Raimundo da Silva
 * Data     : 2020-08-17 <yyyy-mm-dd>
 * Licença  : Este arquivo está sob licença Non-Comercial Creative Commons
 * Descrição: Classe de ordenação que implementa o método por balde crescen-
 *            temente.
 *
 *******************************************************************************/
package fractallotus.fundacao.ordenacoes;

import fractallotus.fundacao.cerne.IProjetor;

public class OrdenacaoPorBaldeAscendente<T> extends OrdenacaoPorBalde<T>
{
  // CONSTRUTORES //
  public OrdenacaoPorBaldeAscendente(IProjetor<T, Double> projetor)
  {
    super(projetor);
  }

  // ASSISTENTES //
  protected int indexar(int total, int indice)
  {
    return indice;
  }
}